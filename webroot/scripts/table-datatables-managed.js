var TableDatatablesManaged = function () {
    var initOrdersTable = function () {

        var ordersTable = $('#orders_table');

        ordersTable.dataTable({
            "searching": false,
            "bFilter" : false,
            "bLengthChange": false,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No record(s) found",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },
            "pageLength": 25,
            "columnDefs": [{
                "className": 'control',
                'orderable': false,
                'targets': [0]
            }, {
                "searchable": false,
                "targets": [0]
            }],
            "aoColumnDefs": [{
                "bSortable": false,
                'aTargets': [9, 10]
            }],
            "order": [
                [0, "asc"]
            ]
        });
    };

    return {
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            if (window.location.pathname === "/retailer-app/orders") {
                initOrdersTable();
            }
        }
    }
}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
    });
}
