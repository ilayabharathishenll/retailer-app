let csrf = (typeof csrfToken === "undefined") ? "" : csrfToken;
$(document).ready(function() {
    $('#available_unit').multiselect({includeSelectAllOption:true,buttonWidth: '417px',nonSelectedText: "Select Available",});
});

jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[\w.]+$/i.test(value);
}, "Letters, numbers, and underscores only please"); 

$(document).ready(function() {
    $('#driver,#cash_on_delivery').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":25,
        "ordering": false
    });
   $("#search_result_length").hide();
    $(".dataTables_empty").empty().append("No record(s) found.");
    
});

$(document).ready(function() {
    jQuery.validator.addMethod("fileSize", function (value, element) {
        var file_size = $('input[name="image"]')[0].files[0].size;
        if(file_size>1097152) {
          return false;
        }
        else{
          return true;
        }
    });
    jQuery.validator.addMethod("fileSizeLogo", function (value, element) {
        var file_size = $('input[name="shop_logo"]')[0].files[0].size;
        if(file_size>1097152) {
          return false;
        }
        else{
          return true;
        }
    });
    products.init();
    retailer.init();
    promotion.init();
    users.init();
    category.init();
    subcategory.init();
    option.init();
    cod.init();
    offer.init();
    welcomeoffer.init();
    $("#date_from").datetimepicker({
      autoclose: true,
      format: "dd-mm-yyyy hh:ii",
      startDate: "+0d",
    });
    $("#cod_update").datetimepicker({
      autoclose: true,
      format: "dd-mm-yyyy hh:ii:ss",
      startDate: "0",
      endDate: "+0d"
    });
    $("#date_from").on('changeDate', function (selected) {
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
      $('#date_to').datetimepicker('setStartDate', startDate);
      $('#date_to').val("").datetimepicker("update");
    });
    $("#date_to").datetimepicker({
      autoclose: true,
      format: "dd-mm-yyyy hh:ii"
    });
    $('#start_time').timepicker({
        timeFormat: 'HH:mm:ss',
        showSeconds: true,
        showMeridian: false,
    });
    $('#end_time').timepicker({
        timeFormat: 'HH:mm:ss',
        showSeconds: true,
        showMeridian: false,
    });
    $('input').blur(function() {
        var val = $.trim($(this).val());
        $(this).val(val)
    });
});

jQuery(document).ready(function() {
    var cod = $('#cash_on_delivery');

        cod.find('.group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        $(set).each(function () {
            if (checked) {
                $(this).prop("checked", true);
                $(this).parents('tr').addClass("active");
            } else {
                $(this).prop("checked", false);
                $(this).parents('tr').removeClass("active");
            }
        });
    });

    cod.on('change', 'tbody tr .checkboxes', function () {
        $(this).parents('tr').toggleClass("active");
    });

    $('.edit-btn').on('click', function () {
      var selectedboxes = $(".checkboxes:checkbox:checked");
      var paymentsArray = [];
      if(selectedboxes.length > 0) {
        selectedboxes.each(function() {
          paymentsArray.push($(this).attr('data-payment-id'));
        });
        var date = new Date();
        var minutes = 60;
        date.setTime(date.getTime() + (minutes * 60 * 300));
        Cookies("payment_id", paymentsArray.join(','), { expires: date });
        window.location.replace(base_url + "/cod/edit");
      } else {
        alert("Please select atleast one order");
      }
    });

    $('#cod-datepicker').datepicker({ 
        dateFormat: 'dd-mm-yyyy, h:i:s',
        autoclose: true
    });
});
$(document).ready(function(){
    var category_id      = $("#category_id option:selected").val();
    var sub_category_id  = $("#hndsubcateid").val();
    if (category_id!=undefined) {
        funcenableDate(category_id);
    }
    if (category_id!=undefined) {
        funcSubcategory(category_id,sub_category_id);
    }
});
$(document).on('change', '#category_id', function() {
    var category_id = $(this).val();
    funcenableDate(category_id);
});
function funcenableDate(category_id){
    $.ajax({
      url: base_url+'/enableDate',
      data: {
        'category_id' : category_id,
        },
      type: 'POST',
      headers: {
            'X-CSRF-Token': csrf
        },
      success: function (response) {
        if (response != "true") {
            $('input[name="from_date"]').rules("remove");
            $('input[name="to_date"]').rules("remove");
            $('input[name="start_time"]').rules("remove");
            $('input[name="end_time"]').rules("remove");
            $('.enable-date-off-remove').hide();
        } else {
            $('input[name="from_date"]').rules("add", {required:true});
            $('input[name="to_date"]').rules("add", {required:true});
            $('input[name="start_time"]').rules("add", {required:true});
            $('input[name="end_time"]').rules("add", {required:true});
            $('.enable-date-off-remove').show();
        }
      }
    });
}
$(document).on('change', '#category_id', function() {
    var category_id = $(this).val();
    funcSubcategory(category_id,'');
});
function funcSubcategory(category_id,sub_category_id) {
    $.ajax({
        url: base_url+'/enableSubCategory',
        data: {
            'category_id' : category_id,
        },
        type: 'POST',
        headers: {
            'X-CSRF-Token': csrf
        },
        success: function (response) {
            var result=response.split("##^^##");
            if (result[0] == "success") {
                var jsondata=$.parseJSON(result[1]);
                  $("#sub_category_id").empty();
                $.each(jsondata, function (key, val) {
                    if(key==0){
                        var selop='<option value>Select</option>';
                    }
                    var selected='';
                    if(sub_category_id==val.sub_cate_id){
                        var selected="selected";
                    }
                    var disopt="<option value="+val.sub_cate_id+" "+selected+">"+val.sub_cate_name+"</option>";
                    $("#sub_category_id").append(selop+disopt);
                })
                $('#sub_category_id').rules("add", {required:true});
                $('.enable-sub-category-remove').show();
                
            } else {
                $("#sub_category_id").empty();
                $('#sub_category_id').rules("remove");
                $('.enable-sub-category-remove').hide();
            }
        }
    });
}


var Dashboard = function() {

    return {
    	initAmChart1: function() {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_1').size() === 0) {
                return;
            }

            var chartData = [{
                "date": "2012-01-05",
                "distance": 480,
                "townName": "Miami",
                "townName2": "Miami",
                "townSize": 10,
                "latitude": 25.83,
                "duration": 501
            }, {
                "date": "2012-01-06",
                "distance": 386,
                "townName": "Tallahassee",
                "townSize": 7,
                "latitude": 30.46,
                "duration": 443
            }, {
                "date": "2012-01-07",
                "distance": 348,
                "townName": "New Orleans",
                "townSize": 10,
                "latitude": 29.94,
                "duration": 405
            }, {
                "date": "2012-01-08",
                "distance": 238,
                "townName": "Houston",
                "townName2": "Houston",
                "townSize": 16,
                "latitude": 29.76,
                "duration": 309
            }, {
                "date": "2012-01-09",
                "distance": 218,
                "townName": "Dalas",
                "townSize": 17,
                "latitude": 32.8,
                "duration": 287
            }, {
                "date": "2012-01-10",
                "distance": 349,
                "townName": "Oklahoma City",
                "townSize": 11,
                "latitude": 35.49,
                "duration": 485
            }, {
                "date": "2012-01-11",
                "distance": 603,
                "townName": "Kansas City",
                "townSize": 10,
                "latitude": 39.1,
                "duration": 890
            }, {
                "date": "2012-01-12",
                "distance": 534,
                "townName": "Denver",
                "townName2": "Denver",
                "townSize": 18,
                "latitude": 39.74,
                "duration": 810
            }, {
                "date": "2012-01-13",
                "townName": "Salt Lake City",
                "townSize": 12,
                "distance": 425,
                "duration": 670,
                "latitude": 40.75,
                "alpha": 0.4
            }, {
                "date": "2012-01-14",
                "latitude": 36.1,
                "duration": 470,
                "townName": "Las Vegas",
                "townName2": "Las Vegas",
                "bulletClass": "lastBullet"
            }, {
                "date": "2012-01-15"
            }];
            var chart = AmCharts.makeChart("dashboard_amchart_1", {
                type: "serial",
                fontSize: 12,
                fontFamily: "Open Sans",
                dataDateFormat: "YYYY-MM-DD",
                dataProvider: chartData,

                addClassNames: true,
                startDuration: 1,
                color: "#6c7b88",
                marginLeft: 0,

                categoryField: "date",
                categoryAxis: {
                    parseDates: true,
                    minPeriod: "DD",
                    autoGridCount: false,
                    gridCount: 50,
                    gridAlpha: 0.1,
                    gridColor: "#FFFFFF",
                    axisColor: "#555555",
                    dateFormats: [{
                        period: 'DD',
                        format: 'DD'
                    }, {
                        period: 'WW',
                        format: 'MMM DD'
                    }, {
                        period: 'MM',
                        format: 'MMM'
                    }, {
                        period: 'YYYY',
                        format: 'YYYY'
                    }]
                },

                valueAxes: [{
                    id: "a1",
                    title: "distance",
                    gridAlpha: 0,
                    axisAlpha: 0
                }, {
                    id: "a2",
                    position: "right",
                    gridAlpha: 0,
                    axisAlpha: 0,
                    labelsEnabled: false
                }, {
                    id: "a3",
                    title: "duration",
                    position: "right",
                    gridAlpha: 0,
                    axisAlpha: 0,
                    inside: true,
                    duration: "mm",
                    durationUnits: {
                        DD: "d. ",
                        hh: "h ",
                        mm: "min",
                        ss: ""
                    }
                }],
                graphs: [{
                    id: "g1",
                    valueField: "distance",
                    title: "distance",
                    type: "column",
                    fillAlphas: 0.7,
                    valueAxis: "a1",
                    balloonText: "[[value]] miles",
                    legendValueText: "[[value]] mi",
                    legendPeriodValueText: "total: [[value.sum]] mi",
                    lineColor: "#08a3cc",
                    alphaField: "alpha",
                }, {
                    id: "g2",
                    valueField: "latitude",
                    classNameField: "bulletClass",
                    title: "latitude/city",
                    type: "line",
                    valueAxis: "a2",
                    lineColor: "#786c56",
                    lineThickness: 1,
                    legendValueText: "[[description]]/[[value]]",
                    descriptionField: "townName",
                    bullet: "round",
                    bulletSizeField: "townSize",
                    bulletBorderColor: "#02617a",
                    bulletBorderAlpha: 1,
                    bulletBorderThickness: 2,
                    bulletColor: "#89c4f4",
                    labelText: "[[townName2]]",
                    labelPosition: "right",
                    balloonText: "latitude:[[value]]",
                    showBalloon: true,
                    animationPlayed: true,
                }, {
                    id: "g3",
                    title: "duration",
                    valueField: "duration",
                    type: "line",
                    valueAxis: "a3",
                    lineAlpha: 0.8,
                    lineColor: "#e26a6a",
                    balloonText: "[[value]]",
                    lineThickness: 1,
                    legendValueText: "[[value]]",
                    bullet: "square",
                    bulletBorderColor: "#e26a6a",
                    bulletBorderThickness: 1,
                    bulletBorderAlpha: 0.8,
                    dashLengthField: "dashLength",
                    animationPlayed: true
                }],

                chartCursor: {
                    zoomable: false,
                    categoryBalloonDateFormat: "DD",
                    cursorAlpha: 0,
                    categoryBalloonColor: "#e26a6a",
                    categoryBalloonAlpha: 0.8,
                    valueBalloonsEnabled: false
                },
                legend: {
                    bulletType: "round",
                    equalWidths: false,
                    valueWidth: 120,
                    useGraphSettings: true,
                    color: "#6c7b88"
                }
            });
        },
		initAmChart3: function() {
		            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_3').size() === 0) {
		                return;
		            }

		            var chart = AmCharts.makeChart("dashboard_amchart_3", {
		                "type": "serial",
		                "addClassNames": true,
		                "theme": "light",
		                "path": "../assets/global/plugins/amcharts/ammap/images/",
		                "autoMargins": false,
		                "marginLeft": 30,
		                "marginRight": 8,
		                "marginTop": 10,
		                "marginBottom": 26,
		                "balloon": {
		                    "adjustBorderColor": false,
		                    "horizontalPadding": 10,
		                    "verticalPadding": 8,
		                    "color": "#ffffff"
		                },

		                "dataProvider": [{
		                    "year": 2009,
		                    "income": 23.5,
		                    "expenses": 21.1
		                }, {
		                    "year": 2010,
		                    "income": 26.2,
		                    "expenses": 30.5
		                }, {
		                    "year": 2011,
		                    "income": 30.1,
		                    "expenses": 34.9
		                }, {
		                    "year": 2012,
		                    "income": 29.5,
		                    "expenses": 31.1
		                }, {
		                    "year": 2013,
		                    "income": 30.6,
		                    "expenses": 28.2,
		                }, {
		                    "year": 2014,
		                    "income": 34.1,
		                    "expenses": 32.9,
		                    "dashLengthColumn": 5,
		                    "alpha": 0.2,
		                    "additional": "(projection)"
		                }],
		                "valueAxes": [{
		                    "axisAlpha": 0,
		                    "position": "left"
		                }],
		                "startDuration": 1,
		                "graphs": [{
		                    "alphaField": "alpha",
		                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		                    "fillAlphas": 1,
		                    "title": "Income",
		                    "type": "column",
		                    "valueField": "income",
		                    "dashLengthField": "dashLengthColumn"
		                }, {
		                    "id": "graph2",
		                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		                    "bullet": "round",
		                    "lineThickness": 3,
		                    "bulletSize": 7,
		                    "bulletBorderAlpha": 1,
		                    "bulletColor": "#FFFFFF",
		                    "useLineColorForBulletBorder": true,
		                    "bulletBorderThickness": 3,
		                    "fillAlphas": 0,
		                    "lineAlpha": 1,
		                    "title": "Expenses",
		                    "valueField": "expenses"
		                }],
		                "categoryField": "year",
		                "categoryAxis": {
		                    "gridPosition": "start",
		                    "axisAlpha": 0,
		                    "tickLength": 0
		                },
		                "export": {
		                    "enabled": true
		                }
		            });
		        },
        init: function() {
        	this.initAmChart1()
            this.initAmChart3();
        }
    }
}();
$(document).ready(function() {
    $('.search_opt').select2();
    $('.portlet-body table thead th a').attr('href', 'javascript:;');
    $(".from").datepicker({
      autoclose: true,
      format: "dd/mm/yyyy"
    });
    $(".from ").on('changeDate', function (selected) {
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
      $('.to').datepicker('setStartDate', startDate);
      $('.to').val("").datepicker("update");
    });
    $(".to").datepicker({
      autoclose: true,
      format: "dd/mm/yyyy"
    });
    Dashboard.init();
    $(".portlet-body.editing .fa-times").on('click', function () {
        $(".portlet-body.editing .image_prod").css('display', 'none');
        $(this).css('display', 'none');
        $(".portlet-body.editing input.img_upload").removeClass('hidden').attr('type', 'file');
    });
    if (!$(".portlet-body.listing table tbody tr").length) {
        var thead = $(".portlet-body.listing table thead th").length;
        $(".portlet-body.listing table tbody").html("<tr><td colspan='"+thead+"'>No record(s) found.</td></tr>");
    }
    $(document).on('click','.actions a.view_detail', function(){
        detail = $(this).siblings('.hidden.row_detail').html();
        $('#view_detail').html(detail);
    });
    $("select.discount_type").on('change', function () {
        var discount_type = $(this).val();
        if (discount_type == 'Promotion') {
            $('.form-group.discount').addClass('hidden');
            $('.form-group.discount input').val('');
            $('.form-group.promotion').removeClass('hidden');
            $('input[name="total_stock"]').rules("add", {required: true});
            $('input[name="image"]').rules("add", {required: true});
        } else if (discount_type == 'Discount') {
            $('input[name="total_stock"]').rules("remove");
            $('input[name="image"]').rules("remove");
            $('.form-group.promotion').addClass('hidden');
            $('.form-group.discount').removeClass('hidden');
            $('.form-group.promotion input').val('');
            $('.form-group.promotion textarea').val('');
        } else {
            $('.form-group.promotion').addClass('hidden');
            $('.form-group.discount').addClass('hidden');
            $('.form-group.promotion input').val('');
            $('.form-group.promotion textarea').val('');
            $('.form-group.discount input').val('');
        }
    });
    var discount_type = $('select.discount_type').val();
    if (discount_type == 'Promotion') {
        $('.form-group.discount').addClass('hidden');
        $('.form-group.discount input').val('');
        $('.form-group.promotion').removeClass('hidden');
        $('input[name="total_stock"]').rules("add", {required: true});
        $('input[name="image"]').rules("add", {required: true});
    } else if (discount_type == 'Discount') {
        $('input[name="total_stock"]').rules("remove");
        $('input[name="image"]').rules("remove");
        $('.form-group.promotion').addClass('hidden');
        $('.form-group.discount').removeClass('hidden');
        $('.form-group.promotion input').val('');
        $('.form-group.promotion textarea').val('');
    } else {
        $('.form-group.promotion').addClass('hidden');
        $('.form-group.discount').addClass('hidden');
        $('.form-group.promotion input').val('');
        $('.form-group.promotion textarea').val('');
        $('.form-group.discount input').val('');
    }

    var total = 0;
    $("tr .cost").each(function(){
        var cost = parseInt($(this).text());
        total = total + cost;
      });
    $('.total').text(total);

    if (window.location.pathname === "/retailer-app/orders") {
        var ordertable = $('#orders_table').DataTable({
            destroy: true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bInfo": false,
            "iDisplayLength":25,
            "ordering": false
        });
        var cDate = new Date();
        var ctime = cDate.getTime();
        var buttons = new $.fn.dataTable.Buttons(ordertable, {
            
            buttons: [
                {
                    text: 'CSV <i class="fa fa-file-excel-o"></i>',
                    extend: 'csv',
                    className: 'btn blue',
                    titleAttr: 'Download CSV',
                    exportOptions: {
                        columns: "thead th:not(.noExport)",
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    },
                    filename: 'Orders_' + ctime,

                }
            ]
        }).container().appendTo($('.export-csv'));
    }
});

let users = function () {
  let handleUsers = function () {
    $('.users').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        first_name: {
          required: true,
          maxlength: 10,
          alphanumeric : true
        },
        last_name: {
          required: true,
          maxlength: 10,
          alphanumeric : true
        },
        username: {
          required: true,
          alphanumeric : true

        },
        email_id: {
          required: true,
          email: true,
        },
        password: {
          required: true,
        },
        mobile: {
          required: true,
          number: true,
          minlength: 10,
          remote: {
                url: base_url + "/uniqueUserMobile",
                type: "post",
                data: {
                    id: function () {
                        let id = (typeof $("#user_id").val() === "undefined") ? "" : $("#user_id").val();
                        return id;
                    }
                },
                headers: {
                    'X-CSRF-Token': csrf
                },
            }
        },
        user_role: {
          required: true,
        },
      },

      messages: {
        first_name: {
          required: "First Name is required.",
          maxlength: "Please enter no more than 10 characters"
        },
        last_name: {
          required: "Last Name is required.",
          maxlength: "Please enter no more than 10 characters"
        },
        username: {
          required: "Username is required."
        },
        email_id: {
          required: "Email Id is required.",
          email: "Please enter valid email",
        },
        password: {
          required: "Password is required"
        },
        mobile: {
          required: "Mobile number is required",
          number: "Enter valid mobile number",
          minlength: "Please enter at least 10 digits",
          remote: "This mobile already exists"
        },
        user_role: {
          required: "User Role is required",
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleUsers();
    }
  };

}();

let products = function () {
  let handleProduct = function () {
    $('.products').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        product_name: {
          required: true,
          // maxlength: 21,
          // lettersonly: true
        },
        price: {
          required: true,
          maxlength: 17,
          number: true
        },
        status: {
          required: true,
          maxlength: 15,
        },
        category_id: {
          required: true
        },
        sub_category_id: {
          required: true
        },
        total_stock: {
          required: true,
          number: true
        },
        unit: {
          required: true,
        },
        from_date: {
          required: true,
        },
        to_date: {
          required: true,
        },
        start_time: {
          required: true,
        },
        end_time: {
          required: true,
        },
        image: {
          required: true,
        },
        minimum: {
            required: true,
        },
        maximum: {
            required: true,
        },
        multiply: {
            required: true,
        }
      },

      messages: {
        product_name: {
          required: "Product Name is required.",
        },
        price: {
          required: "Price is required.",
          number: "Please enter valid price"
        },
        status: {
          required: "Status is required."
        },
        category_id: {
          required: "Category Id is required."
        },
        sub_category_id: {
          required: "Sub Category  is required."
        },
        total_stock: {
          required: "Total stock is required",
          number: "Please enter valid stock"
        },
        unit: {
          required: "Please select the unit",
        },
        from_date: {
          required: "From Date is required",
        },
        to_date: {
          required: "To Date is required",
        },
        start_time: {
          required: "Start time is required"
        },
        end_time: {
          required: "End Time is required"
        },
        image: {
            required: "Image is required",
        },
        minimum: {
            required: "Minimum quantity is required",
        },
        maximum: {
            required: "Maximum quantity is required",
        },
        multiply: {
            required: "Multiply quantity is required",
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleProduct();
    }
  };

}();
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
}, "Please enter only alphabets"); 

let retailer = function () {
  let handleProduct = function () {
    $('.retailer').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        first_name: {
          required: true,
          maxlength: 21,
          lettersonly: true
        },
        last_name: {
          required: true,
          maxlength: 15,
          lettersonly: true
        },
        username: {
          required: true,
          maxlength: 15,
          lettersonly: true
        },
        email_id: {
          email: true,
          required: true,
          remote: {
                url: base_url + "/uniqueRetailerEmail",
                type: "post",
                data: {
                    id: function () {
                        let id = (typeof $("#shop_id").val() === "undefined") ? "" : $("#shop_id").val();
                        return id;
                    }
                },
                headers: {
                    'X-CSRF-Token': csrf
                },
            }
        },
        password: {
          required: true,
        },
        mobile: {
          required: true,
          number: true,
          minlength: 6,
          maxlength: 10,
          remote: {
                url: base_url + "/uniqueRetailerMobile",
                type: "post",
                data: {
                    id: function () {
                        let id = (typeof $("#shop_id").val() === "undefined") ? "" : $("#shop_id").val();
                        return id;
                    }
                },
                headers: {
                    'X-CSRF-Token': csrf
                },
            }
        },
        shop_name: {
          required: true,
        },
        shop_logo: {
          required: true,
        },
        shop_no: {
          required: true,
        },
        address1: {
          required: true,
        },
        address2: {
          required: true,
        },
        pincode: {
          required: true,
          number: true
        },
        city: {
          required: true,
          lettersonly: true
        },
        state: {
          required: true,
          lettersonly: true
        },
        country: {
          required: true,
          lettersonly: true
        },
        user_role: {
          required: true,
        },
        area: {
            required: true
        },
        proof_identity: {
            required: true
        },
        gst_no: {
            required: true
        }
      },

      messages: {
        first_name: {
          required: "First Name is required.",
          maxlength: "Please enter no more than 21 characters"
        },
        last_name: {
          required: "Last Name is required",
          maxlength: "Please enter no more than 15 characters"
        },
        username: {
          required: "Owner name is required.",
          maxlength: "Please enter no more than 15 characters"
        },
        email_id: {
          required: "Email is required",
          email: "Please enter a valid Email",
          remote: "This email already exists"
        },
        password: {
          required: "Password is required."
        },
        shop_name: {
          required: "Shop name is required."
        },
        shop_logo: {
          required: "Please upload shop logo."
        },
        shop_no: {
          required: "Shop number is required."
        },
        mobile: {
          required: "Mobile number is required.",
          number: "Enter valid mobile number",
          remote: "This mobile number already exists",
          minlength: "Please enter minimum 6 digit number",
          maxlength: "Please enter maximum 10 digit number"
        },
        address1: {
          required: "Address is required."
        },
        address2: {
          required: "Address is required."
        },
        pincode: {
          required: "Pincode is required.",
          number: "Enter valid pincode"
        },
        city: {
          required: "City is required.",
        },
        state: {
          required: "State is required.",
        },
        country: {
          required: "Country is required.",
        },
        user_role: {
          required: "Role is required.",
        },
        area: {
            required: "area is required."
        },
        proof_identity: {
            required: "Identity is required."
        },
        gst_no: {
            required: "Identity No is required."
        }

      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleProduct();
    }
  };

}();

let promotion = function () {
  let handlePromotion = function () {
    $('.promotion').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        product_id: {
            required: true,
        },
        discount_type: {
          required: true,
        },
        weight: {
          required: true,
          number: true
        },
        total_stock: {
          required: true,
          number: true
        },
        image: {
          required: true,
          extension: "png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF",
          fileSize: true
        },
        start_date: {
          required: true,
        },
        end_date: {
          required: true,
        }
      },

      messages: {
        product_id: {
          required: "Please choose any product.",
        },
        discount_type: {
          required: "Type is required.",
        },
        weight: {
          required: "Weight is required.",
          number: "Enter valid weight without alphabets"
        },
        total_stock: {
          required: "Total Stock is required.",
          number: "Enter valid stock without alphabets"
        },
        image: {
          required: "Image is required.",
          extension: "File type must be jpeg, jpg or png and file size should be 1MB(max)",
          fileSize: "File type must be jpeg, jpg or png and file size should be 1MB(max)"
        },
        start_date: {
          required: "From date is required.",
        },
        end_date: {
          required: "To date is required.",
        }

      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handlePromotion();
    }
  };
}();

let cod = function () {
  let handleCod = function () {
    $('.payments').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        'payment[transaction_date]': {
            required: true,
        },
        'payment[transaction_status]': {
          required: true,
        }
      },

      messages: {
        'payment[transaction_date]': {
            required: "Please select date",
        },
        'payment[transaction_status]': {
          required: "Please select status",
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleCod();
    }
  };
}();

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only allowed"); 

let category = function () {
  let handleCategory = function () {
    $('.category').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        category_name: {
          required: true,
        },
        image: {
          required: true,
          extension: "png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF",
          fileSize: true
        },
      },

      messages: {
        category_name: {
          required: "Name is required.",
        },
        image: {
          required: "Image is required.",
          extension: "File type must be jpeg, jpg or png and file size should be 1MB(max)",
          fileSize: "File type must be jpeg, jpg or png and file size should be 1MB(max)"
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleCategory();
    }
  };
}();

let option = function () {
  let handleOption = function () {
    $('.option').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        name: {
          required: true,
        },
        value: {
          required: true,
        },
      },

      messages: {
        name: {
          required: "Name is required.",
        },
        value: {
          required: "Value is required.",
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleOption();
    }
  };
}();

let subcategory = function () {
  let handleCategory = function () {
    $('.subcategory').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        category_id: {
          required: true,
        },
        sub_category_name: {
          required: true,
        },
        image: {
          required: true,
          extension: "png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF",
          fileSize: true
        },
      },

      messages: {
        category_id: {
          required: "Category Name is required.",
        },
        sub_category_name: {
          required: "Sub Category Name is required.",
        },
        image: {
          required: "Image is required.",
          extension: "File type must be jpeg, jpg or png and file size should be 1MB(max)",
          fileSize: "File type must be jpeg, jpg or png and file size should be 1MB(max)"
        }
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleCategory();
    }
  };
}();

let offer = function () {
  let handleOffer = function () {
    $('.offer').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        offer_name: {
          required: true,
        },
        offer_description: {
          required: true,
        },
        offer_type: {
          required: true,
        },
        discount_type: {
          required:  true,
        },
        discount_amt: {
          required:  true,
        },
        min_order_amt:{
          required:  true,
        },
        no_of_usage: {
          required:  true,
        },
        start_date:{
          required:  true,
        },
        end_date:{
          required:  true,
        },
        image: {
          required: true,
          extension: "png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF",
          fileSize: true
        },
        status: {
          required: true,
        },
      },

      messages: {
        offer_name: {
          required: "Offer Name is required.",
        },
        offer_description: {
          required:  "Offer Description is required.",
        },
        offer_type: {
          required:  "Offer Type is required.",
        },
        discount_type: {
          required:  "Discount Type is required.",
        },
        discount_amt: {
          required:  "Discount is required.",
        },
         min_order_amt:{
          required: "Minimum order amount is required.",
        },
        no_of_usage: {
          required: "No of Usage is required.",
        },
        start_date:{
          required:  "Start Date is required.",
        },
        end_date:{
          required: "End Date is required.",
        },
        image: {
          required: "Image is required.",
          extension: "File type must be jpeg, jpg or png and file size should be 1MB(max)",
          fileSize: "File type must be jpeg, jpg or png and file size should be 1MB(max)"
        },
        status:{
          required: "status is required.",
        },
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handleOffer();
    }
  };
}();


let welcomeoffer = function () {
  let handlewelcomeoffer = function () {
    $('.welcomeoffer').validate({
      errorElement: 'span',
      errorClass: 'help-block help-block-error',
      focusInvalid: true,
      ignore: "",
      rules: {
        offer_name: {
          required: true,
          alphanumeric : true
        },
        discount_type: {
          required:  true,
        },
        discount_amt: {
          required:  true,
          number : true
        },
        min_order_amt:{
          required:  true,
          number : true
        },
        no_of_days: {
          required:  true,
        },
        image: {
          required: true,
          extension: "png|jpg|jpeg|gif|PNG|JPG|JPEG|GIF",
          fileSize: true
        }
      },

      messages: {
        offer_name: {
          required: "Offer Name is required.",
        },
        discount_type: {
          required:  "Discount Type is required.",
        },
        discount_amt: {
          required:  "Discount is required.",
        },
         min_order_amt:{
          required: "Minimum order amount is required.",
        },
        no_of_days: {
          required: "No of days is required.",
        },
        image: {
          required: "Image is required.",
          extension: "File type must be jpeg, jpg or png and file size should be 1MB(max)",
          fileSize: "File type must be jpeg, jpg or png and file size should be 1MB(max)"
        },
      },

      highlight: function (element) {
        $(element)
          .closest('.form-group').addClass('has-error');
      },

      unhighlight: function (element) {
        $(element)
          .closest('.form-group').removeClass('has-error');
      },

      success: function (label) {
        label
          .closest('.form-group').removeClass('has-error');
      }
    });
  };
  return {
    init: function () {
      handlewelcomeoffer();
    }
  };
}();
$(document).ready(function(){
    var selectval=$("#proof option:selected").val();
    if(selectval!=undefined && selectval!='')
        funRetailerlabel(selectval);
});
$(document).on("change","#proof",function() {
    var selectval = $(this).val();
    funRetailerlabel(selectval);

});
function funRetailerlabel(selectval){
  $(".appendlabel").empty().append(selectval+" No" + '<span class="required"> *</span>');
  $(".appendplaceholder").attr("placeholder",selectval+" No");
}
$(document).on("click","#newshopRegister",function(){ 
    var className=$(this).attr('class');
    if (className=="show_shop_table") {
        $("#newshopRegister").addClass("hide_shop_table");
        $("#newshopRegister").removeClass("show_shop_table");
        $(".new_shop_hide").show();
    }
    if (className=="hide_shop_table") {
        $("#newshopRegister").addClass("show_shop_table");
        $("#newshopRegister").removeClass("hide_shop_table");
        $(".new_shop_hide").hide();
    }
});
$(document).on("click","#newlatestOrder",function(){ 
    var className=$(this).attr('class');
    if (className=="show_order_table") {
        $("#newlatestOrder").addClass("hide_order_table");
        $("#newlatestOrder").removeClass("show_order_table");
        $(".new_order_hide").show();
    }
    if (className=="hide_order_table") {
        $("#newlatestOrder").addClass("show_order_table");
        $("#newlatestOrder").removeClass("hide_order_table");
        $(".new_order_hide").hide();
    }
});

$(document).on("click","#newcodlatestOrder",function(){ 
    var className=$(this).attr('class');
    if (className=="show_codorder_table") {
        $("#newcodlatestOrder").addClass("hide_codorder_table");
        $("#newcodlatestOrder").removeClass("show_codorder_table");
        $(".new_codorder_hide").show();
    }
    if (className=="hide_codorder_table") {
        $("#newcodlatestOrder").addClass("show_codorder_table");
        $("#newcodlatestOrder").removeClass("hide_codorder_table");
        $(".new_codorder_hide").hide();
    }
});
$(document).ready(function(){
    var unit=$("#unit option:selected").val();
    if(unit!=undefined && unit!='')
        funcAvailableOption(unit);
    $('input').blur(function() {
        $(this).val($.trim($(this).val()));
    });
    $('textarea').blur(function() {
        $(this).val($.trim($(this).val()));
    });
});
$(document).on("click","#unit",function(){

    var unit=$(this).val();
    $('#available_unit option:selected').prop('selected', false);
    $(".multiselect-selected-text").empty().append("Select Available");
    funcAvailableOption(unit);
});
function funcAvailableOption(unit){
    if(unit=="Kg"){
        $(".enable-available-show").show();
        $(".grams").show();
        $(".ml").hide();
    }
    if(unit=="Liter"){
        $(".enable-available-show").show();
        $(".grams").hide();
        $(".ml").show();
    }
    if(unit=="Count"){
       $(".enable-available-show").hide();
       $(".grams").hide();
       $(".ml").hide();
    }

}