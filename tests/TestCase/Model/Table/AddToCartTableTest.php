<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AddToCartTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AddToCartTable Test Case
 */
class AddToCartTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AddToCartTable
     */
    public $AddToCart;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.add_to_cart',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AddToCart') ? [] : ['className' => AddToCartTable::class];
        $this->AddToCart = TableRegistry::getTableLocator()->get('AddToCart', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AddToCart);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
