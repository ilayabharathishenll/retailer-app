<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Users', 'action' => 'index']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);
    $routes->connect('uniqueRetailerEmail', ['controller' => 'Retailer', 'action' => 'uniqueRetailerEmail']);
    $routes->connect('uniqueRetailerMobile', ['controller' => 'Retailer', 'action' => 'uniqueRetailerMobile']);
    $routes->connect('uniqueUserMobile', ['controller' => 'Users', 'action' => 'uniqueUserMobile']);
    $routes->connect('enableDate', ['controller' => 'Category', 'action' => 'getEnableDate']);
    $routes->connect('cod', ['controller' => 'Cod', 'action' => 'index']);
    $routes->connect('cod', ['controller' => 'Cod', 'action' => 'index']);
    $routes->connect('subcategory', ['controller' => 'SubCategory', 'action' => 'index']);
    $routes->connect('subcategory/add', ['controller' => 'SubCategory', 'action' => 'add']);
    $routes->connect('subcategory/edit/*', ['controller' => 'SubCategory', 'action' => 'edit']);
    $routes->connect('enableSubCategory', ['controller' => 'SubCategory', 'action' => 'getEnableSubCategory']);
    $routes->connect('offer', ['controller' => 'Offer', 'action' => 'index']);
    $routes->connect('welcomeoffer', ['controller' => 'WelcomeOffer', 'action' => 'index']);
    $routes->connect('welcomeoffer/add', ['controller' => 'WelcomeOffer', 'action' => 'add']);
    $routes->connect('welcomeoffer/edit/*', ['controller' => 'WelcomeOffer', 'action' => 'edit']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});
Router::scope('/api/', function (RouteBuilder $routes) {
    $routes->setExtensions(['json']);
    $routes->connect('admin/login', ['controller' => 'ApiUsers', 'action' => 'authenticateUser']);

    //API User
    $routes->connect('admin', ['controller' => 'ApiUsers', 'action' => 'index']);
    $routes->connect('admin/*', ['controller' => 'ApiUsers', 'action' => 'userView']);
    $routes->connect('admin/add', ['controller' => 'ApiUsers', 'action' => 'userAdd']);
    $routes->connect('admin/update/*', ['controller' => 'ApiUsers', 'action' => 'userEdit']);
    $routes->connect('admin/delete/*', ['controller' => 'ApiUsers', 'action' => 'userDelete']);

    //API Shop
    $routes->connect('login', ['controller' => 'ApiShop', 'action' => 'authenticateUser']);
    $routes->connect('user', ['controller' => 'ApiShop', 'action' => 'index']);
    $routes->connect('user/info/*', ['controller' => 'ApiShop', 'action' => 'userInfo']);
    $routes->connect('user/*', ['controller' => 'ApiShop', 'action' => 'userView']);
    $routes->connect('user/add', ['controller' => 'ApiShop', 'action' => 'userAdd']);
    $routes->connect('user/update/*', ['controller' => 'ApiShop', 'action' => 'userEdit']);
    $routes->connect('user/delete/*', ['controller' => 'ApiShop', 'action' => 'userDelete']);

    //API product
    $routes->connect('product', ['controller' => 'ApiProduct', 'action' => 'index']);
    $routes->connect('product/*', ['controller' => 'ApiProduct', 'action' => 'productView']);
    $routes->connect('product/add', ['controller' => 'ApiProduct', 'action' => 'productAdd']);
    $routes->connect('product/update/*', ['controller' => 'ApiProduct', 'action' => 'productEdit']);
    $routes->connect('product/delete/*', ['controller' => 'ApiProduct', 'action' => 'productDelete']);
    $routes->connect('promotions/*', ['controller' => 'ApiProduct', 'action' => 'promotion']);
    $routes->connect('cart/user/*', ['controller' => 'ApiProduct', 'action' => 'cartUser']);
    $routes->connect('cart/add', ['controller' => 'ApiProduct', 'action' => 'cartAdd']);
    $routes->connect('cart/update', ['controller' => 'ApiProduct', 'action' => 'cartUpdate']);
    $routes->connect('cart/delete', ['controller' => 'ApiProduct', 'action' => 'cartDelete']);

    //API Payment
    $routes->connect('retailer-payment', ['controller' => 'ApiProduct', 'action' => 'payment']);
    $routes->connect('refunds', ['controller' => 'ApiProduct', 'action' => 'refunds']);
    $routes->connect('checkout', ['controller' => 'ApiProduct', 'action' => 'checkout']);
    $routes->connect('cards', ['controller' => 'ApiProduct', 'action' => 'cards']);
    $routes->connect('transfers', ['controller' => 'ApiProduct', 'action' => 'transfers']);
    $routes->connect('payments/*', ['controller' => 'ApiProduct', 'action' => 'paymentList']);
    
    //API category
    $routes->connect('category', ['controller' => 'ApiCategorys', 'action' => 'index']);
    $routes->connect('category/*', ['controller' => 'ApiCategorys', 'action' => 'categoryView']);
    $routes->connect('category/add', ['controller' => 'ApiCategorys', 'action' => 'categoryAdd']);
    $routes->connect('category/update/*', ['controller' => 'ApiCategorys', 'action' => 'categoryEdit']);
    $routes->connect('category/delete/*', ['controller' => 'ApiCategorys', 'action' => 'categoryDelete']);
});
