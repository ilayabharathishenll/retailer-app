<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
/**
 * User Entity
 *
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property string $user_role
 * @property string $image
 * @property string $email_id
 * @property string $phone
 * @property string $mobile
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $updated_on
 *
 * @property \App\Model\Entity\Email $email
 */
class Retailer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'username' => true,
        'password' => true,
        'user_role' => true,
        'shop_name' => true,
        'shop_no' => true,
        'shop_logo' => true,
        'address1' => true,
        'address2' => true,
        'pincode' => true,
        'gst_no' => true,
        'area' => true,
        'city' => true,
        'state' => true,
        'country' => true,
        'email_id' => true,
        'phone' => true,
        'mobile' => true,
        'proof_identity' => true,
        'created_on' => true,
        'updated_on' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected function _setPassword($password) {
        $hasher = new DefaultPasswordHasher();
        $update_password = $hasher->hash($password);
        return($update_password);
    }
}
