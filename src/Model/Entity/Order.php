<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $order_id
 * @property int $cart_id
 * @property int $payment_id
 * @property string $status
 * @property \Cake\I18n\FrozenTime $expected_delivery_time
 * @property string $repay_type
 * @property float $repay_amount
 * @property string $repay_details
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $modifed_on
 *
 * @property \App\Model\Entity\TblCart $tbl_cart
 * @property \App\Model\Entity\TblPayment $tbl_payment
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cart_id' => true,
        'payment_id' => true,
        'status' => true,
        'expected_delivery_time' => true,
        'repay_type' => true,
        'repay_amount' => true,
        'repay_details' => true,
        'created_on' => true,
        'modifed_on' => true,
        'tbl_cart' => true,
        'tbl_payment' => true,
        'reference_token'=>true,
        'amount'=>true,
    ];
}
