<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cart Entity
 *
 * @property int $cart_id
 * @property int $shop_id
 * @property int $order_id
 * @property float $total_products
 * @property float $total_discounts
 * @property float $net_total
 * @property float $grand_total
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $updated_on
 *
 * @property \App\Model\Entity\TblShop $tbl_shop
 * @property \App\Model\Entity\Order $order
 */
class Cart extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'shop_id' => true,
        'order_id' => true,
        'total_products' => true,
        'total_discounts' => true,
        'total_delivery_charge' => true,
        'net_total' => true,
        'grand_total' => true,
        'offer_id' => true,
        'offer_discount' => true,
        'order_price' => true,
        'status' => true,
        'payment_date'=>true,
        'created_on' => true,
        'updated_on' => true,
        'tbl_shop' => true,
        'order' => true
    ];
}
