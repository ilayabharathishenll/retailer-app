<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CartProduct Entity
 *
 * @property int $cart_product_id
 * @property int $cart_id
 * @property int $product_id
 * @property int $discount_id
 * @property float $weight
 * @property float $qty
 * @property float $unit_price
 * @property float $unit_total
 * @property float $discount_amount
 * @property float $total_amount
 * @property float $discount_percentage
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $modified_on
 *
 * @property \App\Model\Entity\TblCart $tbl_cart
 * @property \App\Model\Entity\TblProduct $tbl_product
 * @property \App\Model\Entity\Discount $discount
 */
class CartProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cart_id' => true,
        'product_id' => true,
        'discount_id' => true,
        /*'weight' => true,*/
        'qty' => true,
        'unit_price' => true,
        'unit_total' => true,
        'discount_amount' => true,
        'discount_kgs' => true,
        'product_type' => true,
        'total_amount' => true,
        'discount_percentage' => true,
        'delivery_charge' => true,
        'package_size'=> true,
        'status' => true,
        'created_on' => true,
        'modified_on' => true,
        'tbl_cart' => true,
        'tbl_product' => true,
        'discount' => true
    ];
}
