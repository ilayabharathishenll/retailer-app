<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AddToCart Entity
 *
 * @property int $id
 * @property string $product_id
 * @property string $quantity
 *
 * @property \App\Model\Entity\Product $product
 */
class AddToCart extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'quantity' => true,
        'created' => true,
        'modified' => true
    ];
}
