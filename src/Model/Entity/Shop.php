<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Shop Entity
 *
 * @property int $shop_id
 * @property string $shop_name
 * @property string $shop_logo
 * @property string $username
 * @property string $password
 * @property string $address1
 * @property string $address2
 * @property string $email_id
 * @property string $phone
 * @property string $mobile
 * @property string $pincode
 * @property string $city
 * @property string $state
 * @property string $country
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $updated_on
 *
 * @property \App\Model\Entity\Email $email
 */
class Shop extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name'=>true,
        'last_name'=>true,
        'shop_name' => true,
        'shop_no' => true,
        'shop_logo' => true,
        'username' => false,
        'password' => true,
        'address1' => true,
        'address2' => true,
        'email_id' => true,
        'phone' => true,
        'mobile' => true,
        'pincode' => true,
        'city' => true,
        'state' => true,
        'country' => true,
        'created_on' => true,
        'updated_on' => true,
        'email' => true,
        'area' => true,
        'gst_no' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected function _setPassword($password) {
        return(new DefaultPasswordHasher)->hash($password);
    }
}
