<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $payment_id
 * @property int $cart_id
 * @property string $payment_type
 * @property string $card_details
 * @property string $expire_date
 * @property string $reference_number
 * @property float $cart_amount
 * @property float $transaction_amount
 * @property \Cake\I18n\FrozenTime $transaction_date
 * @property string $transaction_status
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $modified_on
 *
 * @property \App\Model\Entity\TblCart $tbl_cart
 */
class Payment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'payment_id' => true,
        'shop_id' => true,
        'cart_id' => true,
        'payment_type' => true,
        'card_details' => true,
        'expire_date' => true,
        'reference_number' => true,
        'cart_amount' => true,
        'transaction_amount' => true,
        'transaction_date' => true,
        'transaction_status' => true,
        'created_on' => true,
        'modified_on' => true,
        'reference_token'=>true
    ];
}
