<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Delivery Entity
 *
 * @property int $id
 * @property int $cart_id
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $pincode
 * @property string $country
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $updated_on
 *
 * @property \App\Model\Entity\TblCart $tbl_cart
 */
class Delivery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cart_id' => true,
        'name' => true,
        'address1' => true,
        'address2' => true,
        'city' => true,
        'state' => true,
        'pincode' => true,
        'country' => true,
        'phone' => true,
        'mobile' => true,
        'email' => true,
        'created_on' => true,
        'updated_on' => true,
        'tbl_cart' => true
    ];
}
