<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Discount Entity
 *
 * @property int $discount_id
 * @property int $product_id
 * @property string $discount_type
 * @property string $promotion_name
 * @property string $promotion_desc
 * @property string $image
 * @property string $aggregate
 * @property float $weight
 * @property float $discount_percentage
 * @property \Cake\I18n\FrozenTime $start_date
 * @property \Cake\I18n\FrozenTime $end_date
 * @property \Cake\I18n\FrozenTime $created_on
 * @property \Cake\I18n\FrozenTime $updated_on
 *
 * @property \App\Model\Entity\TblProduct $tbl_product
 */
class Discount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'discount_type' => true,
        'promotion_name' => true,
        'promotion_desc' => true,
        'promotion_price' => true,
        'total_stock' => true,
        'image' => true,
        'aggregate' => true,
        'weight' => true,
        'discount_percentage' => true,
        'start_date' => true,
        'end_date' => true,
        'created_on' => true,
        'updated_on' => true,
        'tbl_product' => true
    ];
}
