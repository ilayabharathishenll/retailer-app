<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Auth\FormAuthenticate;
use Cake\Http\ServerRequest;

/**
 * Users Model
 *
 * @property \App\Model\Table\EmailsTable|\Cake\ORM\Association\BelongsTo $Emails
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_users');
        $this->setDisplayField('user_id');
        $this->setPrimaryKey('user_id');
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
        // $this->belongsTo('Emails', [
        //     'foreignKey' => 'email_id'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('user_id')
            ->allowEmpty('user_id', 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->allowEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->allowEmpty('last_name');

        $validator
            ->scalar('username')
            ->maxLength('username', 50)
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 100)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('user_role')
            ->requirePresence('user_role', 'create')
            ->notEmpty('user_role');

        $validator
            ->scalar('image')
            ->maxLength('image', 150)
            ->allowEmpty('image');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 50)
            ->allowEmpty('mobile');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        //$rules->add($rules->existsIn(['email_id'], 'Emails'));

        return $rules;
    }

    public function check($formData){
        $users='';
        $hasher=new DefaultPasswordHasher();
        $auths=new FormAuthenticate();
        $auths->config(['fields' => [
            'username' => 'email_id',
            'password' => 'password'
        ]]);
        $user = $auths->getUsers($formData);       
        if($user){
            $logins =$this->find()->where(['user_id'=>$user['user_id']])->toArray();
            $users=$logins[0];
            $passOk = $hasher->check($formData['password'], $users['password']);
        }
        if($passOk==1 || !empty($users)){
            return $users;
        }else{
            return "";
        }  
    }
    // public function check($formData) {

    //     $passOk = false;
    //     $user=array();
    //     $user = $this->find()
    //             ->hydrate(false)
    //             ->select([ 
    //                 'id','userName'=>'name', 'email', 'password', 'created', 'modified', 'mobile','pinCode'=>'pin_code', 'shopNo'=>'shop_no', 'address', 'state', 'city'])
    //             ->where(['email' => $formData['username']])
    //             ->first();
    //     $query = TableRegistry::get('add_to_cart')
    //             ->find()
    //             ->hydrate(false)
    //             ->select(['Count'=>'count("id")'])
    //             ->where(['user_id'=>1])
    //             ->first();  
    //     $user['cartCount']=$query['Count'];             
    //     $hasher = new DefaultPasswordHasher();

    //     //$hashing = $this->passwordHasher();
    //     // print_r($hashing->check($formData['password'], $user['password']));  
        
    //     if(!is_null($passOk)){
    //         $passOk = $hasher->check($formData['password'], $user['password']);

    //         $bases= new FormAuthenticate();
    //         $auths=$bases->checking_password($formData['password'], $user['password']);
    //         echo "passOk=>".$passOk."password=>".$auths;exit;
    //     }
    //     if($passOk==1){
    //         return $user;
    //     }else{
    //         return "";
    //     }  
    // }

    
}
