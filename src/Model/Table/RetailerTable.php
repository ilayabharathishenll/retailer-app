<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Shops Model
 *
 * @property \App\Model\Table\EmailsTable|\Cake\ORM\Association\BelongsTo $Emails
 *
 * @method \App\Model\Entity\Shop get($primaryKey, $options = [])
 * @method \App\Model\Entity\Shop newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Shop[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Shop|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shop|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shop patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Shop[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Shop findOrCreate($search, callable $callback = null, $options = [])
 */
class RetailerTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_shops');
        $this->setDisplayField('shop_id');
        $this->setPrimaryKey('shop_id');

         $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('shop_id')
            ->allowEmpty('shop_id', 'create');

        $validator
            ->scalar('shop_name')
            ->maxLength('shop_name', 150)
            ->requirePresence('shop_name', 'create')
            ->notEmpty('shop_name');

         $validator
            ->scalar('shop_no')
            ->maxLength('shop_no', 150)
            ->requirePresence('shop_no', 'create')
            ->notEmpty('shop_no');

        $validator
            ->scalar('shop_logo')
            ->maxLength('shop_logo', 150)
            ->allowEmpty('shop_logo', 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 50)
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 150)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('address1')
            ->maxLength('address1', 255)
            ->requirePresence('address1', 'create')
            ->notEmpty('address1');

        $validator
            ->scalar('address2')
            ->maxLength('address2', 255)
            ->allowEmpty('address2');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 60)
            ->requirePresence('phone', 'create')
            ->allowEmpty('phone');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 50)
            ->allowEmpty('mobile');

        $validator
            ->scalar('pincode')
            ->maxLength('pincode', 20)
            ->requirePresence('pincode', 'create')
            ->notEmpty('pincode');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->scalar('state')
            ->maxLength('state', 100)
            ->requirePresence('state', 'create')
            ->notEmpty('state');

        $validator
            ->scalar('country')
            ->maxLength('country', 100)
            ->requirePresence('country', 'create')
            ->notEmpty('country');
        $validator
            ->scalar('area')
            ->maxLength('area', 100)
            ->requirePresence('area', 'create')
            ->notEmpty('area');
         $validator
            ->scalar('proof_identity')
            ->maxLength('proof_identity', 50)
            ->requirePresence('proof_identity', 'create')
            ->notEmpty('proof_identity');
        $validator
            ->scalar('gst_no')
            ->maxLength('gst_no', 100)
            ->requirePresence('gst_no', 'create')
            ->notEmpty('gst_no');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));

        return $rules;
    }
}
