<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Category Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class WelcomeOfferTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_welcome_offer');
        $this->setDisplayField('welcome_id');
        $this->setPrimaryKey('welcome_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('offer_name')
            ->allowEmpty('offer_name', 'create');
        $validator
            ->scalar('offer_description')
            ->allowEmpty('offer_description', 'create');
            
        // $validator
        //     ->scalar('offer_type')
        //     ->requirePresence('offer_type', 'create')
        //     ->notEmpty('offer_type');
         $validator
            ->scalar('discount_type')
            ->requirePresence('discount_type', 'create')
            ->notEmpty('discount_type');
        $validator
            ->scalar('discount_amt')
            ->requirePresence('discount_amt', 'create')
            ->notEmpty('discount_amt');
        $validator
            ->scalar('min_order_amt')
            ->requirePresence('min_order_amt', 'create')
            ->notEmpty('min_order_amt');
        $validator
            ->integer('no_of_days')
            ->requirePresence('no_of_days', 'create')
            ->notEmpty('no_of_days');
        // $validator
        //     ->scalar('start_date')
        //     ->requirePresence('start_date', 'create')
        //     ->notEmpty('start_date');
        // $validator
        //     ->scalar('end_date')
        //     ->requirePresence('end_date', 'create')
        //     ->notEmpty('end_date');
        $validator
            ->scalar('image')
            ->maxLength('image', 150)
            ->allowEmpty('image');
        $validator
            ->scalar('status')
            ->maxLength('status', 10)
            ->allowEmpty('status');

        return $validator;
    }

    public function getOff() {
        $query = $this->find('all')->select(['offer_id','offer_name'])->toArray();
        return $query;
    }

    public function getOfferInfo() {
        $query = $this->find('all')->select()->toArray();
        $offers = array();
        if (!empty($query)) {
            foreach ($query as $offers) {
                $off['offer_id'] = $offers->offer_id;
                $off['offer_description'] = $offers->offer_description;
                // $off['offer_type'] = $offers->offer_type;
                $off['discount_type'] = $offers->discount_type;
                $off['discount_amt'] = $offers->discount_amt;
                $off['no_of_usage'] = $offers->no_of_days;
                // $off['start_date'] = $offers->start_date;
                // $off['end_date'] = $offers->end_date;
                $off['image'] = $offers->image;
                $offers[] = $off;
            }
        }
        return $offers;
    }
}
