<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Category Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class OfferHistoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_offer_history');
        $this->setDisplayField('history_id');
        $this->setPrimaryKey('history_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('shop_id')
            ->allowEmpty('shop_id', 'create');
        $validator
            ->integer('offer_id')
            ->allowEmpty('offer_id', 'create');
        $validator
            ->integer('cart_id')
            ->allowEmpty('cart_id', 'create');
        $validator
            ->numeric('offer_discount_amt')
            ->allowEmpty('offer_discount_amt', 'create');
        return $validator;
    }
    // public function getOff() {
    //     $query = $this->find('all')->select(['offer_id','offer_name'])->toArray();
    //     return $query;
    // }
    // public function getOfferInfo() {
    //     $query = $this->find('all')->select()->toArray();
    //     $offers = array();
    //     if (!empty($query)) {
    //         foreach ($query as $offers) {
    //             $off['offer_id'] = $offers->offer_id;
    //             $off['offer_description'] = $offers->offer_description;
    //             $off['offer_type'] = $offers->offer_type;
    //             $off['discount_type'] = $offers->discount_type;
    //             $off['discount_amt'] = $offers->discount_amt;
    //             $off['no_of_usage'] = $offers->no_of_usage;
    //             $off['start_date'] = $offers->start_date;
    //             $off['end_date'] = $offers->end_date;
    //             $off['image'] = $offers->image;
    //             $offers[] = $off;
    //         }
    //     }
    //     return $offers;
    // }
}
