<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Products Model
 *
 * @property \App\Model\Table\TblCategoriesTable|\Cake\ORM\Association\BelongsTo $TblCategories
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_products');
        $this->setDisplayField('product_id');
        $this->setPrimaryKey('product_id');
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);

        $this->belongsTo('TblCategories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('product_id')
            ->allowEmpty('product_id', 'create');
         $validator
            ->scalar('sub_category_id')
            ->allowEmpty('sub_category_id', 'create');
        $validator
            ->scalar('product_name')
            ->maxLength('product_name', 255)
            ->requirePresence('product_name', 'create')
            ->notEmpty('product_name');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');
        $validator
            ->scalar('unit')
            ->allowEmpty('unit', 'create');
        $validator
            ->scalar('available_option')
            ->allowEmpty('available_option');
        $validator
            ->date('from_date')
            ->allowEmpty('from_date');

        $validator
            ->date('to_date')
            ->allowEmpty('to_date');

        $validator
            ->time('start_time')
            ->allowEmpty('start_time');

        $validator
            ->time('end_time')
            ->allowEmpty('end_time');

        $validator
            ->numeric('minimum')
            ->requirePresence('minimum', 'create')
            ->notEmpty('minimum');

        $validator
            ->numeric('maximum')
            ->requirePresence('maximum', 'create')
            ->notEmpty('maximum');

        $validator
            ->numeric('multiply')
            ->requirePresence('multiply', 'create')
            ->notEmpty('multiply');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'TblCategories'));

        return $rules;
    }

    public function listing($category_id=null,$type=null,$user_id=null,$cart_id=null,$searchKey=''){
       
        $checkCategories = TableRegistry::get('tbl_sub_categories')->find()->where(['category_id'=>$category_id])->toArray();
        if (count($checkCategories)>0) {
            $output=array();
            $type=empty($type)?"Discount":$type;
            $basepath = BASE_URL.'/img/product/';
            $subbasepath = BASE_URL.'/img/subcategory/';
            $time = new Time();
            $offer_date = $time->format('Y-m-d H:i');
            $query = $this->find();
            $keep_sub_id='';
            $i=0;
            $sub_cate_id ='';
            if(count($checkCategories)>0){
                $itemsRow=0;
                foreach ($checkCategories as $subCategory) {
                    $subcatdetails = array();
                    $subcatdetails["subCategoryId"]=$subCategory->sub_category_id;
                    $subcatdetails["subCategoryName"]=$subCategory->sub_category_name;
                    $subcatdetails["subCategoryImage"]=$subbasepath.$subCategory->image;
                    $query = $this->find()->select(['id'=>'Products.product_id','productName'=>'Products.product_name', 'Products.description', 'category'=>'categorys.category_name','dateEnabled'=>'categorys.enable_date', 'Products.price','stock'=>'Products.total_stock', 'imageUrl'=>'Products.image','Products.status','startTime'=>'Products.start_time', 'endTime'=>'Products.end_time','fromDate'=>'Products.from_date','endDate'=>'Products.to_date','minimum'=>'Products.minimum','maximum'=>'Products.maximum','multiply'=>'Products.multiply','unit'=>'Products.unit','availableOption'=>'Products.available_option']);
                    $query->enableHydration(false)
                    ->join([
                        'categorys' => [
                            'table' => 'tbl_categories',
                            'type' => 'INNER',
                            'conditions' => 'categorys.category_id=Products.category_id',
                        ]
                    ]);
                    if(!empty($category_id)){
                        $query->where('Products.category_id='.$category_id);
                    }
                    if(!empty($searchKey)){
                        $query->where('Products.product_name like "%'.$searchKey.'%"');
                    }
                    if($subCategory->sub_category_id){
                        $query->where('Products.sub_category_id='.$subCategory->sub_category_id);
                    }
                    $currentDate = date("Y-m-d");
                    $currentTime = $time->format('H:i:s');
                    $query->where(['Products.from_date <=' => $currentDate, 'Products.to_date >=' => $currentDate]);
                    $query->where(['Products.start_time <=' => $currentTime, 'Products.end_time >=' => $currentTime]);
                    $query->where(['Products.status =' => "Active"]);

                    $query = $query->toArray();
                    if (!empty($query)) {
                        $subcatdetails["productAvailable"]=1;
                        foreach ($query as $key => $value) {
                            $productCart = 0;
                            $discount = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Discount', 'product_id'=>$value["id"],'Date(start_date) <=' => $offer_date,'Date(end_date) >=' => $offer_date])->toArray();
                            if(!empty($cart_id)) {
                                $cartProductsData = TableRegistry::get('tbl_cart_products')->find()->select(['qty'=>'qty','discount_id'=>'discount_id', 'product_id'=>'product_id'])->where(['product_id'=>$value["id"], 'cart_id' => $cart_id])->toArray();
                            }
                            if(!empty($cartProductsData)) {
                                foreach ($cartProductsData as $cartProduct) {
                                    $promotion = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Promotion','discount_id'=>$cartProduct['discount_id'], 'product_id'=>$cartProduct["product_id"],'Date(start_date) <=' => $offer_date,'Date(end_date) >=' => $offer_date])->toArray();
                                    $productCart = $cartProduct['qty'];
                                    if(!empty($promotion)) {
                                        $value['availableStock'] = $value["stock"];
                                    } else {
                                        $value['availableStock'] = $value["stock"] - $productCart;
                                    }
                                }
                            } else {
                                $value['availableStock'] = $value["stock"];
                            }

                            if(!empty($discount)) {
                                foreach ($discount as  $discountValue) {
                                    if($discountValue['product_id'] == $value["id"]) {
                                        $value['discountId'] = $discountValue['discount_id'];
                                        $value['discountPercentage'] = $discountValue['discount_percentage'];
                                        $value['discountKgs'] = $discountValue['weight'];
                                        $value['aggregate'] = $discountValue['aggregate'];
                                        $value['type'] = 'Discount';
                                    }
                                }
                            } else {
                                $value['discountId'] = Null;
                                $value['discountPercentage'] = Null;
                                $value['discountKgs'] = Null;
                                $value['aggregate'] = Null;
                                $value['type'] = 'Product';
                            }
                            if ($value['fromDate'])
                                $value['fromDate'] = date("Y-m-d", strtotime($value['fromDate']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                            if ($value['endDate'])
                                $value['endDate'] = date("Y-m-d", strtotime($value['endDate']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                            if ($value['startTime'])
                                $value['startTime'] = date("G:i:s", strtotime($value['startTime']->i18nFormat([\IntlDateFormatter::NONE, \IntlDateFormatter::LONG])));
                            if ($value['endTime'])
                                $value['endTime'] = date("G:i:s", strtotime($value['endTime']->i18nFormat([\IntlDateFormatter::NONE, \IntlDateFormatter::LONG])));
                            if($value['imageUrl']) {
                                $value['imageUrl']=$basepath.$value['imageUrl'];
                            }
                            if (strtolower($value['unit'])!="count" && $value['unit']!=null ){
                                $value["packageSizeAvailable"]=1;
                                $value["packageSizes"]=json_decode($value['availableOption']);
                            } else if (strtolower($value['unit'])=="count" && $value['unit']!=null){
                                $value["packageSizeAvailable"]=0;
                                $value["packageSizes"]=array();
                            } else{
                                $value["packageSizeAvailable"]=0;
                                $value["packageSizes"]=array();
                            }
                            unset($value["availableOption"]);
                            if (!empty($user_id) && !empty($cart_id)) {

                            }
                            $subcatdetails["items"][]=$value;
                        }
                    } else {
                        $subcatdetails["productAvailable"]=0;
                        $subcatdetails["items"] = array();
                    }
                    $output[] = $subcatdetails;
                }
                return $output;
            }
               
        } else {
            $query=$output=array();
            $type=empty($type)?"Discount":$type;
            $basepath = BASE_URL.'/img/product/';
            $time = new Time();
            $offer_date = $time->format('Y-m-d H:i');
            $query = $this->find();

            $query->select(['id'=>'Products.product_id','productName'=>'Products.product_name', 'Products.description', 'category'=>'categorys.category_name','dateEnabled'=>'categorys.enable_date', 'Products.price','stock'=>'Products.total_stock', 'imageUrl'=>'Products.image','Products.status','startTime'=>'Products.start_time', 'endTime'=>'Products.end_time','fromDate'=>'Products.from_date','endDate'=>'Products.to_date','minimum'=>'Products.minimum','maximum'=>'Products.maximum','multiply'=>'Products.multiply','unit'=>'Products.unit','availableOption'=>'Products.available_option']);
            $query->enableHydration(false)
            ->join([
                'categorys' => [
                    'table' => 'tbl_categories',
                    'type' => 'INNER',
                    'conditions' => 'categorys.category_id=Products.category_id',
                ]
            ]);
            if(!empty($category_id)){
                $query->where('Products.category_id='.$category_id);
            }
            if(!empty($searchKey)){
                $query->where('Products.product_name like "%'.$searchKey.'%"');
            }
            $currentDate = date("Y-m-d");
            $currentTime = $time->format('H:i:s');
            $query->where(['Products.from_date <=' => $currentDate, 'Products.to_date >=' => $currentDate]);
            $query->where(['Products.start_time <=' => $currentTime, 'Products.end_time >=' => $currentTime]);
            $query->where(['Products.status =' => "Active"]);
            $query = $query->toArray();
            if (!empty($query)) {
                foreach ($query as $key => $value) {
                    $productCart = 0;
                    $discount = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Discount', 'product_id'=>$value["id"],'Date(start_date) <=' => $offer_date,'Date(end_date) >=' => $offer_date])->toArray();
                    if(!empty($cart_id)) {
                        $cartProductsData = TableRegistry::get('tbl_cart_products')->find()->select(['qty'=>'qty','discount_id'=>'discount_id', 'product_id'=>'product_id'])->where(['product_id'=>$value["id"], 'cart_id' => $cart_id])->toArray();
                    }
                    if(!empty($cartProductsData)) {
                        foreach ($cartProductsData as $cartProduct) {
                            $promotion = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Promotion','discount_id'=>$cartProduct['discount_id'], 'product_id'=>$cartProduct["product_id"],'Date(start_date) <=' => $offer_date,'Date(end_date) >=' => $offer_date])->toArray();
                            $productCart = $cartProduct['qty'];
                            if(!empty($promotion)) {
                                $value['availableStock'] = $value["stock"];
                            } else {
                                $value['availableStock'] = $value["stock"] - $productCart;
                            }
                        }
                    } else {
                        $value['availableStock'] = $value["stock"];
                    }

                    if(!empty($discount)) {
                        foreach ($discount as  $discountValue) {
                            if($discountValue['product_id'] == $value["id"]) {
                                $value['discountId'] = $discountValue['discount_id'];
                                $value['discountPercentage'] = $discountValue['discount_percentage'];
                                $value['discountKgs'] = $discountValue['weight'];
                                $value['aggregate'] = $discountValue['aggregate'];
                                $value['type'] = 'Discount';
                            }
                        }
                    } else {
                        $value['discountId'] = Null;
                        $value['discountPercentage'] = Null;
                        $value['discountKgs'] = Null;
                        $value['aggregate'] = Null;
                        $value['type'] = 'Product';
                    }
                   if ($value['fromDate'])
                        $value['fromDate'] = date("Y-m-d", strtotime($value['fromDate']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                    if ($value['endDate'])
                        $value['endDate'] = date("Y-m-d", strtotime($value['endDate']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                    if ($value['startTime'])
                        $value['startTime'] = date("G:i:s", strtotime($value['startTime']->i18nFormat([\IntlDateFormatter::NONE, \IntlDateFormatter::LONG])));
                    if ($value['endTime'])
                        $value['endTime'] = date("G:i:s", strtotime($value['endTime']->i18nFormat([\IntlDateFormatter::NONE, \IntlDateFormatter::LONG])));
                    if($value['imageUrl']) {
                        $value['imageUrl']=$basepath.$value['imageUrl'];
                    }
                    if (strtolower($value['unit'])!="count" && $value['unit']!=null ){
                        $value["packageSizeAvailable"]=1;
                        $value["packageSizes"]=json_decode($value['availableOption']);
                    } else if (strtolower($value['unit'])=="count" && $value['unit']!=null){
                        $value["packageSizeAvailable"]=0;
                        $value["packageSizes"]=array();
                    } else{
                        $value["packageSizeAvailable"]=0;
                        $value["packageSizes"]=array();
                    }
                    if (!empty($user_id) && !empty($cart_id)) {

                    }
                    unset($value["availableOption"]);
                    array_push($output,$value);
                }
            }
            return $output;
        }
    }
    public function getProducts() {
        $query = $this->find('all')->select(['product_id','product_name'])->toArray();
        return $query;
    }

    public function updateStock($product_id, $quantity) {
        $product = $this->find("all")->where(['product_id'=> $product_id ])->toArray();
        $product_entity = $this->get($product_id);
        $product_update=array(
            'total_stock'=>$product[0]['total_stock'] - $quantity,
            'updated_on'=>date("Y-m-d H:i:s")
        );
        $products_update = $this->patchEntity($product_entity, $product_update);
        $this->save($products_update);
    }
}
