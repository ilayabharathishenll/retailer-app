<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Category Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class SubCategoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_sub_categories');
        $this->setDisplayField('sub_category_id');
        $this->setPrimaryKey('sub_category_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);

         $this->belongsTo('tbl_categories', [
            'foreignKey' => 'category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('sub_category_id')
            ->allowEmpty('sub_category_id', 'create');
        $validator
            ->scalar('category_id')
            ->requirePresence('category_id', 'create')
            ->notEmpty('category_id');
        $validator
            ->scalar('sub_category_name')
            ->maxLength('sub_category_name', 50)
            ->requirePresence('sub_category_name', 'create')
            ->notEmpty('sub_category_name');

        $validator
            ->scalar('image')
            ->maxLength('image', 150)
            ->allowEmpty('image');

        return $validator;
    }

    public function getCat() {
        $query = $this->find('all')->select(['sub_category_id','sub_category_name'])->toArray();
        return $query;
    }

    public function getCatInfo() {
        $query = $this->find('all')->select(['sub_category_id','category_id','sub_category_name'])->toArray();
        $categories = array();
        if (!empty($query)) {
            foreach ($query as $category) {
                $cat['sub_category_id'] = $category->sub_category_id;
                $cat['sub_category_name'] = $category->sub_category_name;
                $cat['category_id'] = $category->category_id;
                $categories[] = $cat;
            }
        }
        return $categories;
    }
}
