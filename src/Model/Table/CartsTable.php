<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Carts Model
 *
 * @property \App\Model\Table\TblShopsTable|\Cake\ORM\Association\BelongsTo $TblShops
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\Cart get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cart newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cart[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cart|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cart|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cart patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cart[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cart findOrCreate($search, callable $callback = null, $options = [])
 */
class CartsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_carts');
        $this->setDisplayField('cart_id');
        $this->setPrimaryKey('cart_id');

        $this->belongsTo('TblShops', [
             'className' => 'Carts',
            'foreignKey' => 'shop_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TblPayments', [
            'foreignKey' => 'cart_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('cart_id')
            ->allowEmpty('cart_id', 'create');

        $validator
            ->numeric('total_products')
            ->requirePresence('total_products', 'create')
            ->notEmpty('total_products');

        $validator
            ->numeric('total_discounts')
            ->requirePresence('total_discounts', 'create')
            ->notEmpty('total_discounts');

        $validator
            ->numeric('net_total')
            ->requirePresence('net_total', 'create')
            ->notEmpty('net_total');

        $validator
            ->numeric('grand_total')
            ->requirePresence('grand_total', 'create')
            ->notEmpty('grand_total');
        $validator
            ->integer('offer_id')
            ->allowEmpty('offer_id');
        $validator
            ->numeric('offer_discount')
            ->allowEmpty('offer_discount');
        $validator
            ->numeric('order_price')
            ->requirePresence('order_price', 'create')
            ->notEmpty('order_price');
        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');
        $validator
            ->date('payment_date')
            ->requirePresence('payment_date', 'create')
            ->notEmpty('payment_date');
        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->dateTime('updated_on')
            ->requirePresence('updated_on', 'create')
            ->notEmpty('updated_on');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['shop_id'], 'TblShops'));
        //$rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }
    public function check($user_id,$cart_id=null){
        $query=$this->find();
        $query->where('shop_id='.$user_id);
        if(!empty($cart_id)){
           $query->where('cart_id='.$cart_id);
        }
        $result=$query->where('status="Active"')->where('DATE_FORMAT(created_on,"%Y-%m-%d") ="'.date('Y-m-d').'"')->toArray();
        return $result;
    }
    public function listing($user_id){
        $product= $query=$output=$result=array();
        $basepath = BASE_URL.'/img/product/';
        $output=$this->check($user_id);
        if(!empty($output)){
            $cart_id=!empty($output)?$output[0]->cart_id:'';
            $query=array(
                'cartId'=>$output[0]->cart_id,
                'userId'=>$output[0]->shop_id,
                'totalProducts'=>$output[0]->total_products,
                'totalDiscounts'=>($output[0]->total_discounts + $output[0]->offer_discount),
                'totalDeliveryCharge'=>$output[0]->total_delivery_charge,
                'netTotal'=>$output[0]->net_total,
                'grandTotal'=>$output[0]->order_price
            );
            $output['cartList']= TableRegistry::get('tbl_cart_products')
                ->find()
                ->select([
                    'cartProductId'=>'tbl_cart_products.cart_product_id',
                    'discountId'=>'tbl_cart_products.discount_id',
                    'quantity'=>'tbl_cart_products.qty',
                    'price'=>'tbl_cart_products.unit_price',
                    'unitTotal'=>'tbl_cart_products.unit_total',
                    'discountAmount'=>'tbl_cart_products.discount_amount',
                    'deliveryCharge'=>'tbl_cart_products.delivery_charge',
                    'discountPercentage'=>'tbl_cart_products.discount_percentage',
                    'totalAmount'=>'tbl_cart_products.total_amount',
                    'productType'=>'tbl_cart_products.product_type',
                    'productId'=>'product.product_id',
                    'productName'=>'product.product_name',
                    'totalStock'=>'product.total_stock',
                    'imageUrl'=>'product.image',
                    'description'=>'product.description',
                    'productPrice'=>'product.price',
                    'productFromDate'=>'product.from_date',
                    'productToDate'=>'product.to_date',
                    'productStartTime'=>'product.start_time',
                    'productEndTime'=>'product.end_time',
                    'minimum'=>'product.minimum',
                    'maximum'=>'product.maximum',
                    'multiply'=>'product.multiply',
                    'productCategory'=>'product.category_id',
                    'dateEnabled'=>'category.enable_date',
                    'selectedPackageSize'=>'tbl_cart_products.package_size',
                    'unit'=>'product.unit',
                    'availableOption'=>'product.available_option',
                ])
                ->hydrate(false)
                ->join([
                    'product' => [
                        'table' => 'tbl_products',
                        'type' => 'INNER',
                        'conditions' => 'product.product_id=tbl_cart_products.product_id',
                    ]
                ])
                ->join([
                    'category' => [
                        'table' => 'tbl_categories',
                        'type' => 'INNER',
                        'conditions' => 'product.category_id=category.category_id',
                    ]
                ])
                ->where(['cart_id'=>$cart_id])
                ->toArray();
            $cartList = array();
            if(!empty($output['cartList'])){
                foreach ($output['cartList'] as $values) {
                    $discounts = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Discount', 'discount_id'=>$values["discountId"]])->toArray();
                    $promotions = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Promotion', 'discount_id'=>$values["discountId"]])->toArray();
                    if (!empty($discounts)) {
                        foreach ($discounts as $discount) {
                            $values['discountKgs'] = $discount->weight;
                            $values['aggregate'] = $discount->aggregate;
                        }
                    } else {
                        $values['discountKgs'] = '';
                        $values['aggregate'] = '';
                    }
                    if (!empty($promotions)) {
                        foreach ($promotions as $promotion) {
                            $values['availableStock'] = $promotion->total_stock - $values['quantity'];
                            $values['totalStock'] = $promotion->total_stock;
                        }
                    } else {
                        $values['availableStock'] = $values['totalStock'] - $values['quantity'];
                    }
                    if (strtolower($values['unit'])!="count" && $values['unit']!=null ){
                        $values["packageSizeAvailable"]=1;
                        $values["packageSizes"]=json_decode($values['availableOption']);
                    } else if (strtolower($values['unit'])=="count" && $values['unit']!=null){
                        $values["packageSizeAvailable"]=0;
                        $values["packageSizes"]=array();
                    } else{
                        $values["packageSizeAvailable"]=0;
                        $values["packageSizes"]=array();
                    }
                    $values['product']['productId']=$values['productId'];
                    $values['product']['productName']=$values['productName'];
                    $values['product']['productPrice']=$values['productPrice'];
                    $values['product']['productFromDate']=$values['productFromDate'];
                    $values['product']['productToDate']=$values['productToDate'];
                    $values['product']['productStartTime']=$values['productStartTime'];
                    $values['product']['productEndTime']=$values['productEndTime'];
                    $values['product']['productType']=$values['productType'];
                    if($values['imageUrl']){
                        $values['product']['imageUrl']=$basepath.$values['imageUrl'];
                    }
                    unset($values['availableOption']);
                    unset($values['productId']);
                    unset($values['productName']);
                    unset($values['productPrice']);
                    unset($values['imageUrl']);
                    unset($values['productFromDate']);
                    unset($values['productToDate']);
                    unset($values['productStartTime']);
                    unset($values['productEndTime']);
                    unset($values['productType']);
                    // unset($values['totalStock']);
                    $cartList[]=$values;
                }
            }
            $query['cartList'] = $cartList;
        }
        return $query;
    }

}
