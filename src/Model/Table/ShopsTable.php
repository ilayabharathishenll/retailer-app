<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Auth\FormAuthenticate;
use Cake\Http\ServerRequest;

/**
 * Shops Model
 *
 * @property \App\Model\Table\EmailsTable|\Cake\ORM\Association\BelongsTo $Emails
 *
 * @method \App\Model\Entity\Shop get($primaryKey, $options = [])
 * @method \App\Model\Entity\Shop newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Shop[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Shop|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shop|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shop patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Shop[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Shop findOrCreate($search, callable $callback = null, $options = [])
 */
class ShopsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_shops');
        $this->setDisplayField('shop_id');
        $this->setPrimaryKey('shop_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);

        // $this->belongsTo('Emails', [
        //     'foreignKey' => 'email_id',
        //     'joinType' => 'INNER'
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('shop_id')
            ->allowEmpty('shop_id', 'create');

        $validator
            ->scalar('shop_name')
            ->maxLength('shop_name', 150)
            ->requirePresence('shop_name', 'create')
            ->notEmpty('shop_name');
         $validator
            ->scalar('shop_no')
            ->maxLength('shop_no', 50)
            ->requirePresence('shop_no', 'create')
            ->notEmpty('shop_no');

        $validator
            ->scalar('shop_logo')
            ->maxLength('shop_logo', 150)
            ->requirePresence('shop_logo', 'create')
            ->notEmpty('shop_logo');

        /*$validator
            ->scalar('username')
            ->maxLength('username', 50)
            ->requirePresence('username', 'create')
            ->notEmpty('username');*/

        $validator
            ->scalar('password')
            ->maxLength('password', 150)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('address1')
            ->maxLength('address1', 255)
            ->requirePresence('address1', 'create')
            ->notEmpty('address1');

        $validator
            ->scalar('address2')
            ->maxLength('address2', 255)
            ->allowEmpty('address2');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 60)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 50)
            ->allowEmpty('mobile');

        $validator
            ->scalar('pincode')
            ->maxLength('pincode', 20)
            ->requirePresence('pincode', 'create')
            ->notEmpty('pincode');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->scalar('state')
            ->maxLength('state', 100)
            ->requirePresence('state', 'create')
            ->notEmpty('state');

        $validator
            ->scalar('country')
            ->maxLength('country', 100)
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->dateTime('updated_on')
            ->requirePresence('updated_on', 'create')
            ->notEmpty('updated_on');

        $validator
            ->scalar('area')
            ->maxLength('area', 100)
            ->requirePresence('area', 'create')
            ->notEmpty('area');

        $validator
            ->scalar('gst_no')
            ->maxLength('gst_no', 20)
            ->requirePresence('gst_no', 'create')
            ->notEmpty('gst_no');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
       // $rules->add($rules->existsIn(['email_id'], 'Emails'));

        return $rules;
    }

    public function check($formData){
        $users=$passOk='';
        $basepath = BASE_URL.'/img/logo/';
        $hasher=new DefaultPasswordHasher();
        $auths=new FormAuthenticate();
        $auths->config(['fields' => [
            'username' => 'mobile',
            'password' => 'password'
        ],  'userModel' => 'Shops']);
        $user = $auths->getUsers($formData);
        if($user){
            $logins =$this->find()
            ->select([
                'userId'=>'shop_id',
                'firstName'=>"first_name",
                'lastName'=>"last_name",
                'userName'=>"username",
                'userRole'=>"user_role",
                'shopName'=>"shop_name",
                'shopNo'=>"shop_no",
                'shopLogo'=>"CONCAT('".$basepath."',shop_logo)",
                "address1",
                "address2",
                'email'=>"email_id",
                "phone",
                "mobile",
                'pinCode'=>"pincode",
                "city",
                "state",
                "password",
                "country",
                'created'=>"created_on",
                'modified'=>"updated_on",
            ])->where(['shop_id'=>$user['shop_id']])->toArray();
            $users=$logins[0];
            $passOk = $hasher->check($formData['password'], $users['password']);
        }
        if($passOk==1 || !empty($users)){
            return $users;
        }else{
            return "";
        }  
    }

    public function view($shop_id){
        $basepath = BASE_URL.'/img/logo/';
        $query=array();
        $query = $this->find('all')
        ->hydrate(false)
        ->select([
            'userId'=>'shop_id',
            'firstName'=>"first_name",
            'lastName'=>"last_name",
            'userName'=>"username",
            'userRole'=>"user_role",
            'shopName'=>"shop_name",
            'shopNo'=>"shop_no",
            'shopLogo'=>"CONCAT('".$basepath."',shop_logo)",
            "address1",
            "address2",
            'email'=>"email_id",
            "phone",
            "mobile",
            'pinCode'=>"pincode",
            "city",
            "state",
            "country",
            'created'=>"created_on",
            'modified'=>"updated_on",
        ])
        ->where(['shop_id'=>$shop_id])->toArray();
        if(!empty($query)){
            return $query[0];
        }else{
            return "";
        }
    } 
}
