<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CartProducts Model
 *
 * @property \App\Model\Table\TblCartsTable|\Cake\ORM\Association\BelongsTo $TblCarts
 * @property \App\Model\Table\TblProductsTable|\Cake\ORM\Association\BelongsTo $TblProducts
 * @property \App\Model\Table\DiscountsTable|\Cake\ORM\Association\BelongsTo $Discounts
 *
 * @method \App\Model\Entity\CartProduct get($primaryKey, $options = [])
 * @method \App\Model\Entity\CartProduct newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CartProduct[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CartProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CartProduct|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CartProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CartProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CartProduct findOrCreate($search, callable $callback = null, $options = [])
 */
class CartProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_cart_products');
        $this->setDisplayField('cart_product_id');
        $this->setPrimaryKey('cart_product_id');

        $this->belongsTo('TblCarts', [
            'foreignKey' => 'cart_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TblProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Discounts', [
            'foreignKey' => 'discount_id'
        ]);
        // $this->belongsTo('Payments', [
        //     'foreignKey' => 'cart_id',
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('cart_product_id')
            ->allowEmpty('cart_product_id', 'create');
        

        $validator
            ->numeric('qty')
            ->requirePresence('qty', 'create')
            ->notEmpty('qty');

        $validator
            ->numeric('unit_price')
            ->requirePresence('unit_price', 'create')
            ->notEmpty('unit_price');

        $validator
            ->numeric('unit_total')
            ->requirePresence('unit_total', 'create')
            ->notEmpty('unit_total');

        $validator
            ->numeric('total_amount')
            ->requirePresence('total_amount', 'create')
            ->notEmpty('total_amount');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');
        $validator
            ->scalar('package_size')
            ->requirePresence('package_size', 'create')
            ->allowEmpty('package_size');
          
        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->dateTime('modified_on')
            ->requirePresence('modified_on', 'create')
            ->notEmpty('modified_on');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cart_id'], 'TblCarts'));
        $rules->add($rules->existsIn(['product_id'], 'TblProducts'));
        $rules->add($rules->existsIn(['discount_id'], 'Discounts'));

        return $rules;
    }

    public function checkProducts($productDetails) {
        if (!empty($productDetails['discount_id'])) {
            $condition = $productDetails;
        } else {
            $condition = 'cart_id = '.$productDetails['cart_id'].' AND product_id = '.$productDetails['product_id'].' AND discount_id is NULL';
        }
        $query = $this->find('all')
            ->select(['total_products'=>'count(*)','cart_product_id'=>'cart_product_id','discount_amount'=>'discount_amount','qty'=>'qty','total_amount'=>'total_amount'])
            ->where($condition)
            ->group('cart_product_id')->toArray();
        if(!empty($query)){
            $query=$query[0];
        }
        return $query;
    }

    public function cart_total($cart_id){
        $query=$this->find('all')
        ->select(['total_products'=>'count(*)','total_discount'=>'sum(discount_amount)','net_total'=>'sum(unit_total)','total_delivery_charge'=>'sum(delivery_charge)','grand_total'=>'sum(total_amount)'])
        ->where('cart_id='.$cart_id)->toArray();
        if(!empty($query)){
            $query=$query[0];
        }
        return $query;
    }
    public function cart_product_delete($cart_product_id,$cart_id){
        $query = $this->query();
        $query->delete()
            ->where(['cart_product_id' => $cart_product_id,'cart_id'=>$cart_id,'status'=>"Active"])
            ->execute();
    }

    public function shop_user($shop_id){
        $query=$this->find('all')
        ->select()
        ->where(['shop_id='.$shop_id,"status"=>"Paid"])->toArray();
       return $query;
    }

}
