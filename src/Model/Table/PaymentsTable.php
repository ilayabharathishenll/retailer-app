<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 *
 * @property \App\Model\Table\TblCartsTable|\Cake\ORM\Association\BelongsTo $TblCarts
 *
 * @method \App\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payment findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_payments');
        $this->setDisplayField('payment_id');
        $this->setPrimaryKey('payment_id');

        $this->belongsTo('Carts', [
            'foreignKey' => 'cart_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CartProducts', [
            'foreignKey' => 'cart_id',
        ]);
        $this->belongsTo('Shops', [
            'foreignKey' => 'shop_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('payment_id')
            ->allowEmpty('payment_id', 'create');

        $validator
            ->scalar('payment_type')
            ->maxLength('payment_type', 100)
            ->requirePresence('payment_type', 'create')
            ->notEmpty('payment_type');

        $validator
            ->scalar('card_details')
            ->maxLength('card_details', 255)
            ->allowEmpty('card_details');

        $validator
            ->scalar('expire_date')
            ->maxLength('expire_date', 50)
            ->allowEmpty('expire_date');

        $validator
            ->scalar('reference_number')
            ->maxLength('reference_number', 255)
            ->allowEmpty('reference_number');

        $validator
            ->numeric('transaction_amount')
            ->requirePresence('transaction_amount', 'create')
            ->notEmpty('transaction_amount');

        $validator
            ->dateTime('transaction_date')
            ->allowEmpty('transaction_date');

        $validator
            ->scalar('transaction_status')
            ->maxLength('transaction_status', 100)
            ->requirePresence('transaction_status', 'create')
            ->notEmpty('transaction_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
}
