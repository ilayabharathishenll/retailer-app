<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Discounts Model
 *
 * @property \App\Model\Table\TblProductsTable|\Cake\ORM\Association\BelongsTo $TblProducts
 *
 * @method \App\Model\Entity\Discount get($primaryKey, $options = [])
 * @method \App\Model\Entity\Discount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Discount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Discount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Discount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Discount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Discount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Discount findOrCreate($search, callable $callback = null, $options = [])
 */
class DiscountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_discounts');
        $this->setDisplayField('discount_id');
        $this->setPrimaryKey('discount_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);

        $this->belongsTo('TblProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('discount_id')
            ->allowEmpty('discount_id', 'create');

        $validator
            ->scalar('discount_type')
            ->requirePresence('discount_type', 'create')
            ->notEmpty('discount_type');

        $validator
            ->scalar('promotion_name')
            ->maxLength('promotion_name', 100)
            ->allowEmpty('promotion_name');

        $validator
            ->scalar('promotion_desc')
            ->maxLength('promotion_desc', 255)
            ->allowEmpty('promotion_desc');

        $validator
            ->scalar('total_stock')
            ->maxLength('total_stock', 255)
            ->allowEmpty('total_stock');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmpty('image');

        $validator
            ->numeric('weight')
            ->requirePresence('weight', 'create')
            ->notEmpty('weight');

        $validator
            ->numeric('discount_percentage')
            ->requirePresence('discount_percentage', 'create')
            ->allowEmpty('discount_percentage');

        $validator
            ->dateTime('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->dateTime('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmpty('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'TblProducts'));

        return $rules;
    }

    public function promotion($user_id, $cart_id) {
        $promotion=$query=array();
        $currentDatetime = date('Y-m-d H:i:s');
        $basepath = BASE_URL.'/img/';
        $query =$this->find()
        ->select(['promotionId'=>'Discounts.discount_id','productId'=>'Discounts.product_id', 'name'=>'Discounts.promotion_name' ,'imageUrl'=>'Discounts.image','description'=>'Discounts.promotion_desc','Discounts.weight','Discounts.promotion_price', 'productName' => 'Products.product_name', 'productImg' => 'Products.image','totalStock'=>'Discounts.total_stock','type'=>'Discounts.discount_type'])
       ->join([
            'Products' => [
                'table' => 'tbl_products',
                'type' => 'INNER',
                'conditions' => 'Discounts.product_id=Products.product_id',
            ]
        ])
        ->enableHydration(false)
        ->where(['Discounts.discount_type '=> "Promotion", 'Discounts.end_date >=' => $currentDatetime, 'Discounts.start_date <=' => $currentDatetime])
        ->toArray();

         $GetOfferDetails = TableRegistry::get('tbl_welcome_offer')->find('all')->select()->where("status='Active'")->order('welcome_id DESC')->first();
        if(count($GetOfferDetails)>0){
            $welcome_id           = $GetOfferDetails->welcome_id;
            $offer_name           = $GetOfferDetails->offer_name;
            $offer_description    = $GetOfferDetails->offer_description;
            $discount_type        = $GetOfferDetails->discount_type;
            $discount_amt         = $GetOfferDetails->discount_amt;
            $min_order_amt        = $GetOfferDetails->min_order_amt;
            $no_of_days           = $GetOfferDetails->no_of_days;
            $image                = $GetOfferDetails->image;
            $values["promotionId"]=$welcome_id;
            $values["productId"]='';
            $values["name"]=$offer_name;
            $values["imageUrl"]=$basepath."offers/".$image;
            $values["description"]=$offer_description;
            $values["productName"]='';
            $values["productImg"]='';
            $values["totalStock"]=""; 
            $values["type"]="Welcomeoffer"; 
            $values['products']=new \stdClass();
        }

        if(count($GetOfferDetails)>0){
            array_push($promotion,$values);
        }

        if(!empty($query)){
            $no=0;
            foreach ($query as $value) {
                $productCart = 0;
                $value['products']=array();
                
                if($value['imageUrl']){
                    $value['imageUrl']=$basepath."promotion/".$value['imageUrl'];
                }
                if($value['productId']){
                    $value['products']['productId']=$value['productId'];
                }
                if($value['productName']){
                    $value['products']['name']=$value['productName'];
                }
                if($value['weight']){
                    $value['products']['weight']=$value['weight'];
                }
                if($value['promotion_price']){
                    $value['products']['price']=$value['promotion_price'];
                }
                if($value['productImg']){
                    $value['products']['image']=$basepath."product/".$value['productImg'];
                }
                if(!empty($cart_id)) {
                    $cartProductsData = TableRegistry::get('tbl_cart_products')->find()->select(['qty'=>'qty', 'product_id'=>'product_id'])->where(['product_id'=>$value["productId"], 'cart_id' => $cart_id, 'discount_id'=>$value['promotionId']])->toArray();
                }
                if(!empty($cartProductsData)) {
                    $productCart = $cartProductsData[0]['qty'];
                }
                $value['products']['availableStock'] = $value["totalStock"] - $productCart;
                unset($value['weight']);
                unset($value['promotion_price']); 
                array_push($promotion,$value);
                $no++;
            }
        }
        return $promotion;
    }

    public function updateStock($product_id, $quantity, $discount_id) {
        $promotion = $this->find("all")->where(['discount_id'=> $discount_id ])->toArray();
        $promotion_entity = $this->get($discount_id);
        $promotion_update=array(
            'total_stock'=>$promotion[0]['total_stock'] - $quantity,
            'updated_on'=>date("Y-m-d H:i:s")
        );
        $promotions_update = $this->patchEntity($promotion_entity, $promotion_update);
        $this->save($promotions_update);
    }
    
}
