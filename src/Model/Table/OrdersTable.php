<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\TblCartsTable|\Cake\ORM\Association\BelongsTo $TblCarts
 * @property \App\Model\Table\TblPaymentsTable|\Cake\ORM\Association\BelongsTo $TblPayments
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_orders');
        $this->setDisplayField('order_id');
        $this->setPrimaryKey('order_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
        $this->belongsTo('Payments', [
            'foreignKey' => 'reference_token',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('order_id')
            ->allowEmpty('order_id', 'create');

       /* $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->requirePresence('status', 'create')
            ->notEmpty('status');*/

        $validator
            ->dateTime('expected_delivery_time')
            ->allowEmpty('expected_delivery_time');

        $validator
            ->scalar('repay_type')
            ->maxLength('repay_type', 255)
            ->allowEmpty('repay_type');

        $validator
            ->numeric('repay_amount')
            ->allowEmpty('repay_amount');

        $validator
            ->scalar('repay_details')
            ->allowEmpty('repay_details');

        /*$validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->dateTime('modifed_on')
            ->requirePresence('modifed_on', 'create')
            ->notEmpty('modifed_on');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['cart_id'], 'TblCarts'));
       // $rules->add($rules->existsIn(['payment_id'], 'TblPayments'));

        return $rules;
    }
}
