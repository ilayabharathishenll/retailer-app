<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * AddToCart Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\AddToCart get($primaryKey, $options = [])
 * @method \App\Model\Entity\AddToCart newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AddToCart[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AddToCart|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AddToCart|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AddToCart patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AddToCart[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AddToCart findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AddToCartTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('add_to_cart');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('quantity')
            ->maxLength('quantity', 255)
            ->requirePresence('quantity', 'create')
            ->notEmpty('quantity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
    
     // �cartList� : [{
     //    �cartId� : ��,
     //    �quantity� : �� ,
     //    �product� : {
     //                 productId : 1234,
     //                 productName : Tomato,
     //                 productPrice : 250.00,
//     Array
// (
//     [pid] => 1
//     [price_per_kg] => 20
//     [product_name] => Pulse
//     [discount] => 10
// )
     //                 productDiscount : 10
     //    } 'productId'=> 'productPrice"]'=>     
     //    }]
    // ->select(['cartId'=>'AddToCart.id', 'quantity'=>'AddToCart.quantity'])
    //         ->select(['productId'=>'product.pid','productName'=>'product.product_name','productPrice'=>'product.price_per_kg','productDiscount'=>'product.discount'])

     public function listing($user_id){
        $query=$output=$result=array();
        $query['totalDiscount']=$query['totalAmount']=$query['totalProducts']=2;
        $query['cartList'] =$this->find()
            // ->select(['cartId'=>'AddToCart.id', 'quantity'=>'AddToCart.quantity',
            //    'product.pid','product.price_per_kg', 'product.product_name','product.discount'])
            ->select(['cartId'=>'AddToCart.id', 'quantity'=>'AddToCart.quantity',
               'productId'=>'product.pid','productName'=>'product.product_name','productPrice'=>'product.price_per_kg','productDiscount'=>'product.discount'])
            ->hydrate(false)
            ->join([
                'product' => [
                    'table' => 'product_details',
                    'type' => 'INNER',
                    'conditions' => 'product.pid=AddToCart.product_id',
                ]
            ])->where('AddToCart.user_id='.$user_id)->toArray();

        if(!empty($query['cartList'])){
            $cartList = array();
            foreach ($query['cartList'] as $values) {               
                    $values['product']['productId']=$values['productId'];
                    $values['product']['productName']=$values['productName'];
                    $values['product']['productPrice']=$values['productPrice'];
                    $values['product']['productDiscount']=$values['productDiscount'];
                    unset($values['productId']);
                    unset($values['productName']);
                    unset($values['productPrice']);
                    unset($values['productDiscount']);
                    $cartList[]=$values;               
            }            
        }
        $query['cartList'] = $cartList;
       //print_r($query);exit;        
        return $query;
    }
}
