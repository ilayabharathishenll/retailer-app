<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * Category Model
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 */
class CategoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tbl_categories');
        $this->setDisplayField('category_id');
        $this->setPrimaryKey('category_id');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_on' => 'new',
                    'updated_on' => 'always',
                ],
            ]
        ]);
        $this->hasMany('tbl_sub_categories', [
            'foreignKey' => 'category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('category_id')
            ->allowEmpty('category_id', 'create');

        $validator
            ->scalar('category_name')
            ->maxLength('category_name', 50)
            ->requirePresence('category_name', 'create')
            ->notEmpty('category_name');

        $validator
            ->scalar('image')
            ->maxLength('image', 150)
            ->allowEmpty('image');

        $validator
            ->scalar('enable_date')
            ->requirePresence('enable_date', 'create')
            ->notEmpty('enable_date');

        return $validator;
    }

    public function getCat() {
        $query = $this->find('all')->select(['category_id','category_name'])->toArray();
        return $query;
    }

    public function getCatInfo() {
        $query = $this->find('all')->select(['category_id','category_name','enable_date'])->toArray();
        $categories = array();
        if (!empty($query)) {
            foreach ($query as $category) {
                $cat['category_id'] = $category->category_id;
                $cat['category_name'] = $category->category_name;
                $cat['date_enabled'] = $category->enable_date;
                $subcategories = TableRegistry::get('tbl_sub_categories')->find()->where(['category_id'=>$category->category_id])->toArray();
                if (count($subcategories)>0) {
                   $cat['sub_category_available']=1;
                } else {
                    $cat['sub_category_available']=0;
                }
              
                $categories[] = $cat;
            }
        }
        return $categories;
    }
}
