<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Orders COD</span></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-2"><input class="form-control" type="text" placeholder="Shop Name" name="shop_name" value="<?php echo !empty($searchParams) ? $searchParams['shop_name'] : '' ?>"></div>
                <div class="col-md-2"><input class="form-control" type="text" placeholder="Ref No" name="reference_number" value="<?php echo !empty($searchParams) ? $searchParams['reference_number'] : '' ?>"></div>
                <div class="col-md-2"><input class="form-control" type="text" placeholder="Area" name="area" value="<?php echo !empty($searchParams) ? $searchParams['area'] : '' ?>"></div>
                <div class="col-md-5">
                    <button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button>
                    <a class="btn btn-success customresetbtn" title="Reset" href="<?php echo BASE_URL;?>/cod"><i class="fa fa-undo"></i> Reset</a>
                </div>
            <?= $this->Form->end() ?>
                <button class="btn btn-success edit-btn" title="" data-original-title="Edit">Edit <i class="fa fa-plus"></i></button>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="cash_on_delivery">
            <thead>
                <tr>
                    <th nowrap class="all">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" data-set="#cash_on_delivery .checkboxes" />
                            <span></span>
                        </label>
                    </th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('S No') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('Shop Name') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('Retailer Name') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('Phone') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('Ref No') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('cost') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('payment_type') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('area') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('transaction_time') ?></th>
                    <th nowrap scope="col"><?= $this->Paginator->sort('status') ?></th>
                    <th nowrap scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $s_no = 1;
                foreach ($orders as $order) { ?>
                <tr>
                    <td nowrap>
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="checkboxes" data-payment-id="<?= $order->payment_id;?>" value="1" />
                            <span></span>
                        </label>
                    </td>
                    <td nowrap><?= $this->Number->format($s_no) ?></td>
                    <td nowrap><?= h($order->shop->shop_name) ?></td>
                    <td nowrap><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                    <td nowrap><?= h($order->shop->phone) ?></td>
                    <td nowrap><?= h($order->reference_number) ?></td>
                    <td nowrap><?= h($order->transaction_amount) / 100 ?></td>
                    <td nowrap><?= h($order['payment_type'] == 'cod' ? "Cash on delivery" : $order['payment_type']) ?></td>
                    <td nowrap><?= h($order->shop->area) ?></td>
                    <td nowrap><?= h($order->transaction_date) ?></td>
                    <td nowrap><?= h($order->transaction_status) ?></td>
                    <td nowrap class="actions">
                        <a data-toggle="modal" data-value="view" data-target="#view_detail" title="View" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                         <h4 class="modal-title"><b>Order</b>
                                         <button type="button" class="btn green customsearchbtn text-right" style="    float: right;margin-right: 15px;" onclick="jQuery('.printContent').print()">
                                            <i class="fa fa-print"></i> Print
                                         </button>
                                         </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-horizontal">
                                            <table class=" table vertical-table table-bordered printContent">
                                                <tr>
                                                    <th scope="row"><?= __('Order Id') ?></th>
                                                    <td><?= h($order->cart_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Shop Name') ?></th>
                                                    <td><?= h($order->shop->shop_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Retailer Name') ?></th>
                                                    <td><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Email') ?></th>
                                                    <td><?= h($order->shop->email_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Mobile Number') ?></th>
                                                    <td><?= h($order->shop->phone) ?></td>
                                                </tr>
                                                <?php 
                                                 if (!empty($order["items"])) { 
                                                ?>
                                                <tr>
                                                    <th scope="row"><?= __('Products') ?></th>
                                                    <td>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                  <th>S.NO</th>
                                                                  <th>Name</th>
                                                                  <th>Qty (kg/ltr)</th>
                                                                  <th>Package Size</th>
                                                                  <th>Price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php 
                                                            $no=1;
                                                             foreach ($order["items"] as $products) { 
                                                            ?>
                                                                <tr>
                                                                  <td><?= $no?></td>
                                                                  <td><?= $products["productName"]?></td>
                                                                  <td><?= $products["quantity"]?></td>
                                                                  <td><?= $products["package_size"]?></td>
                                                                  <td><?= $products["total_amount"]?></td>
                                                                </tr>
                                                            <?php
                                                            $no++;
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                <tr>
                                                    <th scope="row"><?= __('Cost') ?></th>
                                                    <td><?= h($order->transaction_amount / 100) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Payment Type') ?></th>
                                                    <td><?= h($order->payment_type) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Transaction Time') ?></th>
                                                    <td><?= h($order->transaction_date) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Transaction Status') ?></th>
                                                    <td><?= h($order->transaction_status) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Area') ?></th>
                                                    <td><?= h($order->shop->area) ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php 
                    $s_no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>