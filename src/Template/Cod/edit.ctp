<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Orders COD</span></div>
    </div>
    <div class="portlet-body listing">
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="cash_on_delivery">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('S No') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Shop Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Retailer Name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Ref No') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('cost') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $s_no = 1;
                foreach ($orders as $order) { ?>
                <tr>
                    <td>
                        <?= $this->Number->format($s_no) ?>
                    </td>
                    <td>
                        <?= h($order->shop->shop_name) ?>
                    </td>
                    <td>
                        <?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?>
                    </td>
                    <td>
                        <?= h($order->reference_number) ?>
                    </td>
                    <td class="cost">
                        <?= h($order->transaction_amount) / 100 ?>
                    </td>
                </tr>
                <?php 
                    $s_no++;
                } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" style="text-align:right;"><b>Total:<b></td>
                    <td><b><i class="fa fa-inr" aria-hidden="true"><span class="total"></span>/-<b></td>
                </tr>
            </tfoot>
        </table>
        <div class="text-center">
            <fieldset>
            <?= $this->Form->create("", ['class'=>'payments']) ?>
                <div class="form-group">
                    <div class="col-md-4">
                        <div>
                            <div class="custom-date input-group date date-picker">
                                <input type="text" placeholder="DD/MM/YYYY" name="payment[transaction_date]" id="cod_update" class="form-control" data-validation="required" value=""  autocomplete="off">
                                <span class="input-group-btn"  style="vertical-align: top;">
                            <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                            </button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <select class="status form-control" name="payment[transaction_status]" value="">
                        <option value="">---Select Status---</option>
                        <option value="delivered">Delivered</option>
                        <option value="rejected">Rejected</option>
                        <option value="cancelled">Cancelled</option>
                        <option value="pending">Pending</option>
                    </select>
                </div>
                <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/cod"><i class="fa fa-angle-left"></i> Cancel</a>
            <?= $this->Form->end() ?>
            </fieldset>
        </div>
    </div>
</div>