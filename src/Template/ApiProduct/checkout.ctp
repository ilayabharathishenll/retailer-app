<?php
	// print_r($data);exit;
?>
<div class="header thankyou">
	<img src="<?php echo BASE_URL."/webroot/img/logo.png";?>">
	<p>Retailer App</p>
</div>
<div class="content thankyou text-center">
		<i class="fa fa-money"></i>
		<h3>PAY HERE</h3>
		<div class="deliver-status">
			<p>Please proceed payment by clicking the below button</p>
		</div>
		<div class="order-status">
			<form action="<?php echo BASE_URL."/api/payment/"?> method="POST">
				<script src="https://checkout.razorpay.com/v1/checkout.js"
				data-key="rzp_live_tpqmikVdLeaUZs"
				data-amount="<?php echo $data['amount']; ?>"
				data-buttontext="Proceed to Pay"
				data-name="<?php echo $data['name']; ?>"
				data-description="Purchase Description"
				data-image="<?php echo $data['logo']; ?>"
				data-prefill.name="<?php echo $data['name']; ?>"
				data-prefill.email="<?php echo $data['email']; ?>"
				data-theme.color="#F02044"
				data-notes.userId="<?php echo $data['userId']; ?>"
				data-notes.cart_id="<?php echo $data['cart_id']; ?>"
				data-notes.reference_token="<?php echo $data['reference_token']; ?>"
				></script>
				<input type="hidden" value="Hidden Element" name="hidden">
			</form>
		</div>
		<script type="text/javascript">
			document.querySelector('.razorpay-payment-button').click();
		</script>
</div>
<div style="padding: 0 15px;" class="header thankyou">
	<p> 2018 &copy; Retailer App  |  All Rights Reserved.</p>
</div>
