<?php
?>
<div class="header thankyou">
	<p>Retailer App</p>
</div>
<div class="content thankyou text-center">
	<?php if ($result['status'] != 'failed') {?>
		<i class="fa fa-check"></i>
		<h3>THANKYOU</h3>
		<p>for your purchase!</p>
		<div class="deliver-status">
			<p>We'll deliver your product soon</p>
		</div>
	<?php } else { ?>
		<i class="fa fa-times"></i>
		<h3>Error</h3>
		<p>Please contact our team</p>
		<div class="deliver-status">
			<p>Your payment has been faild</p>
		</div>
	<?php } ?>
	<div class="order-status">
		<p><strong>Order ID</strong></p>
		<h2><?php echo $result['id']; ?></h2>
	</div>
</div>
<div style="padding: 0 15px;" class="header thankyou">
	<p> 2018 &copy; Retailer App  |  All Rights Reserved.</p>
</div>
