<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $retailers
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Retailers</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Retailer" href="<?php echo BASE_URL;?>/retailer/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo !empty($searchParams) ? $searchParams['name'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Email" name="email_id" value="<?php echo !empty($searchParams) ? $searchParams['email_id'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Mobile" name="mobile" value="<?php echo !empty($searchParams) ? $searchParams['mobile'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Shop Name" name="shop_name" value="<?php echo !empty($searchParams) ? $searchParams['shop_name'] : '' ?>"></div>
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset" href="<?php echo BASE_URL;?>/retailer"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('area') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('shop_name') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($retailer as $retailer): ?>
                <tr>
                    <td><?= $this->Number->format($retailer->shop_id) ?></td>
                    <td><?= h($retailer->first_name) ?></td>
                    <td><?= h($retailer->email_id) ?></td>
                    <td><?= h($retailer->mobile) ?></td>
                    <td><?= h($retailer->area) ?></td>
                    <td><?= h($retailer->shop_name) ?></td>
                    <td class="actions">
                    <?= 
                        $this->Html->link(
                        '<i class="fa fa-edit"></i> ' .__('Edit'),
                        ['action' => 'edit', $retailer->shop_id],
                        ['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]
                    ) ?>
                    
                    <?= $this->Form->postLink('<i class="fa fa-trash"></i> ' .__('Delete'), ['action' => 'delete', $retailer->shop_id],['escape' => false, 'confirm' => __('Are you sure you want to delete {0}?', $retailer->first_name),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                    
                    <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" title="View" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                    <div class="hidden row_detail portlet box green">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b><?= ucfirst($retailer->shop_name) ?></b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <table class=" table vertical-table table-bordered">
                                            <tr>
                                                <th scope="row"><?= __('Id') ?></th>
                                                <td><?= $this->Number->format($retailer->shop_id) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Name') ?></th>
                                                <td><?= h($retailer->first_name) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Email') ?></th>
                                                <td><?= h($retailer->email_id) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Mobile') ?></th>
                                                <td><?= h($retailer->mobile) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Role') ?></th>
                                                <td><?= h($retailer->user_role) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Shop Name') ?></th>
                                            	<td><?= h($retailer->shop_name) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('Address1') ?></th>
                                            	<td><?= h($retailer->address1) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('Address2') ?></th>
                                            	<td><?= h($retailer->address2) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('Phone') ?></th>
                                            	<td><?= h($retailer->phone) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('Pincode') ?></th>
                                            	<td><?= h($retailer->pincode) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('City') ?></th>
                                            	<td><?= h($retailer->city) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('State') ?></th>
                                            	<td><?= h($retailer->state) ?></td>
                                        	</tr>
                                        	<tr>
                                                <th scope="row"><?= __('Country') ?></th>
                                            	<td><?= h($retailer->country) ?></td>
                                        	</tr>
                                            <tr>
                                                <th scope="row"><?= __('Area') ?></th>
                                                <td><?= h($retailer->area) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= h($retailer->proof_identity) ?> No</th>
                                                <td><?= h($retailer->gst_no) ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>