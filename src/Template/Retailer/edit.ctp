<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $retailer
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold">Edit Retailer</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/users"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body editing">
            <div class="form-horizontal">
                <?= $this->Form->create($retailer, array('enctype'=>'multipart/form-data', 'class'=>'retailer')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">First Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="first_name" placeholder="First Name" value="<?php echo $retailer['first_name']?>"/>
                            <input type="hidden" id="shop_id" value="<?php echo $retailer['shop_id']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="last_name">Last Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="last_name" placeholder="Last Name" value="<?php echo $retailer['last_name']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Owner Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="username" placeholder="Owner Name" value="<?php echo $retailer['username']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Email<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="email form-control" type="text" name="email_id" placeholder="Email" value="<?php echo $retailer['email_id']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Mobile<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="mobile form-control" type="text" name="mobile" placeholder="Mobile" value="<?php echo $retailer['mobile']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="user_role">
                                <option value="">--- Select Role ---</option>
                                <option value="retailer" <?= $retailer['user_role'] == "retailer" ? "selected='selected'" : "" ?>>Retailer</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone<span class="required"> </span></label>
                        <div class="col-md-5">
                            <input class="phone form-control" type="text" name="phone" placeholder="Phone" value="<?php echo $retailer['phone']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Shop Name<span class="required"> *</span></label> 
                        <div class="col-md-5">
                            <input class="shop_name form-control" type="text" name="shop_name" placeholder="Shop Name" value="<?php echo $retailer['shop_name']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Shop No<span class="required"> *</span></label> 
                        <div class="col-md-5">
                            <input class="shop_no form-control" type="text" name="shop_no" placeholder="Shop Number" value="<?php echo $retailer['shop_no']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address 1<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="address1 form-control" type="text" name="address1" placeholder="Address 1"><?php echo $retailer['address1']?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address 2<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="address2 form-control" type="text" name="address2" placeholder="Address 2"><?php echo $retailer['address2']?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pincode<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="pincode form-control" type="text" name="pincode" placeholder="Pincode" value="<?php echo $retailer['pincode']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">City<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="city form-control" type="text" name="city" placeholder="City" value="<?php echo $retailer['city']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">State<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="state form-control" type="text" name="state" placeholder="State" value="<?php echo $retailer['state']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Country<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="country form-control" type="text" name="country" placeholder="Country" value="<?php echo $retailer['country']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Area<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="area form-control" type="text" name="area" placeholder="Area" value="<?php echo $retailer['area']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Identity/Proof<span class="required"></span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="proof_identity" id="proof">
                                <option value="">--- Select Identity ---</option>
                                <option value="GST" <?= $retailer['proof_identity'] == "GST" ? "selected='selected'" : "" ?>>GST</option>
                                <option value="TIN" <?= $retailer['proof_identity'] == "TIN" ? "selected='selected'" : "" ?>>TIN</option>
                                <option value="Udyog Aadhar" <?= $retailer['proof_identity'] == "Udyog Aadhar" ? "selected='selected'" : "" ?>>Udyog Aadhar</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 appendlabel">GST No<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="gst form-control appendplaceholder" type="text" name="gst_no" placeholder="GST No" value="<?php echo $retailer['gst_no']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Logo<span class="required"> * </span></label>
                        <div class="col-md-5">
                            <img title="<?= $retailer->shop_logo ?>" alt="<?= $retailer->shop_logo ?>" class="image_prod" src="<?= BASE_URL.'/img/logo/'.$retailer->shop_logo ?>">
                            <span class="fa fa-times"></span>
                            <input class="hidden img_upload form-control" type="text" name="shop_logo" placeholder="Image Url" value="<?php echo $retailer['shop_logo']?>"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update </button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/retailer"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
</script>