<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold">Add Retailer</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/retailer"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($retailer, array('enctype'=>'multipart/form-data', 'class'=>'retailer')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">First Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="first_name" placeholder="First Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="last_name">Last Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="last_name" placeholder="Last Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Owner Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="username" placeholder="Owner Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="email form-control" type="text" name="email_id" placeholder="Email" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="password form-control" type="text" name="password" placeholder="Password" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Mobile<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="mobile form-control" type="text" name="mobile" placeholder="Mobile" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="user_role">
                                <option value="">--- Select Role ---</option>
                                <option value="Retailer">Retailer</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone<span class="required"> </span></label>
                        <div class="col-md-5">
                            <input class="mobile form-control" type="text" name="phone" placeholder="Phone" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Shop Name<span class="required"> *</span></label> 
                        <div class="col-md-5">
                            <input class="shop_name form-control" type="text" name="shop_name" placeholder="Shop Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Shop No<span class="required"> *</span></label> 
                        <div class="col-md-5">
                            <input class="shop_no form-control" type="text" name="shop_no" placeholder="Shop Number" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address 1<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="address1 form-control" type="text" name="address1" placeholder="Address 1"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address 2<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="address2 form-control" type="text" name="address2" placeholder="Address 2"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Pincode<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="pincode form-control" type="text" name="pincode" placeholder="Pincode" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">City<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="city form-control" type="text" name="city" placeholder="City" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">State<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="state form-control" type="text" name="state" placeholder="State" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Country<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="country form-control" type="text" name="country" placeholder="Country" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Area<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="area form-control" type="text" name="area" placeholder="Area" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Identity/Proof<span class="required"></span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="proof_identity" id="proof">
                                <option value="">--- Select Identity ---</option>
                                <option value="GST" <?= $retailer['proof_identity'] == "GST" ? "selected='selected'" : "" ?>>GST</option>
                                <option value="TIN" <?= $retailer['proof_identity'] == "TIN" ? "selected='selected'" : "" ?>>TIN</option>
                                <option value="Udyog Aadhar" <?= $retailer['proof_identity'] == "Udyog Aadhar" ? "selected='selected'" : "" ?>>Udyog Aadhar</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 appendlabel">GST No<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="gst form-control appendplaceholder" type="text" name="gst_no" placeholder="GST No" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
		                <label class="control-label col-md-3" for="first_name">Logo<span class="required"> * </span></label>
		                <div class="col-md-5">
		                    <input class="first_name form-control" type="file" name="shop_logo" value=""/>
		                </div>
		            </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Save </button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/retailer"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
</script>