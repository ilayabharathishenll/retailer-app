<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Setting</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/settings">Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($settings, array('class'=>'option')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="name" placeholder="Name" value="<?php echo $settings["name"]?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Value<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="value form-control" type="text" name="value" placeholder="Value" value="<?php echo $settings["value"]?>"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form" type="submit">Update</button>
                            <a class="btn btn-primary cancel-btn" title="Go back to listing page" href="<?php echo BASE_URL ?>/settings">Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
