<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Orders</span></div>
            <div class="export-csv pull-right">
            </div>
    </div>

    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Shop Name" name="shop_name" value="<?php echo !empty($searchParams) ? $searchParams['shop_name'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Ref No" name="reference_number" value="<?php echo !empty($searchParams) ? $searchParams['reference_number'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Area" name="area" value="<?php echo !empty($searchParams) ? $searchParams['area'] : '' ?>"></div>
                <div class="col-md-3 text-center"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset" href="<?php echo BASE_URL;?>/orders"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover" id="orders_table">
            <thead>
                <tr>
                    <th scope="col">S No</th>
                    <th scope="col">Shop Name</th>
                    <th scope="col">Retailer Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Ref No</th>
                    <th scope="col">Cost</th>
                    <th scope="col">Payment Type</th>
                    <th scope="col">Area</th>
                    <th scope="col">Transaction Date</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $s_no = 1;
                foreach ($orders as $order) { ?>
                <tr>
                    <td><?= $this->Number->format($s_no) ?></td>
                    <td><?= h($order->shop->shop_name) ?></td>
                    <td><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                    <td><?= h($order->shop->phone) ?></td>
                    <td><?= h($order->reference_number) ?></td>
                    <td><?= h($order->transaction_amount / 100) ?></td>
                    <td><?= h($order->payment_type) ?></td>
                    <td><?= h($order->shop->area) ?></td>
                    <td><?= h($order->transaction_date) ?></td>
                    <td><?php echo ($order->transaction_status=="authorized")?"Paid":$order->transaction_status; ?></td>
                    <td class="actions">
                        <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title"><b>Order <?= $this->Number->format($s_no) ?></b> <button type="button" class="btn green customsearchbtn text-right" style="    float: right;margin-right: 15px;" onclick="jQuery('.printContent').print()">
                                            <i class="fa fa-print"></i> Print
                                         </button></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-horizontal">
                                            <table class=" table vertical-table table-bordered printContent">
                                                <tr>
                                                    <th scope="row"><?= __('Order Id') ?></th>
                                                    <td><?= h($order->cart_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Shop Name') ?></th>
                                                    <td><?= h($order->shop->shop_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Retailer Name') ?></th>
                                                    <td><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Email') ?></th>
                                                    <td><?= h($order->shop->email_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Mobile Number') ?></th>
                                                    <td><?= h($order->shop->phone) ?></td>
                                                </tr>                                             
                                                <?php 
                                                 if (!empty($order["items"])) { 
                                                ?>
                                                <tr>
                                                    <th scope="row"><?= __('Products') ?></th>
                                                    <td>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                  <th>S.NO</th>
                                                                  <th>Name</th>
                                                                  <th>Qty (kg/ltr)</th>
                                                                   <th>Package Size</th>
                                                                  <th>Price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php 
                                                            $no=1;
                                                             foreach ($order["items"] as $products) { 
                                                            ?>
                                                                <tr>
                                                                  <td><?= $no?></td>
                                                                  <td><?= $products["productName"]?></td>
                                                                  <td><?= $products["quantity"]?></td>
                                                                  <td><?= $products["package_size"]?></td>
                                                                  <td><?= $products["total_amount"]?></td>
                                                                </tr>
                                                            <?php
                                                            $no++;
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                <tr>
                                                    <th scope="row"><?= __('Cost') ?></th>
                                                    <td><?= h($order->transaction_amount / 100) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Payment Type') ?></th>
                                                    <td><?= h($order->payment_type) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Transaction Time') ?></th>
                                                    <td><?= h($order->transaction_date) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Transaction Status') ?></th>
                                                    <td><?php echo ($order->transaction_status=="authorized")?"Paid":$order->transaction_status; ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Area') ?></th>
                                                    <td><?= h($order->shop->area) ?></td>
                                                </tr>
                                                
                                            </table>
                                            <?php  if (!empty($order->cart_products)) { ?>
                                                <table class="table table-bordered table-striped table-condensed  table-body-view">
                                                    <tbody>
                                                    <tr>
                                                        <th colspan="6" style="text-align: center;">Product Details</th>
                                                    </tr>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Product Name</th>
                                                        <th>Quantity</th>
                                                        <th>Unit Total (<i class="fa fa-inr" aria-hidden="true"></i>)
                                                        </th>
                                                        <th>Discount Amount (<i class="fa fa-inr"
                                                                                aria-hidden="true"></i>)</th>
                                                        <th>Total Amount (<i class="fa fa-inr"
                                                                             aria-hidden="true"></i>)</th>
                                                    </tr>
                                                    <?php $sno=1; ?>
                                                    <?php foreach ($order->cart_products as $products): ?>
                                                        <tr>
                                                            <td><?= $this->Number->format($sno) ?></td>
                                                            <td><?= h($products->tbl_product->product_name) ?></td>
                                                            <td><?= h($products->qty) ?></td>
                                                            <td><i class="fa fa-inr"
                                                                   aria-hidden="true"></i> <?= $products->unit_total
                                                                    ? $products->unit_total : 0;
                                                                ?></td>
                                                            <td><i class="fa fa-inr"
                                                                   aria-hidden="true"></i> <?=
                                                                $products->discount_amount ?
                                                                    $products->discount_amount : 0; ?></td>
                                                            <td><i class="fa fa-inr"
                                                                   aria-hidden="true"></i> <?=
                                                                $products->total_amount ? $products->total_amount
                                                             : 0;   ?></td>
                                                        </tr>
                                                        <?php $sno++;?>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
                    $s_no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>
