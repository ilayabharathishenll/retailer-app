<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
// print_r($order);exit;
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Order</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/orders"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($order) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Cost<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="cost form-control" type="text" name="payment[transaction_amount]" placeholder="Cost" value="<?php echo $order['payment']['transaction_amount']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Payment Type<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="payment_type form-control" type="text" name="payment[payment_type]" placeholder="Payment Type" value="<?php echo $order['payment']['payment_type']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Transaction Status<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="payment_type form-control" type="text" name="payment[transaction_status]" placeholder="Transaction Status" value="<?php echo $order['payment']['transaction_status']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">status<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="status form-control" name="status">
                                <option vlaue="">--Select Status--</option>
                                <option vlaue="Ordered" <?php echo $order['status'] == "Ordered" ? 'selected="selected"' : "" ?>>Ordered</option>
                                <option vlaue="Cancel" <?php echo $order['status'] == "Cancel" ? 'selected="selected"' : "" ?>>Cancel</option>
                                <option vlaue="Accept" <?php echo $order['status'] == "Accept" ? 'selected="selected"' : "" ?>>Accept</option>
                                <option vlaue="Shipping" <?php echo $order['status'] == "Shipping" ? 'selected="selected"' : "" ?>>Shipping</option>
                                <option vlaue="Delivered" <?php echo $order['status'] == "Delivered" ? 'selected="selected"' : "" ?>>Delivered</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/orders"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
            
        
</div>
