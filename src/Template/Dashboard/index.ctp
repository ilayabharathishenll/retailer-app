<?php
$interval='';$today=date('Y-m-d H:i:s');
function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);
        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
    }
?>

<style>
    .feeds li .col2 {
        width: 120px;
        margin-left: -123px;
    }
</style>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?= $carts ?>"><?= $carts ?></span>
                </div>
                <div class="desc"> Total orders </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?= $activeUsersCount ?>"><?= $activeUsersCount ?></span></div>
                <div class="desc"> Active Users </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                    <!-- <span data-counter="counterup" data-value="<?php echo ($usersCount>0)?$usersCount:"0"?>"><?php echo ($usersCount>0)?$usersCount:"0"?></span> -->
                    <span data-counter="counterup" data-value="<?= $usersCount ?>"><?= $usersCount ?></span>
                </div>
                <div class="desc">  Total Shops </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-inr"></i>
            </div>
            <div class="details">
                <div class="number">
                    ₹ <span data-counter="counterup" data-value="<?= $total_order_price ?>"><?= $total_order_price ?></span></div>
                <div class="desc"> Total Revenue </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="recent">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase recent-activity">Recent Activities</span>
                </div>
            </div>
            <div class="portlet-body listing">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                    <div class="scroller" style="height: auto; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible="0" data-initialized="1">
                        <ul class="feeds">
                            <?php 
                            if (count($RetailerDetails)>0) {
                                foreach($RetailerDetails as $NewRetailerData) {
                            ?>
                            <li>
                                <a href="javascript:;" class="show_shop_table" id="newshopRegister">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-warning">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    New Retailer Registered -
                                                    <span class="label label-sm label-warning ">
                                                       <?php echo $NewRetailerData->first_name;?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php
                                }
                            }
                            ?>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column new_shop_hide" id="driver-reg" style="display:none;">
                                <thead>
                                    <tr><th>S.NO</th><th>Name</th><th>Email</th><th>Mobile</th><th>Area</th> <th>Shop Name</th> <th>Action</th></tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(count($RetailerTablesDetails)>0) {
                                        $sno=1;
                                        foreach ($RetailerTablesDetails as $shopdata) {
                                ?> 
                                    <tr>
                                        <td><?php echo $sno++ ?></td>
                                        <td><?php echo $shopdata->first_name ?></td>
                                        <td><?php echo $shopdata->email_id ?></td>
                                        <td><?php echo $shopdata->mobile ?></td>
                                        <td><?php echo $shopdata->area ?></td>
                                        <td><?php echo $shopdata->shop_name ?></td>
                                        <td class="actions"> <a data-toggle="modal" title="View" data-value="view" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                                            <div class="hidden row_detail portlet box green">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title"><b><?= ucfirst($shopdata->shop_name) ?></b></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-horizontal">
                                                                <table class=" table vertical-table table-bordered">
                                                                    <tr>
                                                                        <th scope="row"><?= __('Id') ?></th>
                                                                        <td><?= $this->Number->format($shopdata->shop_id) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Name') ?></th>
                                                                        <td><?= h($shopdata->first_name) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Email') ?></th>
                                                                        <td><?= h($shopdata->email_id) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Mobile') ?></th>
                                                                        <td><?= h($shopdata->mobile) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Role') ?></th>
                                                                        <td><?= h($shopdata->user_role) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Shop Name') ?></th>
                                                                        <td><?= h($shopdata->shop_name) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Address1') ?></th>
                                                                        <td><?= h($shopdata->address1) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Address2') ?></th>
                                                                        <td><?= h($shopdata->address2) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Phone') ?></th>
                                                                        <td><?= h($shopdata->phone) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Pincode') ?></th>
                                                                        <td><?= h($shopdata->pincode) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('City') ?></th>
                                                                        <td><?= h($shopdata->city) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('State') ?></th>
                                                                        <td><?= h($shopdata->state) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Country') ?></th>
                                                                        <td><?= h($shopdata->country) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= __('Area') ?></th>
                                                                        <td><?= h($shopdata->area) ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th scope="row"><?= h($shopdata->proof_identity) ?> No</th>
                                                                        <td><?= h($shopdata->gst_no) ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                   <!--   -->
                                </tbody>
                            </table>
                            <?php 
                            if (count($orderdetails)>0) {
                                foreach($orderdetails as $NewOdederData) {
                            ?>
                            <li>
                                <a href="javascript:;" class="show_order_table" id="newlatestOrder">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    New Order Received -
                                                    <span class="label label-sm label-info ">
                                                       Online Payment
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                             <?php
                                }
                            }
                            ?>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column new_order_hide" id="driver-order" style="display:none;">
                                <thead>
                                    <tr><th>S.NO</th><th>Shop Name</th><th>Retailer Name</th><th>Phone</th><th>Ref No</th> <th>Cost</th><th>Payment Type</th><th>Area</th><th>Transaction Date</th><th>Status</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <?php
                                    if (count($orders)>0) {
                                        $s_no=1;
                                        foreach($orders as $order) {
                                ?>
                                            <tr>
                                                <td><?= $this->Number->format($s_no) ?></td>
                                                <td><?= h($order->shop->shop_name) ?></td>
                                                <td><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                                                <td><?= h($order->shop->phone) ?></td>
                                                <td><?= h($order->reference_number) ?></td>
                                                <td><?= h($order->transaction_amount )/ 100 ?></td>
                                                <td><?= h($order->payment_type) ?></td>
                                                <td><?= h($order->shop->area) ?></td>
                                                <td><?= h($order->transaction_date) ?></td>
                                                <td><?php echo ($order->transaction_status=="authorized")?"Paid":$order->transaction_status; ?></td>
                                                <td class="actions">
                                                    <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                                                    <div class="hidden row_detail portlet box green">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title"><b>Order</b> <button type="button" class="btn green customsearchbtn text-right" style="    float: right;margin-right: 15px;" onclick="jQuery('.printContentOnline').print()"><i class="fa fa-print"></i> Print</button></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-horizontal">
                                                                        <table class=" table vertical-table table-bordered printContentOnline">
                                                                            <tr>
                                                                                <th scope="row"><?= __('S No') ?></th>
                                                                                <td><?= $this->Number->format($s_no) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Shop Name') ?></th>
                                                                                <td><?= h($order->shop->shop_name) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Retailer Name') ?></th>
                                                                                <td><?= h($order->shop->first_name. ' ' .$order->shop->last_name) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Email') ?></th>
                                                                                <td><?= h($order->shop->email_id) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Mobile Number') ?></th>
                                                                                <td><?= h($order->shop->phone) ?></td>
                                                                            </tr>
                                                                            <?php 
                                                                             if (!empty($order["items"])) { 
                                                                            ?>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Products') ?></th>
                                                                                <td>
                                                                                    <table class="table">
                                                                                        <thead>
                                                                                            <tr>
                                                                                              <th>S.NO</th>
                                                                                              <th>Name</th>
                                                                                              <th>Qty (kg/ltr)</th>
                                                                                              <th>Package Size</th>
                                                                                              <th>Price</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <?php 
                                                                                        $no=1;
                                                                                         foreach ($order["items"] as $products) { 
                                                                                        ?>
                                                                                            <tr>
                                                                                              <td><?= $no?></td>
                                                                                              <td><?= $products["productName"]?></td>
                                                                                              <td><?= $products["quantity"]?></td>
                                                                                              <td><?= $products["package_size"]?></td>
                                                                                              <td><?= $products["total_amount"]?></td>
                                                                                            </tr>
                                                                                        <?php
                                                                                        $no++;
                                                                                        }
                                                                                        ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Cost') ?></th>
                                                                                <td><?= h($order->transaction_amount )/ 100 ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Payment Type') ?></th>
                                                                                <td><?= h($order->payment_type) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Transaction Time') ?></th>
                                                                                <td><?= h($order->transaction_date) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Transaction Status') ?></th>
                                                                                <td><?php echo ($order->transaction_status=="authorized")?"Paid":$order->transaction_status; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Area') ?></th>
                                                                                <td><?= h($order->shop->area) ?></td>
                                                                            </tr>
                                                                        </table>
                                                                        <?php  if (!empty($order->cart_products)) { ?>
                                                                            <table class="table table-bordered table-striped table-condensed  table-body-view printContentOnline">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th colspan="6" style="text-align: center;">Product Details</th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Product Name</th>
                                                                                    <th>Quantity</th>
                                                                                    <th>Unit Total (<i class="fa fa-inr" aria-hidden="true"></i>)
                                                                                    </th>
                                                                                    <th>Discount Amount (<i class="fa fa-inr"
                                                                                                            aria-hidden="true"></i>)</th>
                                                                                    <th>Total Amount (<i class="fa fa-inr"
                                                                                                         aria-hidden="true"></i>)</th>
                                                                                </tr>
                                                                                <?php $sno=1; ?>
                                                                                <?php foreach ($order->cart_products as $products): ?>
                                                                                    <tr>
                                                                                        <td><?= $this->Number->format($sno) ?></td>
                                                                                        <td><?= h($products->tbl_product->product_name) ?></td>
                                                                                        <td><?= h($products->qty) ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?= $products->unit_total
                                                                                                ? $products->unit_total : 0;
                                                                                            ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?=
                                                                                            $products->discount_amount ?
                                                                                                $products->discount_amount : 0; ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?=
                                                                                            $products->total_amount ? $products->total_amount
                                                                                         : 0;   ?></td>
                                                                                    </tr>
                                                                                    <?php $sno++;?>
                                                                                <?php endforeach; ?>
                                                                                </tbody>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                      <?php
                                        $s_no++;
                                        }
                                    }
                                ?>
                                   <!--   -->
                                </tbody>
                            </table>
                            <?php 
                            if (count($codorderdetails)>0) {
                                foreach($codorderdetails as $NewCodOdederData) {
                            ?>
                            <li>
                                <a href="javascript:;" class="show_codorder_table" id="newcodlatestOrder">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    New Order Received -
                                                    <span class="label label-sm label-success ">
                                                       COD
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php
                                }
                            }
                            ?>
                             <table class="table table-striped table-bordered table-hover table-checkable order-column new_codorder_hide" id="driver-cod" style="display:none;">
                                <thead>
                                    <tr><th>S.NO</th><th>Shop Name</th><th>Retailer Name</th><th>Phone</th> <th>Cost</th><th>Payment Type</th><th>Area</th><th>Transaction Date</th><th>Status</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <?php
                                    if (count($codorders)>0) {
                                        $s_no=1;
                                        foreach($codorders as $orderscod) {
                                ?>
                                            <tr>
                                                <td><?= $this->Number->format($s_no) ?></td>
                                                <td><?= h($orderscod->shop->shop_name) ?></td>
                                                <td><?= h($orderscod->shop->first_name. ' ' .$orderscod->shop->last_name) ?></td>
                                                <td><?= h($orderscod->shop->phone) ?></td>
                                                <!-- <td><?= h($orderscod->reference_number) ?></td> -->
                                                <td><?= h($orderscod->transaction_amount / 100) ?></td>
                                                <td><?= h($orderscod->payment_type) ?></td>
                                                <td><?= h($orderscod->shop->area) ?></td>
                                                <td><?= h($orderscod->transaction_date) ?></td>
                                                <td><?php echo ($orderscod->transaction_status=="delivered")?"Paid":$orderscod->transaction_status; ?></td>
                                                <td class="actions">
                                                    <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                                                    <div class="hidden row_detail portlet box green">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title"><b>Order</b> <button type="button" class="btn green customsearchbtn text-right" style="    float: right;margin-right: 15px;" onclick="jQuery('.printContent').print()"><i class="fa fa-print"></i> Print</button></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-horizontal">
                                                                        <table class=" table vertical-table table-bordered printContent">
                                                                            <tr>
                                                                                <th scope="row"><?= __('Order Id') ?></th>
                                                                                <td><?= h($orderscod->cart_id) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Shop Name') ?></th>
                                                                                <td><?= h($orderscod->shop->shop_name) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Retailer Name') ?></th>
                                                                                <td><?= h($orderscod->shop->first_name. ' ' .$orderscod->shop->last_name) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Email') ?></th>
                                                                                <td><?= h($orderscod->shop->email_id) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Mobile Number') ?></th>
                                                                                <td><?= h($orderscod->shop->phone) ?></td>
                                                                            </tr>
                                                                            <?php 
                                                                             if (!empty($orderscod["items"])) { 
                                                                            ?>
                                                                             
                                                                            <tr>
                                                                                <th scope="row"><?= __('Products') ?></th>
                                                                                <td>
                                                                                    <table class="table">
                                                                                        <thead>
                                                                                            <tr>
                                                                                              <th>S.NO</th>
                                                                                              <th>Name</th>
                                                                                              <th>Qty (kg/ltr)</th>
                                                                                              <th>Package Size</th>
                                                                                              <th>Price</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <?php 
                                                                                        $nos=1;
                                                                                         foreach ($orderscod["items"] as $product) { 
                                                                                        ?>
                                                                                            <tr>
                                                                                              <td><?= $nos?></td>
                                                                                              <td><?= $product["productName"]?></td>
                                                                                              <td><?= $product["quantity"]?></td>
                                                                                               <td><?= $product["package_size"]?></td>
                                                                                              
                                                                                              <td><?= $product["total_amount"]?></td>
                                                                                            </tr>
                                                                                        <?php
                                                                                        $nos++;
                                                                                        }
                                                                                        ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Cost') ?></th>
                                                                                <td><?= h($orderscod->transaction_amount / 100) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Payment Type') ?></th>
                                                                                <td><?= h($orderscod->payment_type) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Transaction Time') ?></th>
                                                                                <td><?= h($orderscod->transaction_date) ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Transaction Status') ?></th>
                                                                                <td><?php echo ($orderscod->transaction_status=="authorized")?"Paid":$orderscod->transaction_status; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="row"><?= __('Area') ?></th>
                                                                                <td><?= h($orderscod->shop->area) ?></td>
                                                                            </tr>
                                                                        </table>
                                                                        <?php  if (!empty($orderscod->cart_products)) { ?>
                                                                            <table class="table table-bordered table-striped table-condensed  table-body-view printContent">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th colspan="6" style="text-align: center;">Product Details</th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Product Name</th>
                                                                                    <th>Quantity</th>
                                                                                    <th>Unit Total (<i class="fa fa-inr" aria-hidden="true"></i>)
                                                                                    </th>
                                                                                    <th>Discount Amount (<i class="fa fa-inr"
                                                                                                            aria-hidden="true"></i>)</th>
                                                                                    <th>Total Amount (<i class="fa fa-inr"
                                                                                                         aria-hidden="true"></i>)</th>
                                                                                </tr>
                                                                                <?php $sno=1; ?>
                                                                                <?php foreach ($orderscod->cart_products as $products): ?>
                                                                                    <tr>
                                                                                        <td><?= $this->Number->format($sno) ?></td>
                                                                                        <td><?= h($products->tbl_product->product_name) ?></td>
                                                                                        <td><?= h($products->qty) ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?= $products->unit_total
                                                                                                ? $products->unit_total : 0;
                                                                                            ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?=
                                                                                            $products->discount_amount ?
                                                                                                $products->discount_amount : 0; ?></td>
                                                                                        <td><i class="fa fa-inr"
                                                                                               aria-hidden="true"></i> <?=
                                                                                            $products->total_amount ? $products->total_amount
                                                                                         : 0;   ?></td>
                                                                                    </tr>
                                                                                    <?php $sno++;?>
                                                                                <?php endforeach; ?>
                                                                                </tbody>
                                                                            </table>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                      <?php
                                        $s_no++;
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </ul>
                    </div>
                </div>
                <div class="scroller-footer">
                    <div class="btn-arrow-link pull-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>