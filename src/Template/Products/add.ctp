<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\product $product
 */
?>
<style>
 .checkbox, .form-horizontal .checkbox {
   padding: 3px 25px 3px 25px !important;
}
.multiselect-container{
  width:100%;
}
</style>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Product</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/products"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body">
        <div class="form-horizontal">
            <?= $this->Form->create($product, array('enctype'=>'multipart/form-data', 'class'=>'products')) ?>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Name<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="product_name" placeholder="Name" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Price per kg/ltr<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="price" placeholder="Price per kg/ltr" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Status<span class="required"> *</span></label>
                <div class="col-md-5">
                    <select class="form-control" name="status">
                        <option value="Active">Active</option>
                        <option value="Deactive">Deactive</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="category_id">Category<span class="required"> *</span></label>
                <div class="col-md-5">
                    <?php if(!empty($categories)) { ?>
                        <select name="category_id" id="category_id" class="form-control search_opt">
                                <?php foreach ($categories as $category) { ?>
                                    <option value="<?php echo $category['category_id']?>"><?php echo $category['category_name']; ?></option>
                                <?php } ?>
                        </select>
                    <?php } else { ?>
                        <select name="" class="form-control">
                            <option value="">Please create categories</option>
                        </select>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group enable-sub-category-remove">
                <label class="control-label col-md-3" for="category_id">Sub Category<span class="required"> *</span></label>
                <div class="col-md-5">
                        <select name="sub_category_id" id="sub_category_id" class="form-control">
                        </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Total Stock<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="total_stock" placeholder="Total Stock" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Unit<span class="required" aria-required="true"> *</span></label>
                <div class="col-md-5">
                    <select class="form-control" name="unit" id="unit">
                        <option value="">--- Select Unit ---</option>
                        <option value="Kg">Kg</option>
                        <option value="Liter">Liter</option>
                        <option value="Count">Count</option>
                    </select>
                </div>
            </div>
             <div class="form-group enable-available-show" style="display:none;">
                <label class="control-label col-md-3">Available Option<span class="required" aria-required="true"> *</span></label>
                <div class="col-md-5">
                    <select class="form-control" name="available_unit[]" id="available_unit" multiple>
                        <option class="grams" value="250 Grams">250 Grams</option>
                        <option class="grams" value="500 Grams">500 Grams</option>
                        <option class="grams" value="1 Kg">1 Kg</option>
                        <option class="grams" value="2 Kg">2 Kg</option>
                        <option class="grams" value="5 Kg">5 Kg</option>
                        <option class="grams" value="10 Kg">10 Kg</option>
                        <option class="grams" value="20 Kg">20 Kg</option>
                        <option class="grams" value="25 Kg">25 Kg</option>
                        <option class="grams" value="50 Kg">50 Kg</option>
                        <option class="ml" value="250 ML">250 ML</option>
                        <option class="ml" value="500 ML">500 ML</option>
                        <option class="ml" value="1000 ML">1000 ML</option>
                    </select>
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Date From<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="form-control from" data-validation="required" type="text" value="" name="from_date" id="from_date" placeholder="Date From">
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Date To<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="to form-control" type="text" name="to_date" placeholder="Date To" value="" id="to_date" />
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Start Time<span class="required"> *</span></label>
                
                <div class='col-md-5' id='datetimepicker3'>
                    <input class="form-control" type="text" name="start_time" placeholder="Start Time" value="" id="start_time" />
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">End Time<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="end_time form-control" type="text" name="end_time" placeholder="End Time" value="" id="end_time" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Minimum<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="minimum" placeholder="Minimum" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Maximum<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="maximum" placeholder="Maximum" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Multiply<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="multiply" placeholder="Multiply" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Image<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="file" name="image" value=""/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Save</button>
                    <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/products"><i class="fa fa-angle-left"></i> Cancel</a>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
</script>