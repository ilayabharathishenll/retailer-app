<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\product $product
 */
$date_from = ($product['from_date'] != null) ? date("d/m/Y", strtotime($product['from_date'])) : "";
$date_to = ($product['to_date'] != null) ? date("d/m/Y", strtotime($product['to_date'])) : "";
$time_from = ($product['start_time'] != null) ? date("H:i:s", strtotime($product['start_time'])) : "";
$time_to = ($product['end_time'] != null) ? date("H:i:s", strtotime($product['end_time'])) : "";
$availableArray=($product['available_option']!="" || $product['available_option']!=null)? json_decode($product['available_option']):"";
//print_r($availableArray);
?>
<style>
 .checkbox, .form-horizontal .checkbox {
   padding: 3px 25px 3px 25px !important;
}
.multiselect-container{
  width:100%;
}
</style>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Product</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/products"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
    <div class="portlet-body editing">
        <div class="form-horizontal">

            <?= $this->Form->create($product, array('enctype'=>'multipart/form-data', 'class'=>'products')) ?>
             <input type="hidden" name="hndsubcateid" id="hndsubcateid" value="<?php echo $product['sub_category_id']?>">
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Name<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="product_name" placeholder="Name" value="<?php echo $product['product_name']?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Price per kg/ltr<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="price" placeholder="Price per kg/ltr" value="<?php echo $product['price']?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="discount_type">Status<span class="required"> *</span></label>
                <div class="col-md-5">
                    <select class="discount_type form-control" type="text" name="status">
                        <option value="">Select</option>
                        <option <?php echo !empty($product) && $product['status'] == 'Active' ? 'selected = "selected"' : '' ?> value="Active">Active</option>
                        <option <?php echo !empty($product) && $product['status'] == 'Deactive' ? 'selected = "selected"' : '' ?> value="Deactive">Deactive</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="category_id">Category<span class="required"> *</span></label>
                <div class="col-md-5">
                    <?php if(!empty($categories)) { ?>
                        <select name="category_id" id="category_id" class="form-control search_opt">
                                <?php foreach ($categories as $category) { ?>
                                    <option value="<?php echo $category['category_id']?>" <?php if($product['category_id'] == $category['category_id']){echo 'selected';}?>><?php echo $category['category_name']; ?></option>
                                <?php } ?>
                        </select>
                    <?php } else { ?>
                        <select name="" class="form-control">
                            <option value="">Please create categories</option>
                        </select>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group enable-sub-category-remove">
                <label class="control-label col-md-3" for="category_id">Sub Category<span class="required"> *</span></label>
                <div class="col-md-5">
                        <select name="sub_category_id" id="sub_category_id" class="form-control">
                        </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Total Stock<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="total_stock" placeholder="Total Stock" value="<?php echo $product['total_stock']?>"/>
                </div>
            </div>

             <div class="form-group">
                <label class="control-label col-md-3">Unit<span class="required" aria-required="true"> *</span></label>
                <div class="col-md-5">
                    <select class="form-control" name="unit" id="unit">
                        <option value="">--- Select Unit ---</option>
                        <option value="Kg" <?php echo !empty($product) && $product['unit'] == 'Kg' ? 'selected = "selected"' : '' ?>>Kg</option>
                        <option value="Liter" <?php echo !empty($product) && $product['unit'] == 'Liter' ? 'selected = "selected"' : '' ?>>Liter</option>
                        <option value="Count" <?php echo !empty($product) && $product['unit'] == 'Count' ? 'selected = "selected"' : '' ?>>Count</option>
                    </select>
                </div>
            </div>
             <div class="form-group enable-available-show" style="display:none;">
                <label class="control-label col-md-3">Available Option<span class="required" aria-required="true"> *</span></label>
                <div class="col-md-5">
                    <select class="form-control" name="available_unit[]" id="available_unit" multiple>

                        <option class="grams" value="250 Grams" <?php echo in_array("250 Grams", $availableArray)?"selected":"";?>>250 Grams</option>
                        <option class="grams" value="500 Grams" <?php echo in_array("500 Grams", $availableArray)?"selected":"";?>>500 Grams</option>
                       <option class="grams" value="1 Kg" <?php echo in_array("1 Kg", $availableArray)?"selected":"";?>>1 Kg</option>
                        <option class="grams" value="2 Kg" <?php echo in_array("2 Kg", $availableArray)?"selected":"";?>>2 Kg</option>
                        <option class="grams" value="5 Kg" <?php echo in_array("5 Kg", $availableArray)?"selected":"";?>>5 Kg</option>
                        <option class="grams" value="10 Kg" <?php echo in_array("10 Kg", $availableArray)?"selected":"";?>>10 Kg</option>
                        <option class="grams" value="20 Kg" <?php echo in_array("20 Kg", $availableArray)?"selected":"";?>>20 Kg</option>
                        <option class="grams" value="25 Kg" <?php echo in_array("25 Kg", $availableArray)?"selected":"";?>>25 Kg</option>
                        <option class="grams" value="50 Kg" <?php echo in_array("50 Kg", $availableArray)?"selected":"";?>>50 Kg</option>
                        <option class="ml" value="250 ML" <?php echo in_array("250 ML", $availableArray)?"selected":"";?>>250 ML</option>
                        <option class="ml" value="500 ML" <?php echo in_array("500 ML", $availableArray)?"selected":"";?>>500 ML</option>
                        <option class="ml" value="1000 ML" <?php echo in_array("1000 ML", $availableArray)?"selected":"";?>>1000 ML</option>
                    </select>
                </div>
            </div>

             <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Date From<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="form-control from" data-validation="required" type="text" value="<?php echo $date_from ?>" name="from_date" id="from_date" placeholder="Date From">
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Date To<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="to form-control" type="text" name="to_date" value="<?php echo $date_to ?>" id="to_date" />
                </div>
            </div>
            <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">Start Time<span class="required"> *</span></label>
                
                <div class='col-md-5' id='datetimepicker3'>
                    <input class="form-control" type="text" name="start_time" value="<?php echo $time_from?>" id="start_time" />
                </div>
            </div>

             <div class="form-group enable-date-off-remove">
                <label class="control-label col-md-3" for="first_name">End Time<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="end_time form-control" type="text" name="end_time" value="<?php echo $time_to; ?>" id="end_time" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Minimum<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="form-control" type="text" name="minimum" placeholder="Minimum" value="<?php echo $product['minimum'] ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Maximum<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="maximum" placeholder="Maximum" value="<?php echo $product['maximum'] ?>"/>
                </div>
            </div>

             <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Multiply<span class="required"> *</span></label>
                <div class="col-md-5">
                    <input class="first_name form-control" type="text" name="multiply" placeholder="Multiply" value="<?php echo $product['multiply'] ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3" for="first_name">Image<span class="required"> *</span></label>
                <div class="col-md-5">
                    <img title="<?= $product->image ?>" alt="<?= $product->image ?>" class="image_prod" src="<?= BASE_URL.'/img/product/'.$product->image ?>">
                    <span class="fa fa-times"></span>
                    <input class="hidden img_upload form-control" type="text" name="image" placeholder="Image Url" value="<?php echo $product['image']?>"/>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                    <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/products"><i class="fa fa-angle-left"></i> Cancel</a>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
</script>