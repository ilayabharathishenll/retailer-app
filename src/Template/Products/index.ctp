<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\product[]|\Cake\Collection\CollectionInterface $products
 */
// print_r($products);exit;
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Products</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Product" href="<?php echo BASE_URL;?>/products/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo !empty($searchParams) ? $searchParams['name'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Price" name="price" value="<?php echo !empty($searchParams) ? $searchParams['price'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Total Stock" name="stock" value="<?php echo !empty($searchParams) ? $searchParams['stock'] : '' ?>"></div>
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset"  href="<?php echo BASE_URL;?>/products"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('pid') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('product_name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('price_per_kg/ltr') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('total_stock') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('date_from') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('date_to') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                <tr>
                    <td><?= $this->Number->format($product->product_id) ?></td>
                    <td><?= h($product->product_name) ?></td>
                    <td><?= h($product->price) ?></td>
                    <td><?= h($product->total_stock) ?></td>
                    <td><?= ($product->from_date != null) ? date_format($product->from_date, 'd/m/Y') : "-" ?></td>
                    <td><?= ($product->to_date != null) ? date_format($product->to_date, 'd/m/Y') : "-" ?></td>
                    <td><img title="<?= $product->image ?>" alt="<?= $product->image ?>" class="image_prod" src="<?= BASE_URL.'/img/product/'.$product->image ?>"></td>
                    <td class="actions">
                        <?= $this->Html->link('<i class="fa fa-edit"></i> ' .__('Edit'), ['action' => 'edit', $product->product_id],['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]) ?>
                        <?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $product->product_id], ['escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $product->product_id),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                        <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b><?= ucfirst($product->product_name) ?></b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-horizontal">
                                            <table class=" table vertical-table table-bordered">
                                                <tr>
                                                    <th scope="row"><?= __('Id') ?></th>
                                                    <td><?= $this->Number->format($product->product_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Name') ?></th>
                                                    <td><?= h($product->product_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Price Per Kg/ltr') ?></th>
                                                    <td><?= h($product->price) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Total Stock') ?></th>
                                                    <td><?= h($product->total_stock) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Image') ?></th>
                                                    <td><img title="<?= $product->image ?>" alt="<?= $product->image ?>" class="image_prod" src="<?= BASE_URL.'/img/product/'.$product->image ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Date From') ?></th>
                                                    <td><?= ($product->from_date != null) ? date_format($product->from_date, 'd/m/Y') : "-" ?> </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Date To') ?></th>
                                                    <td><?= ($product->to_date != null) ? date_format($product->to_date, 'd/m/Y') : "-" ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Status') ?></th>
                                                    <td><?= h($product->status) ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       <!--  <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>