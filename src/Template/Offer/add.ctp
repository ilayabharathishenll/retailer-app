<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Offer</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/offer">Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($offer,array('enctype'=>'multipart/form-data', 'class'=>'offer')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="offer_name" placeholder="Offer Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Description<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="name form-control" type="text" name="offer_description" placeholder="Offer Description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Type<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="name form-control" name="offer_type">
                                <option value="">Select Offer Type</option>
                                <option value="New User">New User</option>
                                <option value="Other User">Other User</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Discount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="name form-control" name="discount_type">
                                <option value="">Select Discount</option>
                                <option value="Percentage">Percentage</option>
                                <option value="Flat">Flat</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Discount Amount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="discount_amt" placeholder="Discount Amount" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Minimum Order Amount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="min_order_amt" placeholder="Minimum Order Amount" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">No Of Usage<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="no_of_usage" placeholder="No Of Usage" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Start Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control date_from" type="text" name="start_date" id="date_from" placeholder="Start Date" value="" autocomplete="off"/>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">End Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control date_to" type="text" name="end_date" id="date_to" placeholder="End Date" value="" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="value form-control" type="file" name="image" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Status<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="status" id="optionsRadios25" value="Active" checked> Active
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="status" id="optionsRadios26" value="Inactive"> Inactive
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Save</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/offer"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
