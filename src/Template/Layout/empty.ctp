<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'retail-app';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('../webroot/plugins/font-awesome/css/font-awesome.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/simple-line-icons/simple-line-icons.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/uniform/css/uniform.default.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>
    <?= $this->Html->css('components.min.css') ?>
    <?= $this->Html->css('plugins.min.css') ?>
    <?= $this->Html->css('login.min.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="login">
    <?= $this->fetch('content') ?>
    <footer>
        <?= $this->Html->script('../webroot/plugins/jquery.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap/js/bootstrap.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/js.cookie.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery.blockui.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/uniform/jquery.uniform.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/moment.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/fullcalendar/fullcalendar.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-ui/jquery-ui.min.js') ?>
        <?= $this->Html->script('../webroot/scripts/app.min.js') ?>
        <?= $this->Html->script('../webroot/layouts/layout/scripts/layout.min.js') ?>
        <?= $this->Html->script('../webroot/layouts/layout/scripts/demo.min.js') ?>
        <?= $this->Html->script('../webroot/layouts/global/scripts/quick-sidebar.min.js') ?>
    </footer>
</body>
