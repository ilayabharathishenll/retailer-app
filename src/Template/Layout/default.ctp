<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cat = $url[2];
$action = isset($url[3]) ? $url[3] : '';
$cakeDescription = 'Retailer App';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('../webroot/plugins/font-awesome/css/font-awesome.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/simple-line-icons/simple-line-icons.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap/css/bootstrap.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/select2/css/select2.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/uniform/css/uniform.default.css') ?>
    <?= $this->Html->css('../webroot/plugins/fullcalendar/fullcalendar.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/datatables/datatables.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')?>
    <?= $this->Html->css('components.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>
    <?= $this->Html->css('plugins.min.css') ?>
    <?= $this->Html->css('../webroot/layouts/layout/css/layout.min.css') ?>
    <?= $this->Html->css('../webroot/layouts/layout/css/themes/darkblue.min.css') ?>
    <?= $this->Html->css('../webroot/layouts/layout/css/custom.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') ?>
    <?= $this->Html->css('../webroot/plugins/icheck/skins/all.css') ?>
    <?= $this->Html->css('../webroot/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css') ?>
    <?= $this->Html->css('../css/bootstrap-datepicker.css') ?>
    <?= $this->Html->css('plugins.min.css') ?>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" value="">

    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">

                <a href="<?php echo BASE_URL;?>">
                    <b>LOGO</b></a>
                <div class="menu-toggler sidebar-toggler"> </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile" style="color:#FFF">
                            <?php echo ucfirst($_SESSION["users"]["username"]);?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo BASE_URL;?>/users/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <div class="page-container clearfix">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <?php if ($current_user['user_role'] == 'Super Admin') { ?>
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler"> </div>
                        </li>
                        <li class="nav-item start <?php if($cat=='dashboard') {echo 'active open';} ?>">
                            <a href="<?php echo BASE_URL."/dashboard";?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item <?php if($cat=='retailer') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-user"></i>
                            <span class="title">Retailers</span>
                            <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='retailer' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/retailer" class="nav-link ">
                                    <span class="title">Retailer List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='retailer' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/retailer/add" class="nav-link ">
                                    <span class="title">Add Retailer</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($cat=='products') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-handbag"></i>
                                <span class="title">Product Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='products' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/products" class="nav-link ">
                                    <span class="title">Products</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='products' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/products/add" class="nav-link ">
                                    <span class="title">Add Product</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($cat=='category') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-support"></i>
                                <span class="title">Category</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='category' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/category" class="nav-link ">
                                    <span class="title">Category List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='category' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/category/add" class="nav-link ">
                                    <span class="title">Add Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($cat=='subcategory') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-cubes"></i>
                                <span class="title">Sub Category</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='subcategory' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/subcategory" class="nav-link ">
                                    <span class="title">Sub Category List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='subcategory' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/subcategory/add" class="nav-link ">
                                    <span class="title">Add Sub Category</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!--<li class="nav-item <?php if($cat=='offer') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-gift"></i>
                                <span class="title">Offers</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='offer' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/offer" class="nav-link ">
                                    <span class="title">Offers List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='offer' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/offer/add" class="nav-link ">
                                    <span class="title">Add Offer</span>
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                        <li class="nav-item <?php if($cat=='welcomeoffer') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-gift"></i>
                                <span class="title">Welcome Offers</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='welcomeoffer' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/welcomeoffer" class="nav-link ">
                                    <span class="title">Welcome Offers List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='welcomeoffer' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/welcomeoffer/add" class="nav-link ">
                                    <span class="title">Add Offer</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($cat=='discounts') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-diamond"></i>
                            <span class="title">Promotions/Discounts</span>
                            <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='discounts' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/discounts" class="nav-link ">
                                    <span class="title">Promotion/Discount List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='discounts' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/discounts/add" class="nav-link ">
                                    <span class="title">Add Promotion/Discount</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($cat=='orders') {echo 'active open';} ?>">
                            <a class="nav-link" href="<?php echo BASE_URL;?>/orders">
                                <i class="icon-layers"></i>
                                <span class="title">Orders</span>
                            </a>
                        </li>
                        <li class="nav-item <?php if($cat=='cod') {echo 'active open';} ?>">
                            <a class="nav-link" href="<?php echo BASE_URL;?>/cod">
                                <i class="icon-wallet"></i>
                                <span class="title">COD Orders</span>
                            </a>
                        </li>
                        <li class="nav-item <?php if($cat=='settings') {echo 'active open';} ?>">
                            <a class="nav-link" href="<?php echo BASE_URL;?>/settings">
                                <i class="icon-settings"></i>
                                <span class="title">Settings</span>
                            </a>
                        </li>
                        <li class="nav-item <?php if($cat=='users') {echo 'active open';} ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-user"></i>
                            <span class="title">Users</span>
                            <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($cat=='users' && $action=='') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/users" class="nav-link ">
                                    <span class="title">User List</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($cat=='users' && $action=='add') {echo 'active open';} ?>">
                                    <a href="<?php echo BASE_URL;?>/users/add" class="nav-link ">
                                    <span class="title">Add User</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                <?php } else { ?>
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper hide">
                            <div class="sidebar-toggler"> </div>
                        </li>
                        <li class="nav-item <?php if($cat=='orders') {echo 'active open';} ?>">
                            <a class="nav-link" href="<?php echo BASE_URL;?>/orders">
                                <i class="icon-layers"></i>
                                <span class="title">Orders</span>
                            </a>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="page-content">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="page-footer-inner"> 2018 © Retailer App.</div>
        <div class="scroll-to-top" style="display: block;">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <footer>
        <script type="text/javascript">
            var base_url = <?= json_encode(BASE_URL) ?>;
        </script>
        <?= $this->Html->script('../webroot/plugins/jquery.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap/js/bootstrap.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/js.cookie.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery.blockui.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/uniform/jquery.uniform.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js') ?>
        <?= $this->Html->script('../webroot/plugins/moment.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/fullcalendar/fullcalendar.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-ui/jquery-ui.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-validation/js/jquery.validate.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/jquery-validation/js/additional-methods.min.js') ?>
        <?= $this->Html->script('../webroot/scripts/app.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/datatables/datatables.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') ?>
        <?= $this->Html->script('../webroot/scripts/table-datatables-managed.js') ?>
        <?= $this->Html->script('../webroot/layouts/layout/scripts/layout.min.js') ?>
        <?= $this->Html->script('../webroot/layouts/layout/scripts/demo.min.js') ?>
        <?= $this->Html->script('../webroot/layouts/global/scripts/quick-sidebar.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/moment.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-daterangepicker/daterangepicker.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/morris/morris.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/morris/raphael-min.js') ?>
        <?= $this->Html->script('../webroot/plugins/counterup/jquery.waypoints.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/counterup/jquery.counterup.min.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/amcharts.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/serial.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/pie.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/radar.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/themes/light.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/themes/patterns.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amcharts/themes/chalk.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/ammap/ammap.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/ammap/maps/js/worldLow.js') ?>
        <?= $this->Html->script('../webroot/plugins/amcharts/amstockcharts/amstock.js') ?>
        <?= $this->Html->script('../webroot/plugins/select2/js/select2.full.min.js') ?>
        <?= $this->Html->script('../webroot/scripts/custom.js') ?>
        <?= $this->Html->script('../webroot/scripts/jQuery.print.js') ?>
        <?= $this->Html->script('../webroot/plugins/icheck/icheck.min.js') ?>
    </footer>
</body>
</html>
