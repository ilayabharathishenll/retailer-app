<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Admin $admin
 */
?>
<<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Admin</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="/retail-app/admin"><img src="/public/assets/img/back.png"/></a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($admin) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="name" placeholder="Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Password<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="password form-control" type="text" name="password" placeholder="Password" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Username<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="username form-control" type="text" name="username" placeholder="Username" value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Mobile<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="mobile form-control" type="text" name="mobile" placeholder="Mobile" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">email<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="email form-control" type="text" name="email" placeholder="Email" value=""/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form" type="submit">Add Admin</button>
                            <a class="btn btn-primary cancel-btn" title="Go back to listing page" href="/retail-app/admin">Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
