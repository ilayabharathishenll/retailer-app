<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Category</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Category" href="<?php echo BASE_URL;?>/category/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo !empty($searchParams) ? $searchParams['name'] : '' ?>"></div>
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset"  href="<?php echo BASE_URL;?>/category"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('enable date') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($category as $category): ?>
                <tr>
                    <td><?= $this->Number->format($category->category_id) ?></td>
                    <td><?= h($category->category_name) ?></td>
                    <td><img title="<?= $category->image ?>" alt="<?= $category->image ?>" class="image_prod" src="<?= BASE_URL.'/img/category/'.$category->image ?>">
                    </td>
                    <td><?php echo($category['enable_date'] === 1 ? "ON" : "OFF") ?></td>
                    <td class="actions">
                        <?= $this->Html->link('<i class="fa fa-edit"></i> ' .__('Edit'), ['action' => 'edit', $category->category_id],['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]) ?>
                        <?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $category->category_id], ['escape' => false,'confirm' => __('Are you sure you want to delete {0}?', $category->category_name),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                        <a data-toggle="modal" data-value="view" data-target="#view_detail" title="View" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b><?= ucfirst($category->category_name) ?></b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-horizontal">
                                            <table class=" table vertical-table table-bordered">
                                                <tr>
                                                    <th scope="row"><?= __('Id') ?></th>
                                                    <td><?= $this->Number->format($category->category_id) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Name') ?></th>
                                                    <td><?= h($category->category_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Enable Date') ?></th>
                                                    <td><?php echo($category['enable_date'] === 1 ? "ON" : "OFF") ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Value') ?></th>
                                                    <td><img title="<?= $category->image ?>" alt="<?= $category->image ?>" class="image_prod" src="<?= BASE_URL.'/img/category/'.$category->image ?>"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>