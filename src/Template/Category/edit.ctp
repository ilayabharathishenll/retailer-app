<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Category</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/category">Back</a></div>
    </div>
        <div class="portlet-body editing">
            <div class="form-horizontal">
                <?= $this->Form->create($category,array('enctype'=>'multipart/form-data', 'class'=>'category')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="category_name" placeholder="Name" value="<?php echo $category['category_name']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <img title="<?= $category->image ?>" alt="<?= $category->image ?>" class="image_prod" src="<?= BASE_URL.'/img/category/'.$category->image ?>">
                            <span class="fa fa-times"></span>
                            <input class="hidden img_upload form-control" type="text" name="image" placeholder="Image Url" value="<?php echo $category['image']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Enable Date<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-4">
                            <input type="checkbox" name="enable_date" class="make-switch" data-on-text="ON" data-off-text="OFF" data-on-color="success" data-off-color="danger" <?php echo $category['enable_date'] === 1 ? 'checked' : ''; ?>>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form" type="submit">Update</button>
                            <a class="btn btn-primary cancel-btn" title="Go back to listing page" href="<?php echo BASE_URL ?>/category">Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
