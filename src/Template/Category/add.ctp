<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Category</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/category">Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($category,array('enctype'=>'multipart/form-data', 'class'=>'category')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="category_name" placeholder="Name" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="value form-control" type="file" name="image" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Enable Date<span class="required" aria-required="true"> * </span></label>
                        <div class="col-md-4">
                            <input type="checkbox" name="enable_date" class="make-switch" data-on-text="ON" data-off-text="OFF" data-on-color="success" data-off-color="danger" checked>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Save</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/category"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
