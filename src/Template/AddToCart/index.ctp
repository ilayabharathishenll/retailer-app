<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AddToCart[]|\Cake\Collection\CollectionInterface $addToCart
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Drivers</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" title="Add new Driver" href="/retail-app/addToCart/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($addToCart as $addToCart): ?>
                <tr>
                    <td><?= $this->Number->format($addToCart->id) ?></td>
                    <td><?= h($addToCart->product_id) ?></td>
                    <td><?= h($addToCart->quantity) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $addToCart->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $addToCart->id], ['confirm' => __('Are you sure you want to delete # {0}?', $addToCart->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>
