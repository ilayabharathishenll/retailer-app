<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AddToCart $addToCart
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Product Detail</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="/retail-app/addToCart"><img src="/public/assets/img/back.png"/></a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($addToCart) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Product Id<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="product_id form-control" type="text" name="product_id" placeholder="Product Id" value=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="quantity">Quantity<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="quantity form-control" type="text" name="quantity" placeholder="Quantity" value=""/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form" type="submit">Add To Cart</button>
                            <a class="btn btn-primary cancel-btn" title="Go back to listing page" href="/retail-app/addToCart">Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
