<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold">Edit User</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/users"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($user,['class'=>'users']) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">First Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="first_name" placeholder="First Name" value="<?php echo $user['first_name']?>"/>
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user['user_id']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="last_name">Last Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="last_name" placeholder="Last Name" value="<?php echo $user['last_name']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Username<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="first_name form-control" type="text" name="username" placeholder="Username" value="<?php echo $user['username']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Email<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="email form-control" type="text" name="email_id" placeholder="Email" value="<?php echo $user['email_id']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">Mobile<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="mobile form-control" type="text" name="mobile" placeholder="Mobile" value="<?php echo $user['mobile']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="user_role">
                                <option value="">--- Select Role ---</option>
                                <option value="Maintainer" <?= $user['user_role'] == "Maintainer" ? "selected='selected'" : "" ?>>Maintainer</option>
                                <option value="Super Admin" <?= $user['user_role'] == "Super Admin" ? "selected='selected'" : "" ?>>Super Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update </button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/users"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
</script>