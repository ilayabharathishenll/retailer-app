
<style>
    .login .content {
        background-image: url('<?php echo BASE_URL."/webroot/img/login-background.jpg";?>');
        background-size: cover;
        background-repeat: no-repeat;
    }
    .login .content {
        width: auto;
        margin: 0;
        border: 0;
        height: 100%;
    }
    .login .content .login-form {
        width: 370px;
        margin: 12% auto;
        background-color: #cccc;
        padding: 8px 35px;
    }
    .login .content h3 {
        color: #fff;
        text-transform: uppercase;
    }
    html,
    body {
        height: 100%;
    }
    .login .content .message.error {
        display: none !important;
    }
</style>
<div class="content">
    <?= $this->Flash->render() ?>
	<div class="login-form">
    <?= $this->Form->create('',['class'=>'form-horizontal','role'=>'form']); ?>
        <h3 class="form-title">Login</h3>
        <?php 
        	if($state == "0") {?>
		       <div class="alert alert-danger alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <button class="close" data-close="alert"></button><span> Invalid Mobile number/Password.</span>
		        </div>
	    <?php } ?>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Mobile number</label>
            <input class="form-control form-control-solid placeholder-no-fix" required="" autocomplete="off" placeholder="Mobile number" name="mobile">
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" required="" autocomplete="off" placeholder="Password" name="password">
        </div>
        <div class="form-actions">
            <button class="btn green uppercase login_btn" type="submit">Login</button>
        </div>
    <?= $this->Form->end(); ?>
</div>
