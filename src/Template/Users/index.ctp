<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Users</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Retailers" href="<?php echo BASE_URL;?>/users/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Name" name="name" value="<?php echo !empty($searchParams) ? $searchParams['name'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Email" name="email" value="<?php echo !empty($searchParams) ? $searchParams['email'] : '' ?>"></div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Mobile" name="mobile" value="<?php echo !empty($searchParams) ? $searchParams['mobile'] : '' ?>"></div>
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset" href="<?php echo BASE_URL;?>/users"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('role') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= $this->Number->format($user->user_id) ?></td>
                    <td><?= h($user->first_name) ?></td>
                    <td><?= h($user->email_id) ?></td>
                    <td><?= h($user->mobile) ?></td>
                    <td><?= h($user->user_role) ?></td>
                    <td class="actions">
                    <?= 
                        $this->Html->link(
                        '<i class="fa fa-edit"></i> ' .__('Edit'),
                        ['action' => 'edit', $user->user_id],
                        ['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]
                    ) ?>
                    
                    <?= $this->Form->postLink('<i class="fa fa-trash"></i> ' .__('Delete'), ['action' => 'delete', $user->user_id],['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->user_id),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                    
                    <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                    <div class="hidden row_detail portlet box green">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"><b><?= ucfirst($user->first_name.' '.$user->last_name) ?></b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <table class=" table vertical-table table-bordered">
                                            <tr>
                                                <th scope="row"><?= __('Id') ?></th>
                                                <td><?= $this->Number->format($user->user_id) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Name') ?></th>
                                                <td><?= h($user->first_name.' '.$user->last_name) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Email') ?></th>
                                                <td><?= h($user->email_id) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Mobile') ?></th>
                                                <td><?= h($user->mobile) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Role') ?></th>
                                                <td><?= h($user->user_role) ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>