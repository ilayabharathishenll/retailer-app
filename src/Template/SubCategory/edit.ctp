<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Sub Category</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/subcategory">Back</a></div>
    </div>
        <div class="portlet-body editing">
            <div class="form-horizontal">
                <?= $this->Form->create($subcategory,array('enctype'=>'multipart/form-data', 'class'=>'subcategory')) ?>
                   <div class="form-group">
                        <label class="control-label col-md-3" for="type">Category Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <?php if(!empty($category)) { ?>
                                <select name="category_id" class="form-control">
                                        <option value="">-- Select --</option>
                                        <?php foreach ($category as $categorys) { 
                                            $select=($categorys['category_id']==$subcategory->category_id)?"Selected":"";
                                            ?>
                                            
                                            <option value="<?php echo $categorys['category_id']?>" <?php echo $select?>><?php echo $categorys['category_id'].' - '.$categorys['category_name']; ?></option>
                                        <?php } ?>
                                </select>
                            <?php } else { ?>
                                <select name="" class="form-control">
                                    <option value="">Please create category</option>
                                </select>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Sub Category Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="sub_category_name" placeholder="Sub Category Name" value="<?= $subcategory->sub_category_name ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <img title="<?= $subcategory->image ?>" alt="<?= $subcategory->image ?>" class="image_prod" src="<?= BASE_URL.'/img/subcategory/'.$subcategory->image ?>">
                            <span class="fa fa-times"></span>
                            <input class="hidden img_upload form-control" type="text" name="image" placeholder="Image Url" value="<?php echo $subcategory['image']?>"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL ?>/subcategory"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
