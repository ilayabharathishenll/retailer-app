<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold">Welcome Offers</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Welcome Offer" href="<?php echo BASE_URL;?>/welcomeoffer/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Offers Name" name="offer_name" value="<?php echo !empty($searchParams) ? $searchParams['offer_name'] : '' ?>"></div>
                <div class="col-md-3">
                    <select class="form-control" name="discount_type">
                        <option value="">Discount Type</option>
                        <option value="Percentage" <?php echo !empty($searchParams) && $searchParams['discount_type'] == 'Percentage' ? 'selected = "selected"' : '' ?>>Percentage</option>
                        <!-- <option value="Flat" <?php echo !empty($searchParams) && $searchParams['discount_type'] == 'Flat' ? 'selected = "selected"' : '' ?>>Flat</option> -->
                    </select>
                </div>
               <!--  <div class="col-md-3"><input class="from form-control" type="text" placeholder="Start Date" name="start_date" data-date-format="dd/mm/yyyy" readonly="" value="<?php echo !empty($searchParams) ? $searchParams['start_date'] : '' ?>"></div>
                <div class="col-md-3"><input class="to form-control" type="text" placeholder="End Date" name="end_date" data-date-format="dd/mm/yyyy" readonly="" value="<?php echo !empty($searchParams) ? $searchParams['end_date'] : '' ?>"></div> -->
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset"  href="<?php echo BASE_URL;?>/welcomeoffer"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Offers Name') ?></th>
                   <!--  <th scope="col"><?= $this->Paginator->sort('Start Date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('End Date') ?></th> -->
                    <th scope="col"><?= $this->Paginator->sort('Discount Type') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Discount') ?></th>
                     <th scope="col"><?= $this->Paginator->sort('No of Days') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=1;
                foreach ($offer as $offers): 
                ?>
                <tr>
                    <td><?= $this->Number->format($i++) ?></td>
                    <td><?= h($offers->offer_name) ?></td>
                   <!--  <td><?= h($offers->start_date) ?></td>
                    <td><?= h($offers->end_date) ?></td> -->
                    <td><?= h($offers->discount_type) ?></td>
                    <td><?= h($offers->discount_amt) ?></td>
                    <td><?= h($offers->no_of_days) ?></td>
                    <td><img title="<?= $offers->image ?>" alt="<?= $offers->image ?>" class="image_prod" src="<?= BASE_URL.'/img/offers/'.$offers->image ?>">
                    </td>
                    <td><span class="label label-sm label-<?php echo ($offers->status=="Active")?"success":"danger"; ?> "><?= h($offers->status) ?></span></td>
                    <td class="actions">
                        <?= $this->Html->link('<i class="fa fa-edit"></i> ' .__('Edit'), ['action' => 'edit', $offers->welcome_id],['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]) ?>
                        <?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $offers->welcome_id], ['escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $offers->welcome_id),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                        <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b><?= ucfirst($offers->offer_name) ?></b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-horizontal">
                                            <table class=" table vertical-table table-bordered">
                                                <tr>
                                                    <th scope="row"><?= __('Offer Description') ?></th>
                                                    <td><?= h($offers->offer_description) ?></td>
                                                </tr>
                                               <!--  <tr>
                                                    <th scope="row"><?= __('Offer Type') ?></th>
                                                    <td><?= h($offers->offer_type) ?></td>
                                                </tr> -->
                                                <tr>
                                                    <th scope="row"><?= __('Discount Type') ?></th>
                                                    <td><?= h($offers->discount_type) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Discount Amount') ?></th>
                                                    <td><?= h($offers->discount_amt) ?></td>
                                                </tr>
                                                <!-- <tr>
                                                    <th scope="row"><?= __('Start Date') ?></th>
                                                    <td><?= h($offers->start_date) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('End Date') ?></th>
                                                    <td><?= h($offers->end_date) ?></td>
                                                </tr> -->
                                                <tr>
                                                    <th scope="row"><?= __('No Of Days') ?></th>
                                                    <td><?= h($offers->no_of_days) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Image') ?></th>
                                                    <td><img title="<?= $offers->image ?>" alt="<?= $offers->image ?>" class="image_prod" src="<?= BASE_URL.'/img/offers/'.$offers->image ?>"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       <!--  <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>