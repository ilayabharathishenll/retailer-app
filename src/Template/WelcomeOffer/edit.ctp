<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Welcome Offer</span></div>
        <div class="actions"><a class="btn btn-default btn-sm" href="<?php echo BASE_URL ?>/welcomeoffer">Back</a></div>
    </div>
        <div class="portlet-body editing">
            <div class="form-horizontal">
                <?= $this->Form->create($offer,array('enctype'=>'multipart/form-data', 'class'=>'welcomeoffer')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="offer_name" placeholder="Offer Name" value="<?php echo $offer->offer_name?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Description</label>
                        <div class="col-md-5">
                            <textarea class="name form-control" type="text" name="offer_description" placeholder="Offer Description"><?php echo $offer->offer_description?></textarea>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3" for="type">Offer Type<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="name form-control" name="offer_type">
                                <option value="">Select Offer Type</option>
                                <option value="New User" <?php echo ($offer->offer_type=="New User")?"selected":"";?>>New User</option>
                                <option value="Other User" <?php echo ($offer->offer_type=="Other User")?"selected":"";?>>Other User</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Discount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="name form-control" name="discount_type">
                                <option value="">Select Discount</option>
                                <option value="Percentage" <?php echo ($offer->discount_type=="Percentage")?"selected":"";?>>Percentage</option>
                               <!--  <option value="Flat" <?php echo ($offer->discount_type=="Flat")?"selected":"";?>>Flat</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Discount Amount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="discount_amt" placeholder="Discount Amount" value="<?php echo $offer->discount_amt?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Minimum Order Amount<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="min_order_amt" placeholder="Minimum Order Amount" value="<?php echo $offer->min_order_amt?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">No Of Days<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control" type="text" name="no_of_days" placeholder="No Of Days" value="<?php echo $offer->no_of_days?>"/>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3" for="type">Start Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control date_from" type="text" name="start_date" id="date_from" placeholder="Start Date" value="<?php echo date("Y-m-d H:i",strtotime($offer->start_date))?>" autocomplete="off"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">End Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="name form-control date_to" type="text" name="end_date" id="date_to" placeholder="End Date" value="<?php echo date("Y-m-d H:i",strtotime($offer->end_date))?>" autocomplete="off" />
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <img title="<?= $offer->image ?>" alt="<?= $offer->image ?>" class="image_prod" src="<?= BASE_URL.'/img/offers/'.$offer->image ?>">
                            <span class="fa fa-times"></span>
                            <input class="hidden img_upload form-control" type="text" name="image" placeholder="Image Url" value="<?php echo $offer->image?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="type">Status<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="status" id="optionsRadios25" value="Active" <?php echo ($offer->status=="Active")?"checked":"";?> checked> Active
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="status" id="optionsRadios26" value="Inactive" <?php echo ($offer->status=="Inactive")?"checked":"";?>> Inactive
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/welcomeoffer"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
