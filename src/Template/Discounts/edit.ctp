<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Promotion $promotion
 */
$start_date = date_format($discount['start_date'],"Y-m-d H:i");
$end_date = date_format($discount['end_date'],"Y-m-d H:i");
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Edit Promotion</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/discounts"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body editing">
            <div class="form-horizontal">
                <?= $this->Form->create($discount, array('enctype'=>'multipart/form-data', 'class'=>'promotion')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="category_id">Product<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <?php if(!empty($products)) { ?>
                                <select name="product_id" class="form-control search_opt">
                                    <option value="">-- Select --</option>
                                    <?php foreach ($products as $product) { ?>
                                        <option value="<?php echo $product['product_id']?>" <?php if($discount['product_id'] == $product['product_id']){echo 'selected';}?>><?php echo $product['product_id'].' - '.$product['product_name']; ?></option>
                                    <?php } ?>
                                </select>
                            <?php } else { ?>
                                <select name="" class="form-control">
                                    <option value="">Please create products</option>
                                </select>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="discount_type">Discount Type<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="discount_type form-control" type="text" name="discount_type">
                                <option value="">Select</option>
                                <option <?php echo !empty($discount) && $discount['discount_type'] == 'Promotion' ? 'selected = "selected"' : '' ?> value="Promotion">Promotion</option>
                                <option <?php echo !empty($discount) && $discount['discount_type'] == 'Discount' ? 'selected = "selected"' : '' ?> value="Discount">Discount</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="promotion_name">Promotion Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="promotion_name" placeholder="Name" value="<?php echo $discount['promotion_name']?>">
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="promotion_desc">Promotion Description<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="form-control" type="text" name="promotion_desc" placeholder="Description" value=""><?php echo $discount['promotion_desc']?></textarea>
                        </div>
                    </div>
                    <div class="form-group promotion hidden">
                        <label class="control-label col-md-3" for="promotion_price">Promotion Price<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="promotion_price" placeholder="Price" value="<?php echo $discount['promotion_price']?>">
                        </div>
                    </div>
                    <div class="form-group discount">
                        <label class="control-label col-md-3" for="discount_percentage">Discount Percentage<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="discount_percentage" placeholder="Percentage" value="<?php echo $discount['discount_percentage']?>">
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="promotion_desc">Total Stock<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="total_stock" placeholder="Total Stock" value="<?php echo $discount['total_stock']?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="weight">Weight<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="weight" placeholder="weight" value="<?php echo $discount['weight']?>">
                        </div>
                    </div>
                    <div class="form-group discount">
                        <label class="control-label col-md-3" for="aggregate">Aggregate<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="aggregate">
                                <option value="">---Select Agregate---</option>
                                <option value=">" <?= ($discount['aggregate'] == ">") ? "selected='selected'" : "" ?>>Greater than</option>
                                <option value="<" <?= ($discount['aggregate'] == "<") ? "selected='selected'" : "" ?>>Less than</option>
                                <option value="==" <?= ($discount['aggregate'] == "==") ? "selected='selected'" : "" ?>>Equal To</option>
                                <option value=">=" <?= ($discount['aggregate'] == ">=") ? "selected='selected'" : "" ?>>Greater than or equal To</option>
                                <option value="<=" <?= ($discount['aggregate'] == "<=") ? "selected='selected'" : "" ?>>Less than or equal To</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="image">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <img  title="<?= $discount['image'] ?>" alt="<?= $discount['image'] ?>" class="image_prod" src="<?= BASE_URL.'/img/promotion/'.$discount->image ?>">
                            <span class="fa fa-times"></span>
                            <input class="hidden img_upload form-control" type="text" name="image" placeholder="Image Url" value="<?php echo $discount['image']?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">From Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control date_from" readonly data-validation="required" type="text" value="<?php echo $start_date?>" name="start_date" id="date_from" placeholder="Date From">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">To Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="date_to form-control" readonly type="text" name="end_date" placeholder="Date To" value="<?php echo $end_date?>" id="date_to" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Update</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/discounts"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
