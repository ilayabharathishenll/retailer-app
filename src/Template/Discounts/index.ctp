<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Promotion[]|\Cake\Collection\CollectionInterface $promotion
 */
?>
<div class="portlet box green driver-list">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Promotion/Discount</span></div>
        <div class="actions"><a class="btn green btn-sm customaddbtn" title="Add Promotion/Discount" href="<?php echo BASE_URL;?>/discounts/add">Add <i class="fa fa-plus"></i></a></div>
    </div>
    <div class="portlet-body listing">
        <div class="row">
            <?= $this->Form->create("",['type'=>'post']) ?>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Product Name" name="product_name" value="<?php echo !empty($searchParams) ? $searchParams['product_name'] : '' ?>"></div>
                <div class="col-md-3">
                    <select class="form-control" name="discount_type">
                        <option value="">Promotion Type</option>
                        <option value="Promotion" <?php echo !empty($searchParams) && $searchParams['discount_type'] == 'Promotion' ? 'selected = "selected"' : '' ?>>Promotion</option>
                        <option value="Discount" <?php echo !empty($searchParams) && $searchParams['discount_type'] == 'Discount' ? 'selected = "selected"' : '' ?>>Discount</option>
                    </select>
                </div>
                <div class="col-md-3"><input class="form-control" type="text" placeholder="Weight" name="weight" value="<?php echo !empty($searchParams) ? $searchParams['weight'] : '' ?>"></div>
                <div class="col-md-3"><input class="from form-control" type="text" placeholder="From Date" name="start_date" data-date-format="dd/mm/yyyy" readonly="" value="<?php echo !empty($searchParams) ? $searchParams['start_date'] : '' ?>"></div>
                <div class="col-md-3"><input class="to form-control" type="text" placeholder="To Date" name="end_date" data-date-format="dd/mm/yyyy" readonly="" value="<?php echo !empty($searchParams) ? $searchParams['end_date'] : '' ?>"></div>
                <div class="col-md-3"><button class="btn btn-success customsearchbtn" type="submit" title="Search"><i class="fa fa-search"></i> Search</button><a class="btn btn-success customresetbtn" title="Reset" href="<?php echo BASE_URL;?>/discounts"><i class="fa fa-undo"></i> Reset</a></div>
            <?= $this->Form->end() ?>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="driver">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('discount_type') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('stock') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($discount as $discount): ?>
                <tr>
                    <td><?= $this->Number->format($discount->discount_id) ?></td>
                    <td><?= h($discount->tbl_product->product_name) ?></td>
                    <td><?= h($discount->discount_type) ?></td>
                    <td><?= h($discount->weight) ?></td>
                    <td><?= h($discount->total_stock) ?></td>
                    <td><?= date_format($discount->start_date, 'd/m/Y h:i A') ?></td>
                    <td><?= date_format($discount->end_date, 'd/m/Y h:i A') ?></td>
                    <td class="actions">
                        <?= $this->Html->link('<i class="fa fa-edit"></i> ' .__('Edit'), ['action' => 'edit', $discount->discount_id],['escape' => false, 'type'=>'button','class'=>'btn grey-cascade btn-xs customeditbtn','title' => __('Edit')]) ?>
                        <?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $discount->discount_id], ['escape' => false,'confirm' => __('Are you sure you want to delete # {0}?', $discount->discount_id),'class'=>'btn grey-cascade btn-xs customdeletebtn','title' => __('Delete')]) ?>
                        <a data-toggle="modal" data-value="view" title="View" data-target="#view_detail" type="button" class="btn grey-cascade view_detail btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a>
                        <div class="hidden row_detail portlet box green">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"><b>Promotion/Discount</b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <table class=" table vertical-table table-bordered">
                                            <tr>
                                                <th scope="row"><?= __('Discount Id') ?></th>
                                                <td><?= $this->Number->format($discount->discount_id) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Product Name') ?></th>
                                                <td><?= h($discount->tbl_product->product_name) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Promotion Type') ?></th>
                                                <td><?= h($discount->discount_type) ?></td>
                                            </tr>
                                            <?php if($discount->discount_type == 'Discount') { ?>
                                                <tr>
                                                    <th scope="row"><?= __('Discount Percentage') ?></th>
                                                    <td><?= h($discount->discount_percentage) ?></td>
                                                </tr>
                                            <?php } ?>
                                            <?php if($discount->discount_type == 'Promotion') { ?>
                                                <tr>
                                                    <th scope="row"><?= __('Promotion Name') ?></th>
                                                    <td><?= h($discount->promotion_name) ?></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><?= __('Promotion Description') ?></th>
                                                    <td><?= h($discount->promotion_desc) ?></td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <th scope="row"><?= __('Weight') ?></th>
                                                <td><?= h($discount->weight) ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Date From') ?></th>
                                                <td><?= date_format($discount->start_date, 'd/m/Y h:i A') ?></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><?= __('Date To') ?></th>
                                                <td><?= date_format($discount->end_date, 'd/m/Y h:i A') ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div> -->
    </div>
</div>
<div class="modal fade in" id="view_detail" tabindex="-1" role="basic" aria-hidden="true">
</div>