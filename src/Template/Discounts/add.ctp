<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Promotion $promotion
 */
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption icon-sprite"><i class="icon-drivers"></i><span class="caption-subject bold"> Add Promotion</span></div>
        <div class="actions"><a class="btn red btn-sm custombackbtn" href="<?php echo BASE_URL;?>/discounts"><i class="fa fa-angle-left"></i> Back</a></div>
    </div>
        <div class="portlet-body">
            <div class="form-horizontal">
                <?= $this->Form->create($discount, array('enctype'=>'multipart/form-data', 'class'=>'promotion')) ?>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="category_id">Product<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <?php if(!empty($products)) { ?>
                                <select name="product_id" class="form-control">
                                        <option value="">-- Select --</option>
                                        <?php foreach ($products as $product) { ?>
                                            <option value="<?php echo $product['product_id']?>"><?php echo $product['product_id'].' - '.$product['product_name']; ?></option>
                                        <?php } ?>
                                </select>
                            <?php } else { ?>
                                <select name="" class="form-control">
                                    <option value="">Please create products</option>
                                </select>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="discount_type">Discount Type<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="discount_type form-control" type="text" name="discount_type">
                                <option value="" selected>Select</option>
                                <option value="Promotion">Promotion</option>
                                <option value="Discount">Discount</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group promotion hidden">
                        <label class="control-label col-md-3" for="promotion_name">Promotion Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="promotion_name" placeholder="Name" value="">
                        </div>
                    </div>
                    <div class="form-group promotion hidden">
                        <label class="control-label col-md-3" for="promotion_desc">Promotion Description<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <textarea class="form-control" type="text" name="promotion_desc" placeholder="Description" value=""></textarea>
                        </div>
                    </div>
                    <div class="form-group discount hidden">
                        <label class="control-label col-md-3" for="discount_percentage">Discount Percentage<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="discount_percentage" placeholder="Percentage" value="">
                        </div>
                    </div>
                    <div class="form-group promotion hidden">
                        <label class="control-label col-md-3" for="promotion_price">Promotion Price<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="promotion_price" placeholder="Price" value="">
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="promotion_desc">Total Stock<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="total_stock" placeholder="Total Stock" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="weight">Weight<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="text" name="weight" placeholder="weight" value="">
                        </div>
                    </div>
                    <div class="form-group discount hidden">
                        <label class="control-label col-md-3" for="aggregate">Aggregate<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <select class="form-control" name="aggregate">
                                <option value="">---Select Agregate---</option>
                                <option value=">">Greater than</option>
                                <option value="<">Less than</option>
                                <option value="==">Equal To</option>
                                <option value=">=">Greater than or equal To</option>
                                <option value="<=">Less than or equal To</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group promotion">
                        <label class="control-label col-md-3" for="image">Image<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control" type="file" name="image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">From Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="form-control date_from" data-validation="required" type="text" value="" name="start_date" id="date_from" placeholder="Date From">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="first_name">To Date<span class="required"> *</span></label>
                        <div class="col-md-5">
                            <input class="date_to form-control" type="text" name="end_date" placeholder="Date To" value="" id="date_to" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-primary submit-form customsavebtn" type="submit"><i class="fa fa-save"></i> Save</button>
                            <a class="btn btn-primary cancel-btn custombackbtn" title="Go back to listing page" href="<?php echo BASE_URL;?>/discounts"><i class="fa fa-angle-left"></i> Cancel</a>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
