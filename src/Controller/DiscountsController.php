<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Promotion Controller
 *
 * @property \App\Model\Table\PromotionTable $Promotion
 *
 * @method \App\Model\Entity\Promotion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DiscountsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Products');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['product_name'])) {
               $conditions['TblProducts.product_name like'] = trim("%" . $searchParams['product_name'] . "%", " ");
            }

            if(!empty($searchParams['discount_type'])) {
                $conditions['discount_type'] = trim($searchParams['discount_type'], " ");
            }

            if(!empty($searchParams['weight'])) {
                $conditions['weight'] = trim($searchParams['weight'], " ");
            }

            if(!empty($searchParams['start_date'])) {
                // echo $searchParams['from_date'];            
                if(is_numeric(strpos($searchParams['start_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$searchParams['start_date']);
                    $fromdate = $year."/".$month."/".$day;
                }
                $this->paginate = [
                    'conditions' => ['start_date >= ' => $fromdate]
                ];
                $conditions['start_date >= '] = $fromdate;
            }
            if(!empty($searchParams['end_date'])) {
                if(is_numeric(strpos($searchParams['end_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$searchParams['end_date']);
                    $toDate = $year."/".$month."/".$day;
                }
                $conditions['end_date <= '] = $toDate;
            }

        }

        $discount = $this->Discounts->find('all', ['contain' => 'TblProducts'])->where($conditions)->toArray();
        $this->set(compact('discount', 'searchParams'));
    }

    /**
     * View method
     *
     * @param string|null $id Promotion id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $products = $this->Products->getProducts();
        $discount = $this->Discounts->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                $data["start_date"] = date("Y-m-d h:i", strtotime($data["start_date"]));
                $data["end_date"] = date("Y-m-d h:i", strtotime($data["end_date"]));
                if(!empty($data['image']['name'])) {
                    $file = $data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/promotion/';
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }

            $discounts = $this->Discounts->find()->select()->where(['discount_type'=>'Discount', 'product_id'=>$data['product_id']])->toArray();
            if (!empty($discounts)) {
                $this->Flash->error(__("Discount is already there for that product."));
            } else {
                $discount = $this->Discounts->patchEntity($discount, $data);
                if ($this->Discounts->save($discount)) {
                    $this->Flash->success(__('Promotion/discount has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Unable to save promotion/discount. Please, try again.'));
            }
            
        }
        $this->set(compact('discount', 'products'));
    }
    /**
     * Edit method
     *
     * @param string|null $id Promotion id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $products = $this->Products->getProducts();
        $discount = $this->Discounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                $data["start_date"] = date("Y-m-d h:i", strtotime($data["start_date"]));
                $data["end_date"] = date("Y-m-d h:i", strtotime($data["end_date"]));
                if(!empty($data['image']['name'])) {
                    $file = $data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/promotion/';
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }
            //print_r($data);exit;
            $discount = $this->Discounts->patchEntity($discount, $data);
            if ($this->Discounts->save($discount)) {
                $this->Flash->success(__('Promotion/discount has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save promotion/discount. Please, try again.'));
        }
        $this->set(compact('discount','products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Promotion id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $discount = $this->Discounts->get($id);
        if ($this->Discounts->delete($discount)) {
            $this->Flash->success(__('The discount has been deleted.'));
        } else {
            $this->Flash->error(__('The discount could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function view($id = null)
    {
        $discount = $this->Discounts->get($id, [
            'contain' => []
        ]);

        $this->set('discount', $discount);
    }
}
