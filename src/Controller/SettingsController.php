<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Setting Controller
 *
 * @property \App\Model\Table\SettingTable $Setting
 *
 * @method \App\Model\Entity\Setting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();
        // print_r($this->request->data());exit;
        if(!empty($searchParams)) {
            if(!empty($searchParams['name'])) {
                $conditions['name like'] = trim("%".$searchParams['name']."%", " ");
            }

            $this->paginate = ['conditions' => $conditions];
        }

        $settings = $this->paginate($this->Settings);

        $this->set(compact('settings', 'searchParams'));
    }

    /**
     * View method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $settings = $this->Settings->get($id, [
            'contain' => []
        ]);

        $this->set('settings', $settings);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $settings = $this->Settings->newEntity();
        if ($this->request->is('post')) {
            $settings = $this->Settings->patchEntity($settings, $this->request->getData());
            if ($this->Settings->save($settings)) {
                $this->Flash->success(__('The settings has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save settings. Please, try again.'));
        }
        $this->set(compact('settings'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $settings = $this->Settings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $settings = $this->Settings->patchEntity($settings, $this->request->getData());
            if ($this->Settings->save($settings)) {
                $this->Flash->success(__('The settings has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save settings. Please, try again.'));
        }
        $this->set(compact('settings'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $settings = $this->Settings->get($id);
        if ($this->Settings->delete($settings)) {
            $this->Flash->success(__('The settings has been deleted.'));
        } else {
            $this->Flash->error(__('The settings could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
