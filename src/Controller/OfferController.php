<?php
namespace App\Controller;

Use App\Controller\AppController;

class OfferController extends AppController {
     public function initialize()
    {
        parent::initialize();
        $this->loadModel('Offer');
    }
	public function index() {
        $searchParams = $this->request->data();
        $conditions = array();
        if(!empty($searchParams)) {
            if(!empty($searchParams['offer_name'])) {
                $conditions['offer_name like'] = trim("%".$searchParams['offer_name']."%", " ");
            }
            if(!empty($searchParams['discount_type'])) {
                $conditions['discount_type'] = trim($searchParams['discount_type'], " ");
            }
            if(!empty($searchParams['start_date'])) {
                // echo $searchParams['from_date'];            
                if(is_numeric(strpos($searchParams['start_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$searchParams['start_date']);
                    $fromdate = $year."/".$month."/".$day;
                }
                // $this->paginate = [
                //     'conditions' => ['start_date >= ' => $fromdate]
                // ];
                $conditions['start_date >= '] = $fromdate;
            }
            if(!empty($searchParams['end_date'])) {
                if(is_numeric(strpos($searchParams['end_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$searchParams['end_date']);
                    $toDate = $year."/".$month."/".$day;
                }
                $conditions['end_date <= '] = $toDate;
            }
            $this->paginate = ['conditions' => $conditions];
        }
        $offer = $this->paginate($this->Offer);
        $this->set(compact('offer', 'searchParams'));
	}
    public function add() {
        $offer = $this->Offer->newEntity();
        if($this->request->is('post')) {
            $data = $this->request->data();
            if(!empty($data["image"]["name"])) {
                $file = $data["image"];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $extArr = array('jpg', 'jpeg', 'gif', 'png');
                $name = time().'-'.$file['name'];
                $upload_path = WWW_ROOT.'img/offers/';
                if(in_array($ext, $extArr)) {
                    move_uploaded_file($file['tmp_name'], $upload_path.$name);
                    $data['image'] = $name;
                }
            }
            $offers = $this->Offer->find()->select()->where(['offer_name'=>$data['offer_name']])->toArray();
            if (!empty($offers)) {
                $this->Flash->error(__("Offer name already used."));
            } else {
                $offer = $this->Offer->patchEntity($offer, $data);
                //print_r($offer); exit;
                if($this->Offer->save($offer)) {
                    $this->Flash->success(__("Offer has been saved"));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Unable to save offer. Please, try again."));
            }
        }
        $this->set(compact('offer'));
    }
    public function edit($id = null) {
        $offer = $this->Offer->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                if(!empty($data["image"]["name"])) {
                    $file = $data["image"];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $extArr = array('jpg', 'jpeg', 'gif', 'png');
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/offers/';
                    if(in_array($ext, $extArr)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }
            $offer = $this->Offer->patchEntity($offer, $data);
            if ($this->Offer->save($offer)) {
                $this->Flash->success(__('The offer has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save offer. Please, try again.'));
        }
        $this->set(compact('offer'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $offer = $this->Offer->get($id);
        if ($this->Offer->delete($offer)) {
            $this->Flash->success(__('The offer has been deleted.'));
        } else {
            $this->Flash->error(__('The offer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
?>