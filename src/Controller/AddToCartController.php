<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddToCartController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $addToCart = $this->paginate($this->AddToCart);

        $this->set(compact('addToCart'));
    }

    /**
     * View method
     *
     * @param string|null $id Add To Cart id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    /*public function add()
    {
        $addToCart = $this->AddToCart->newEntity();
        if ($this->request->is('post')) {
            $addToCart = $this->AddToCart->patchEntity($addToCart, $this->request->getData());
            if ($this->AddToCart->save($addToCart)) {
                $this->Flash->success(__('The add to cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The add to cart could not be saved. Please, try again.'));
        }
        $this->set(compact('addToCart', 'products'));
    }*/

    /**
     * Edit method
     *
     * @param string|null $id Add To Cart id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
   /* public function edit($id = null)
    {
        $addToCart = $this->AddToCart->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $addToCart = $this->AddToCart->patchEntity($addToCart, $this->request->getData());
            if ($this->AddToCart->save($addToCart)) {
                $this->Flash->success(__('The add to cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The add to cart could not be saved. Please, try again.'));
        }
        $this->set(compact('addToCart', 'products'));
    }*/

    /**
     * Delete method
     *
     * @param string|null $id Add To Cart id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $addToCart = $this->AddToCart->get($id);
        if ($this->AddToCart->delete($addToCart)) {
            $this->Flash->success(__('The add to cart has been deleted.'));
        } else {
            $this->Flash->error(__('The add to cart could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }*/
}
