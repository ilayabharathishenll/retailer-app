<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;

class ApiCategorysController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Users');
        $this->loadModel('Products');
        $this->loadModel('Categories');
        $this->loadModel('SubCategory');
        // $this->Auth->config('loginAction');
        // $this->redirect($this->Auth->config('loginAction'));
    }

    public function beforeFilter(Event $event) {
       $this->Auth->allow(array('index','categoryView','CategoryAdd','CategoryEdit','CategoryDelete'));
    }
   
    public function index(){
        //$this->Users->check_cart();
        $Categories = $this->paginate($this->Categories);
        if($Categories){
            $returnCategory = array();
            foreach($Categories as $category){
                $subcategories = $this->SubCategory->find()->select()->where(['category_id'=>$category['category_id']])->toArray();
                $category['image']=BASE_URL.'/img/category/'.$category['image'];
                if (count($subcategories)>0) {
                   $category['subCategoryAvailable']=1;
                } else {
                    $category['subCategoryAvailable']=0;
                }
                $category['image']=BASE_URL.'/img/category/'.$category['image'];
                $returnCategory[]=$category;
            }

            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'message'=>'Category Found',
            'result' => $returnCategory,
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Category Not Found',
                'result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }
    public function categoryView($id)
    {
        $view = $this->Categories->get($id);
        if($view){
            $output = array(
            'status'=> 'success',
            'errorCode' =>1,
            'message'=>'Category Found',
            'result' => $view,
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Category Not Found',
                'result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function categoryAdd()
    {
        $product = $this->Products->newEntity($this->request->getData());
        if ($this->Products->save($product)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'message'=>'Category Added Successfully',
            'result' => new \stdClass(),
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Category Not Added Successfully',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function categoryEdit($id)
    {
        $Categories = $this->Categories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $Categoriess = $this->Categories->patchEntity($Categories, $this->request->getData());
            if ($this->Categories->save($Categoriess)) {
                $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'Category Updated Successfully',
                'result' => new \stdClass(),
                 );
            }
            else{
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>0,
                    'message'=>'Category not Updated Successfully',
                    'result' => new \stdClass(),
                );
            }
        }
        else {
           if($Categories){
            //unset($user['password']);
                $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'result' => $Categories,
                 );
            }
            else{
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>0,
                    'message'=>'Category Not Found',
                    'result' => new \stdClass(),
                );
            }
        }      
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function categoryDelete($id)
    {
        $product_delete = $this->Products->get($id);
        $message = 'Deleted';
        if (!$this->Products->delete($product_delete)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'message'=>'success',
            'result' => new \stdClass(),
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }
}