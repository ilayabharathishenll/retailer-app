<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Category');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();
        if(!empty($searchParams)) {
            if(!empty($searchParams['name'])) {
                $conditions['product_name like'] = trim("%".$searchParams['name']."%", " ");
            }
            if($searchParams['price'] !== "") {
                $conditions['price like'] = trim($searchParams['price'], " ");
            }
            if ($searchParams['stock'] !== "") {
                $conditions['total_stock'] = trim($searchParams['stock'], " ");
            }

            $this->paginate = ['conditions' => $conditions];
        }

        $products = $this->Products->find('all')->where($conditions)->order("product_id desc");
        $this->set(compact('products', 'searchParams'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Detail id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $categories = $this->Category->getCat();
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                if(is_numeric(strpos($data['from_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$data['from_date']);
                    $data['from_date'] = $year."/".$month."/".$day;
                }
                if(is_numeric(strpos($data['to_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$data['to_date']);
                    $data['to_date'] = $year."/".$month."/".$day;
                }

                if(!empty($data['image']['name'])) {
                    $file = $data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/product/';
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }
            $available_unit=NULL;
            if(strtolower($data['unit'])!="count"){
                $available =$data['available_unit'];
                $available_unit=json_encode($available);
            }
             $data['available_option']=$available_unit;
            unset($data["available_unit"]);
            unset($data["hndsubcateid"]);
            //print_r($data); exit;
            $product = $this->Products->patchEntity($product, $data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Unable to save product. Please, try again.'));
            }
        }
        $this->set(compact('product','categories'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Detail id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $categories = $this->Category->getCat();
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                if(is_numeric(strpos($data['from_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$data['from_date']);
                    $data['from_date'] = $year."/".$month."/".$day;
                }
                if(is_numeric(strpos($data['to_date'], "/"))) {
                    list($day,$month,$year) = explode("/",$data['to_date']);
                    $data['to_date'] = $year."/".$month."/".$day;
                }

                if(!empty($data['image']['name'])) {
                    $file = $data['image']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/product/';
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }
            
            if(strtolower($data['unit'])!="count"){
                $available =$data['available_unit'];
                $available_unit=json_encode($available);
            }
            if(strtolower($data['unit'])=="count"){
                $available_unit="";
            }
            $data['available_option']=$available_unit;
            unset($data["available_unit"]);
            unset($data["hndsubcateid"]);
            if (isset($data['start_time']))
                $data['start_time'] = date("H:i:s", strtotime($data['start_time']));
            if (isset($data['end_time']))
                $data['end_time'] = date("H:i:s", strtotime($data['end_time']));

            $product = $this->Products->patchEntity($product, $data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Product details are updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save product. Please, try again.'));
        }
        $this->set(compact('product','categories'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Detail id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('Product has been deleted successfully.'));
        } else {
            $this->Flash->error(__('The product detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

   
}
