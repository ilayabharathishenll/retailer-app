<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Payments');
        $this->loadModel('CartProducts');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['reference_number'])) {
                $conditions['Payments.reference_number'] = trim($searchParams['reference_number'], " ");
            }
            if(!empty($searchParams['shop_name'])) {
                $conditions['Shops.shop_name like'] = trim("%" . $searchParams['shop_name'] . "%", "");
            }
            if(!empty($searchParams['area'])) {
                $conditions['Shops.area'] = trim($searchParams['area'], " ");
            }
        }
        $conditions['transaction_status'] = 'authorized';
        $order_details = $this->Payments->find("all", ['contain' => array('Shops', 'CartProducts' => ['TblProducts'])])
            ->where($conditions)->order("payment_id DESC")->toArray();

        if ($order_details) {
            $cartArrayID=array();
            foreach ($order_details as $order) {
                $data = $order;
                
                if ($order['transaction_date']){
                    $data['transaction_date'] = date("d/m/Y", strtotime($order['transaction_date']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                }
                $data["items"] = $this->getProducts($order["cart_id"]);
                $orders[] = $data;
                
            }
          
        }
        $this->set(compact('orders', 'searchParams'));
    }
    public function getProducts($cart_id) {
        $products = array();
        $cart_products = $this->CartProducts->find("all", array('order'=>array('CartProducts.created_on'=>'desc'),'contain' => array('TblProducts') ))
            ->where(['CartProducts.cart_id'=> $cart_id])->toArray();
            // print_r($cart_products);exit;
        if ($cart_products) {
            foreach ($cart_products as $cart_product) {
                $$product = array();
                $product['productName'] = $cart_product['tbl_product']['product_name'];
                $product['unit_price'] = $cart_product['unit_price'];
                $product['unit_total'] = $cart_product['unit_total'];
                $product['quantity'] = $cart_product['qty'];
                $product['product_type'] = $cart_product['product_type'];
                $product['discount_amount'] = ($cart_product['discount_amount']) ? $cart_product['discount_amount'] : 0;
                $product['discount_percentage'] = ($cart_product['discount_percentage']) ? $cart_product['discount_percentage'] : 0;
                $product['delivery_charge'] = ($cart_product['delivery_charge']) ? $cart_product['delivery_charge'] : 0;
                $product['total_amount'] = ($cart_product['total_amount']) ? $cart_product['total_amount'] : 0;
                $product['package_size'] = ($cart_product['package_size']) ? $cart_product['package_size'] : "";
                $products[] = $product;
            }
        }
        return $products;
    }
    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $order = $this->Orders->newEntity();
    //     if ($this->request->is('post')) {
    //         $order = $this->Orders->patchEntity($order, $this->request->getData());
    //         if ($this->Orders->save($order)) {
    //             $this->Flash->success(__('The order has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The order could not be saved. Please, try again.'));
    //     }
    //     // $users = $this->Orders->Users->find('list', ['limit' => 200]);
    //     $this->set(compact('order', 'users'));
    // }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $order = $this->Orders->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $order = $this->Orders->patchEntity($order, $this->request->getData());
    //         if ($this->Orders->save($order)) {
    //             $this->Flash->success(__('The order has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The order could not be saved. Please, try again.'));
    //     }
    //     // $users = $this->Orders->Users->find('list', ['limit' => 200]);
    //     $this->set(compact('order', 'users'));
    // }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
