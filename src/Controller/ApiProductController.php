<?php
namespace App\Controller;
use Razorpay\Api\Api;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;


class ApiProductController extends AppController{

    const success_payment = "Payment Details Getting Successfully";
    const failed_payment = "Invalid PaymentId";
    const success_refund = "Payment Refund Successfully";
    const failed_refund = "Invalid PaymentId";
    const success_cards= "Card Details Getting Successfully";
    const failed_cards = "Invalid Card Number";
    const success_transfers = "Transfer Successfully";
    const failed_transfers = "Invalid PaymentId";

    public function initialize(){

        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Shops');
        $this->loadModel('Products');
        $this->loadModel('Discounts');
        $this->loadModel('Carts');
        $this->loadModel('CartProducts');
        $this->loadModel('Orders');
        $this->loadModel('Payments');
        $this->loadModel('Settings');
        $this->loadModel('Offer');
        $this->loadModel('OfferHistory');
        $this->loadModel('WelcomeOffer');
        $this->loadModel('SubCategory');
        //razorpay api access key_id and key_secret
        $this->api = new Api('rzp_live_tpqmikVdLeaUZs', 'cjbMiJWkf5WceYMv5DlZrk2D');
    }

    public function beforeFilter(Event $event){
       $this->Auth->allow(array('index','productView','productAdd','productEdit','productDelete','promotion','cartUser','cartAdd','cartUpdate','cartDelete','payment','refunds','checkout','cards','transfers','paymentList'));
    }
    public function index($category=null){
        $productDetails=array();
        if ($this->request->getData(['category']) && !empty($this->request->getData(['type']))) {
            $productDetails = $this->Products->listing($this->request->getData(['category']),$this->request->getData(['type']));
        } elseif ($this->request->getData(['category']) && !empty($this->request->getData(['userId']))) {
            $cart=$this->Carts->check($this->request->getData('userId'));
            if(empty($cart)) {
                $cart_id="";
            } else {
                $cart_id=$cart[0]->cart_id;
            }
            $productDetails = $this->Products->listing($this->request->getData(['category']),'',$this->request->getData(['userId']), $cart_id, $this->request->getData(['searchKey']));
        }
        else {
            $productDetails = $this->Products->listing();
        }
        $subcategories = TableRegistry::get('tbl_sub_categories')->find()->where(['category_id'=>$this->request->getData(['category'])])->toArray();
        if (count($subcategories)>0) {
           $sub_category_available=1;
        } else {
            $sub_category_available=0;
        }
        if($productDetails) {
            $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'subCategoryAvailable'=>$sub_category_available,
                'result' => $productDetails,
            );
        } else {
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Product(s) not found',
                'subCategoryAvailable'=>$sub_category_available,
                'result' => array(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function promotion($user_id) {
        $cart_id = $this->getCartId($user_id);
        $promotionDetails = $this->Discounts->promotion($user_id, $cart_id);
        if($promotionDetails) {
            $output = array (
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'result' => $promotionDetails,
            );
        }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'No Promotion(s) found',
                'result' => array(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function productView($pid)
    {
        $view = $this->Products->get($pid);
        if($view){
            $output = array(
                'status'=> 'success',
                'errorCode' =>1,
                'message'=>'successfully get',
                'result' => $view,
                );
        } else {
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function productAdd()
    {
        $product = $this->Products->newEntity($this->request->getData());
        if ($this->Products->save($product)) {
            $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'result' => array(),
            );
        }
        else {
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function productEdit($id)
    {
        $user = $this->Products->get($id);
        $users=array();
        if ($this->request->is(['post', 'put'])) {
            $users = $this->Products->patchEntity($user, $this->request->getData());
            if ($this->Products->save($users)) {
                $message = 'Saved';
            } else {
                $message = 'Error';
            }
        }
        if($users) {
            $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'result' => new \stdClass(),
            );
        }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Invalid status or reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function productDelete($id)
    {
        $product_delete = $this->Products->get($id);
        $message = 'Deleted';
        if (!$this->Products->delete($product_delete)) {
            $output = array (
                'status'=> 'success',
                'errorCode'=>1,
                'message'=>'success',
                'result' => new \stdClass(),
            );
        }
        else{
            $output = array (
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function getCartId($user_id) {
        $cart=$this->Carts->check($user_id);
        if(empty($cart)) {
            $query = $this->Carts->newEntity();
            $data=array(
                'shop_id'=>$shop_id=$user_id,
                'total_products'=>0,
                'total_discounts'=>0,
                'net_total'=>0,
                'grand_total'=>0,
                'status'=>'Active',
                'created_on'=>date('Y-m-d H:i:s'),
                'updated_on'=>date('Y-m-d H:i:s')
            );
            $query = $this->Carts->query()
                ->insert(['shop_id', 'total_products', 'total_discounts', 'net_total', 'grand_total', 'status', 'created_on', 'updated_on'])
                ->values($data)
                ->execute();
            $id = $query->lastInsertId('id');
        } else {
            $id=$cart[0]->cart_id;
        }
        return $id;
    }

    public function cartAdd()
    {
        if ($this->request->is(['post', 'put'])) {
            if (!empty($this->request->getData('productId'))) {
                try {
                    $id = $this->getCartId($this->request->getData('userId'));
                    $discount_id=$this->request->getData('discountId');
                    $promotion_id=$this->request->getData('promotionId');
                    $product_id=$this->request->getData('productId');
                    $qty=$this->request->getData('quantity');
                    $package_size=$this->request->getData('packageSize');
                    $productData = TableRegistry::get('tbl_products')->find()->select(['total_stock'=>'total_stock'])->where(['product_id'=>$product_id])->toArray();
                    $totalStock = $productData[0]['total_stock'];
                    $apply_discount_id = '';
                    $discount_kg = NULL;
                    if ($discount_id!='') {
                        $discount=$this->Discounts->get($discount_id);
                        $apply_discount_id = $discount->discount_id;
                    } else if ($promotion_id!='') {
                        $discount=$this->Discounts->get($promotion_id);
                        $apply_discount_id = $discount->discount_id;
                        $qty= $discount->weight;
                        $product_id=$discount->product_id;
                        $promotionData = TableRegistry::get('tbl_discounts')->find()->select(['total_stock'=>'total_stock', 'discount_type'=>'discount_type'])->where(['discount_id'=>$promotion_id])->toArray();
                        // print_r($totalStock);
                        if($promotionData[0]['discount_type'] == "Promotion") {
                            $totalStock = $promotionData[0]['total_stock'];
                        }
                        // print_r($totalStock);exit;
                    } else {
                        $discount=array();
                    }
                    $product_details = array (
                        'cart_id'=>$id,
                        'product_id'=>$product_id,
                        'discount_id'=>$apply_discount_id,
                    );

                    $check_products = $this->CartProducts->checkProducts($product_details);
                    $product = $this->Products->get($product_id);
                    $unit_price = $product->price;

                    if (!empty($check_products)) {
                        if ($check_products->total_products) {
                            $qty = $check_products['qty'] + $qty;
                            $current_qty = $check_products['qty'];
                        }
                    } else {
                        $current_qty = 0;
                    }

                    $unit_total = $qty * $unit_price;
                    $total_price = $unit_total;

                    $product_type = 'Product';
                    $discount_per = $discount_amount = 0;
                    if(!empty($discount)){
                        if($discount->discount_type=='Promotion') {
                           if($discount->product_id==$product_id){
                            $price_per_unit = $discount->promotion_price / $discount->weight;
                            $total_price = $qty * $price_per_unit;
                            $unit_total = 0;
                            $product_type = 'Promotion';
                           }
                        }else{
                            $discount_per = $discount->discount_percentage;
                            if($discount->aggregate=="<="){
                                if($qty <= $discount->weight) {
                                    $discount_amount = $unit_total*$discount_per/100;
                                    $total_price = $unit_total - $discount_amount;
                                }
                            }elseif($discount->aggregate==">="){
                                if($qty >= $discount->weight){
                                    $discount_amount = $unit_total*$discount_per/100;
                                    $total_price = $unit_total - $discount_amount;
                                }
                            }elseif($discount->aggregate=="=="){
                                if($discount->weight == $qty){
                                    $discount_amount = $unit_total*$discount_per/100;
                                    $total_price = $unit_total - $discount_amount;
                                }
                            }elseif($discount->aggregate=="<"){
                                if($qty < $discount->weight){
                                    $discount_amount = $unit_total*$discount_per/100;
                                    $total_price = $unit_total - $discount_amount;
                                }
                            }elseif($discount->aggregate==">"){
                                if($qty > $discount->weight){
                                    $discount_amount = $unit_total*$discount_per/100;
                                    $total_price = $unit_total - $discount_amount;
                                }
                            }
                            $discount_kg = $discount->weight;
                            $product_type = 'Product';
                        }
                    }
                    $quantity=$this->Settings->getSettings();
                    if(!empty($quantity)){
                       $delivery_charge = $quantity["delivery charge per kg"];
                    } else{
                       $delivery_charge='0';
                    }
                    $delivery_charge = ($qty*$delivery_charge);
                    $totalamount = $total_price;
                    if($totalStock >= $qty) {
                        $data=array(
                            'cart_id'=>$id,
                            'product_id'=>$product_id,
                            'qty'=>$qty,
                            'discount_id'=>$apply_discount_id,
                            'discount_kgs'=>$discount_kg,
                            'unit_price'=>$unit_price,
                            'unit_total'=>$unit_total,
                            'discount_amount'=>$discount_amount,
                            'discount_percentage'=>$discount_per,
                            'total_amount'=>$totalamount,
                            'delivery_charge'=>$delivery_charge,
                            'product_type'=>$product_type,
                            'package_size'=>$package_size,
                            'status'=>'Active',
                            'created_on'=>date('Y-m-d H:i:s'),
                            'modified_on'=>date('Y-m-d H:i:s')
                        );
                        if (!empty($check_products) && $check_products->total_products) {
                            $cart_product = $this->CartProducts->get($check_products->cart_product_id);
                            $cartProductQuery = $this->CartProducts->patchEntity($cart_product,$data);
                        } else {
                            $cartProductQuery = $this->CartProducts->newEntity($data);
                        }
                        //print_r($cartProductQuery); exit;

                        if($this->CartProducts->save($cartProductQuery)){
                            $cart_prodcut_id = $cartProductQuery->cart_product_id;
                            $cart_count = $this->total_cart_update($id);
                        }
                        $output = array(
                            'status'=> 'success',
                            'errorCode'=>1,
                            'message'=>'Product added to cart successfully',
                            'result' => array('cartId'=>$id,'cartProductId'=>$cart_prodcut_id,'cartCount'=>$cart_count,'availableStock'=>$totalStock-$qty)
                        );
                    } else {
                        $id = $this->getCartId($this->request->getData('userId'));
                        $cart_count = $this->total_cart_update($id);
                        $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>'Quantity exceeded',
                            'result' => array('cartId'=>$id,'cartCount'=>$cart_count,'availableStock'=>$totalStock-$current_qty)
                        );
                    }
                }catch(\Exception $e){
                    $output = array(
                        'status'=> 'success',
                        'errorCode'=>0,
                        'message'=>'Product not added',
                        'result' => new \stdClass(),
                    );
                }
            }
        } else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Please check the data',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function cartUpdate()
    {
        if ($this->request->is(['post', 'put'])) {
            try{
                $cart_id=$this->request->getData('cartId');
                $cart_product_id=$this->request->getData('cartProductId');
                $user_id=$this->request->getData('userId');
                $discount_id=$this->request->getData('discountId');
                $promotion_id=$this->request->getData('promotionId');
                $product_id=$this->request->getData('productId');
                $qty=$this->request->getData('quantity');
                $package_size=$this->request->getData('packageSize');
                $productData = TableRegistry::get('tbl_products')->find()->select(['total_stock'=>'total_stock'])->where(['product_id'=>$product_id])->toArray();
                $totalStock = $productData[0]['total_stock'];
                $apply_discount_id = NULL;
                $discount_kg = NULL;
                $cart = $this->Carts->get($cart_id);
                if(!empty($cart) && $cart->status=='Active'){
                    $cartProductGetId = $this->CartProducts->get($cart_product_id);
                    if(!empty($cartProductGetId) && $cartProductGetId->status=='Active'){
                        $product = $this->Products->get($product_id);
                        if($discount_id!=''){
                            $discount=$this->Discounts->get($discount_id);
                            $apply_discount_id = $discount->discount_id;
                        }elseif ($promotion_id!='') {
                            $discount=$this->Discounts->get($promotion_id);
                            $apply_discount_id = $discount->discount_id;
                        }else {
                            $discount=array();
                        }
                        $unit_price = $product->price;
                        $unit_total = $qty * $unit_price;
                        $total_price = $unit_total;
                        $discount_per = $discount_amount = NULL;
                        if(!empty($discount)){
                            if($discount->discount_type=='Promotion'){
                               if($discount->weight==$qty && $discount->product_id==$product_id){
                                $total_price = $discount->promotion_price;
                                $discount_amount = $unit_total - $total_price;
                               }else{
                                $apply_discount_id = NULL;
                               }
                            }else{
                                if($discount->aggregate=="<="){
                                    if($qty <= $discount->weight){
                                        $discount_per = $discount->discount_percentage;
                                        $discount_amount = $unit_total*$discount_per/100;
                                        $total_price = $unit_total - $discount_amount;
                                    }
                                }elseif($discount->aggregate==">="){
                                    if($qty >= $discount->weight){
                                        $discount_per = $discount->discount_percentage;
                                        $discount_amount = $unit_total*$discount_per/100;
                                        $total_price = $unit_total - $discount_amount;
                                    }
                                }elseif($discount->aggregate=="=="){
                                    if($discount->weight == $qty){
                                        $discount_per = $discount->discount_percentage;
                                        $discount_amount = $unit_total*$discount_per/100;
                                        $total_price = $unit_total - $discount_amount;
                                    }
                                }elseif($discount->aggregate=="<"){
                                    if($qty < $discount->weight){
                                        $discount_per = $discount->discount_percentage;
                                        $discount_amount = $unit_total*$discount_per/100;
                                        $total_price = $unit_total - $discount_amount;
                                    }
                                }elseif($discount->aggregate==">"){
                                    if($qty > $discount->weight){
                                        $discount_per = $discount->discount_percentage;
                                        $discount_amount = $unit_total*$discount_per/100;
                                        $total_price = $unit_total - $discount_amount;
                                    }
                                }
                                $discount_kg = $discount->weight;
                            }
                        }
                        $quantity=$this->Settings->getSettings();
                        if(!empty($quantity)){
                           $delivery_charge = $quantity["delivery charge per kg"];
                        } else{
                           $delivery_charge='0';
                        }
                        $delivery_charge = ($qty*$delivery_charge);
                        $totalamount = $total_price;
                        if($totalStock >= $qty) {
                            $data=array(
                                'cart_product_id'=>$cart_product_id,
                                'cart_id'=>$cart_id,
                                'product_id'=>$product_id,
                                'qty'=>$qty,
                                'discount_id'=>$apply_discount_id,
                                'discount_kgs'=>$discount_kg,
                                'unit_price'=>$unit_price,
                                'unit_total'=>$unit_total,
                                'delivery_charge'=>$delivery_charge,
                                'discount_amount'=>$discount_amount,
                                'discount_percentage'=>$discount_per,
                                'total_amount'=>$totalamount,
                                'package_size'=>$package_size,
                                'modified_on'=>date('Y-m-d H:i:s')
                            );
                            $cartProductQuery = $this->CartProducts->patchEntity($cartProductGetId,$data);
                            if($this->CartProducts->save($cartProductQuery)){
                                $cart_count = $this->total_cart_update($cart_id);
                            }
                            $output = array(
                                        'status'=> 'success',
                                        'errorCode'=>1,
                                        'message'=>'Cart Updated Successfully',
                                        'result' => array('cartId'=>$cart_id,'cartProductId'=>$cart_product_id,'cartCount'=>$cart_count,'availableStock'=>$totalStock)
                                    );
                        } else {
                            $cart_count = $this->total_cart_update($cart_id);
                            $output = array(
                                'status'=> 'success',
                                'errorCode'=>0,
                                'message'=>'Quantity exceeded',
                                'result' => array('cartId'=>$cart_id,'cartCount'=>$cart_count,'availableStock'=>$totalStock)
                            );
                        }
                    }else{
                        $output = array(
                                'status'=> 'success',
                                'errorCode'=>0,
                                'message'=>'Cart product not found',
                                'result' => new \stdClass(),
                            );
                    }
                }else{
                     $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>'Cart not found',
                            'result' => new \stdClass(),
                        );
                }
            }catch(\Exception $e){
                $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>"Record Not Found",
                            'result' => new \stdClass(),
                        );
            }
        }else{
            $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>'Please check the data',
                            'result' => new \stdClass(),
                        );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function total_cart_update($id){
        $get_total=$this->CartProducts->cart_total($id);
        // print_r($get_total); exit;
        //Updating to cart table
        $update_cart = $this->Carts->get($id);
        if($get_total->total_products==0) {
            $update["total_discounts"]=0;
            $update["total_products"]=0;
            $update["total_delivery_charge"]=0;
            $update["offer_id"]=NULL;
            $update["order_price"]=0;
            $update["net_total"]=0;
            $update["grand_total"]=0;
            $update["offer_discount"]=NULL;
            $carts_update = $this->Carts->patchEntity($update_cart, $update);
            $this->Carts->save($carts_update);
            return $update['total_products'];
        }
        if(!empty($get_total)){
            $shop_id=$update_cart->shop_id;
            if (!empty($shop_id)) {
                $CheckShopExists = TableRegistry::get('tbl_carts')->find('all')->select()->where(['shop_id='.$shop_id,"status"=>"Paid"])->first();
                if (count($CheckShopExists) == 0) {
                    $update=array(
                        'total_products'=>$get_total->total_products==0?0:$get_total->total_products,
                        'total_discounts'=>(!$get_total->total_discount)?0:$get_total->total_discount,
                        'total_delivery_charge'=>(!$get_total->total_delivery_charge)?0:$get_total->total_delivery_charge,
                        'net_total'=>(!$get_total->net_total)?0:$get_total->net_total,
                        'grand_total'=>(!$get_total->grand_total)?0:$get_total->grand_total,
                        'order_price'=>(!$get_total->grand_total)?0:($get_total->grand_total + $get_total->total_delivery_charge),
                        'updated_on'=>date("Y-m-d H:i:s")
                    );
                    
                    $carts_update = $this->Carts->patchEntity($update_cart, $update);
                    $this->Carts->save($carts_update);
                    return $update['total_products'];
                } else {
                    $condition['status'] = 'Active';
                    $GetOfferDetails = TableRegistry::get('tbl_welcome_offer')->find('all')->select()->where($condition)->order('updated_on DESC')->first();
                    $payment_date         = date("Y-m-d",strtotime($CheckShopExists->payment_date));
                    $min_order_amt        = $GetOfferDetails->min_order_amt;
                    $discount_amt         = $GetOfferDetails->discount_amt;
                    $no_of_days           = $GetOfferDetails->no_of_days;
                    $offer_id             = $GetOfferDetails->welcome_id;
                    $plusenextdate        = date('Y-m-d', strtotime($payment_date."+1 days"));
                    $lastdate             = date('Y-m-d', strtotime($plusenextdate."+".$no_of_days."days"));
                    if(($min_order_amt <= $CheckShopExists->grand_total) && (strtotime(date('Y-m-d')) != strtotime($payment_date)) && (strtotime(date('Y-m-d')) >= strtotime($plusenextdate) && strtotime(date('Y-m-d')) <= strtotime($lastdate))) {
                        $offer_discount = ($get_total->grand_total*$discount_amt)/100;
                        $order_amount   = $get_total->grand_total-$offer_discount;
                        $update=array(
                            'total_products'=>$get_total->total_products==0?0:$get_total->total_products,
                            'total_discounts'=>(!$get_total->total_discount)?0:$get_total->total_discount,
                            'total_delivery_charge'=>(!$get_total->total_delivery_charge)?0:$get_total->total_delivery_charge,
                            'net_total'=>(!$get_total->net_total)?0:$get_total->net_total,
                            'grand_total'=>(!$get_total->grand_total)?0:$get_total->grand_total,
                            'order_price'=>(!$get_total->grand_total)?0:($get_total->grand_total + $get_total->total_delivery_charge),
                            'offer_discount'=>$offer_discount==''?"":$offer_discount,
                            'offer_id'=>$offer_id==''?"":$offer_id,
                            'updated_on'=>date("Y-m-d H:i:s")
                        );
                        if( $get_total->total_products==0) {
                            $update["total_discounts"]=0;
                            $update["offer_id"]=NULL;
                            $update["offer_discount"]=NULL;
                        }
                        $carts_update = $this->Carts->patchEntity($update_cart, $update);
                        $this->Carts->save($carts_update);
                        return $update['total_products'];
                    } else {
                        $update=array(
                            'total_products'=>$get_total->total_products==0?0:$get_total->total_products,
                            'total_discounts'=>(!$get_total->total_discount)?0:$get_total->total_discount,
                            'total_delivery_charge'=>(!$get_total->total_delivery_charge)?0:$get_total->total_delivery_charge,
                            'net_total'=>(!$get_total->net_total)?0:$get_total->net_total,
                            'grand_total'=>(!$get_total->grand_total)?0:$get_total->grand_total,
                            'order_price'=>(!$get_total->grand_total)?0:($get_total->grand_total + $get_total->total_delivery_charge),
                            'updated_on'=>date("Y-m-d H:i:s")
                        );
                        if( $get_total->total_products==0) {
                            $update["total_discounts"]=0;
                            $update["offer_id"]=NULL;
                            $update["offer_discount"]=NULL;
                        }
                        $carts_update = $this->Carts->patchEntity($update_cart, $update);
                        $this->Carts->save($carts_update);
                        return $update['total_products'];
                    }
                }
            } else{
                $update=array(
                    'total_products'=>$get_total->total_products==0?0:$get_total->total_products,
                    'total_discounts'=>(!$get_total->total_discount)?0:$get_total->total_discount,
                    'total_delivery_charge'=>(!$get_total->total_delivery_charge)?0:$get_total->total_delivery_charge,
                    'net_total'=>(!$get_total->net_total)?0:$get_total->net_total,
                    'grand_total'=>(!$get_total->grand_total)?0:$get_total->grand_total,
                    'order_price'=>(!$get_total->grand_total)?0:($get_total->grand_total + $get_total->total_delivery_charge),
                    'updated_on'=>date("Y-m-d H:i:s")
                );
                if( $get_total->total_products==0) {
                    $update["total_discounts"]=0;
                    $update["offer_id"]=NULL;
                    $update["offer_discount"]=NULL;
                }
                $carts_update = $this->Carts->patchEntity($update_cart, $update);
                $this->Carts->save($carts_update);
                return $update['total_products'];
            }
        }
    }

    public function cartUser($id){
        $cart_details = $this->Carts->listing($id);
        if($cart_details){
            $output = array(
            'status'=> 'success',
            'errorCode' =>1,
            'message'=>'successfully get',
            'result' => $cart_details,
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'No Cart Found',
                'result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function cartDelete()
    {
        if ($this->request->is(['post', 'put'])) {
            try{
                $cart_id=$this->request->getData('cartId');
                $cart_product_id=$this->request->getData('cartProductId');
                $user_id=$this->request->getData('userId');
                $cart=$this->Carts->check($user_id,$cart_id);
                if(!empty($cart)){
                    $cartProductGetId = $this->CartProducts->get($cart_product_id);
                    if(!empty($cartProductGetId)){
                        $this->CartProducts->cart_product_delete($cart_product_id,$cart_id);
                        $cart_count = $this->total_cart_update($cart_id);
                        $output = array(
                            'status'=> 'success',
                            'errorCode'=>1,
                            'message'=>'Cart product deleted successfully',
                            'cartCount'=>$cart_count,
                            'result' => new \stdClass(),
                        );
                    }else{
                        $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>'Cart Product not found',
                            'result' => new \stdClass(),
                        );
                    }

                }else{
                    $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>'Cart not found',
                            'result' => new \stdClass(),
                        );
                }
            }catch(\Exception $e){
                $output = array(
                            'status'=> 'success',
                            'errorCode'=>0,
                            'message'=>"Record Not Found",
                            'result' => new \stdClass(),
                        );
            }
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    //payment
    public function payment(){
        //particular payment_id
        $payment_id = $this->request->getData('razorpay_payment_id');
        
        if(!empty($payment_id)){
            if($this->request->is('post')){
                $payment = $this->api->payment->fetch($payment_id)->toArray(); // Returns a particular payment
                $data['reference_number'] = $payment['id'];
                $data['cart_id'] = $payment['notes']['cart_id'];
                $data['shop_id'] = $payment['notes']['userid'];
                $data['payment_type'] = $payment['method'];
                $data['transaction_amount'] = $payment['amount'];
                $data['transaction_status'] = $payment['status'];
                $data['transaction_date'] = date('Y-m-d H:i:s', $payment['created_at']);
                $data['reference_token'] = $payment['notes']['reference_token'];
                $cart_id = $payment['notes']['cart_id'];
                $order = $this->Orders->find("all")->where(['reference_token'=> $data['reference_token'] ])->toArray();
                $order_id = $order[0]->order_id;
                $cart = $this->Carts->get($cart_id);
                $cartProducts = $this->CartProducts->find("all")->where(['cart_id'=> $cart_id ])->toArray();
                if (!empty($cartProducts)) {
                    foreach ($cartProducts as $product) {
                        $promotion = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Promotion','discount_id'=>$product['discount_id'], 'product_id'=>$product["product_id"]])->toArray();
                        if (!empty($promotion)) {
                            $this->Discounts->updateStock($product['product_id'], $product['qty'], $product['discount_id']);
                        } else {
                            $this->Products->updateStock($product['product_id'], $product['qty']);
                        }
                    }
                }
                $cart_update=array(
                    'order_id'=>$order_id,
                    'status'=>'Paid',
                    'payment_date'=>date("Y-m-d"),
                    'updated_on'=>date("Y-m-d H:i:s")
                );
                $carts_update = $this->Carts->patchEntity($cart, $cart_update);
                $this->Carts->save($carts_update);
                if (!empty($cart_id)) {
                    $CheckCartOfferExists = TableRegistry::get('tbl_carts')->find('all')->select()->where('cart_id='.$cart_id)->first();
                    $offer_id      = $CheckCartOfferExists->offer_id;
                    $offer_discount = $CheckCartOfferExists->offer_discount;
                    if(!empty($offer_id)){
                        $historydata=array(
                            'shop_id'=>$payment['notes']['userid'],
                            'offer_id'=>$offer_id,
                            'cart_id'=>$payment['notes']['cart_id'],
                            'offer_discount_amt'=>$offer_discount,
                            'created_on'=>date("Y-m-d H:i:s"),
                            'updated_on'=>date("Y-m-d H:i:s")
                        );
                        $offerdata = $this->OfferHistory->newEntity();
                        $Offerhistory = $this->OfferHistory->patchEntity($offerdata, $historydata);
                        if($this->OfferHistory->save($Offerhistory, $historydata)){
                        }
                    }
                }
                $exists = $this->Payments->exists(['reference_token'=>$data['reference_token'],'reference_number'=>$data['reference_number']]);
                if(!$exists) {
                    $user = $this->Payments->newEntity();
                    $pay = $this->Payments->patchEntity($user, $data);
                    if($this->Payments->save($pay, $data)){
                        $result =  $payment;
                    }
                } else {
                    $result =  $payment;
                }
            }
        }else{
            $result['status'] = "failure";
            $result['errorCode'] = 0;
            $result['message'] = "PaymentId Is Required Or Invalid PaymentId";
        }
        $this->layout = 'empty';
        $this->set(compact('result'));
        $this->render('payment');
    }


    //refunds
    public function refunds(){

       //particular payment_id and amount
        $refund['payment_id'] =  'pay_B92BUfV6ZHETgD';
        $refund['amount'] =  '5000';

        $refunds = $this->api->refund->create($refund)->toArray(); // Returns a particular payment
        if($refunds){
            $result['status'] = "success";
            $result['errorCode'] = 1;
             $result['message'] = $this::success_refund;
             $result['result'] =  $refunds;
        }else{
            $result['status'] = "failure";
            $result['errorCode'] = 0;
            $result['message'] = $this::failed_refund;
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($result);
    }

    //checkout
    public function checkout(){
        $this->autoRender = false;
        $requestData = $this->request->getData();
        $data['userId'] = $requestData['userId'];
        $cart = $this->Carts->check($data['userId']);
        if(empty($cart)) {
            $query = $this->Carts->newEntity();
            $query->shop_id = $data['userId'];
            $query->total_products = 0;
            $query->status = 'Active';
            $query->created_on = date("Y-m-d H:i:s");
            $query->updated_on = date("Y-m-d H:i:s");
            if($this->Carts->save($query)) {
               $cart_id = $query->cart_id;
            }
            $cart = $this->Carts->check($data['userId']);
        }else{
            $cart_id=$cart[0]->cart_id;
        }
        $offer_discount = ($cart[0]->offer_discount) ? $cart[0]->offer_discount : 0;
        $data['amount'] = ($cart[0]->order_price - $offer_discount) * 100;
        $data['cart_id'] = $cart_id;
        if(!empty($data['userId'])){
            $user = $this->Shops->view($data['userId']);
            if (!empty($user)) {
                $data['email'] = $user['email'];
                $data['name'] = $user['firstName'].' '.$user['lastName'];
                $data['logo'] = $user['shopLogo'];
            }
            $token = bin2hex(random_bytes(4));
            $exists = $this->Orders->exists(['reference_token'=>$token]);
            if(!$exists){
                if($this->request->is('post') || $this->request->is('put')){
                    $user = $this->Orders->newEntity();
                    $data['reference_token'] = $token;
                    $orders = $this->Orders->patchEntity($user, $data);
                    if($order = $this->Orders->save($orders, $data)){
                        $result['status'] = "success";
                        $result['errorCode'] = 1;
                        $result['message'] = "Checkout Successfully";
                        if ($requestData['paymentType'] === 'card') {
                            $result['result'] ='<div class="order-status">
                                                <form name="checkout" action="'.BASE_URL.'/api/retailer-payment/" method="POST">
                                                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                                                    data-key="rzp_live_tpqmikVdLeaUZs"
                                                    data-amount='.$data['amount'].'
                                                    data-buttontext="Proceed to Pay"
                                                    data-name='.$data['name'].'
                                                    data-description="Purchase Description"
                                                    data-image='.$data['logo'].'
                                                    data-prefill.name='.$data['name'].'
                                                    data-prefill.email='.$data['email'].'
                                                    data-theme.color="#F02044"
                                                    data-notes.userid='.$data['userId'].'
                                                    data-notes.cart_id='.$data['cart_id'].'
                                                    data-notes.reference_token='.$data['reference_token'].'
                                                    ></script>
                                                    <input type="hidden" value="Hidden Element" name="hidden">
                                                </form>
                                            </div>
                                            <script type="text/javascript">
                                                document.querySelector(".razorpay-payment-button").click();
                                            </script>';
                        } else if ($requestData['paymentType'] === 'cod') {

                            $cart = $this->Carts->get($cart_id);

                            $cartProducts = $this->CartProducts->find("all")->where(['cart_id'=> $cart_id ])->toArray();
                            if (!empty($cartProducts)) {
                                foreach ($cartProducts as $product) {
                                    $promotion = TableRegistry::get('tbl_discounts')->find()->where(['discount_type'=>'Promotion','discount_id'=>$product['discount_id'], 'product_id'=>$product["product_id"]])->toArray();
                                    if (!empty($promotion)) {
                                        $this->Discounts->updateStock($product['product_id'], $product['qty'], $product['discount_id']);
                                    } else {
                                        $this->Products->updateStock($product['product_id'], $product['qty']);
                                    }
                                }
                            }
                            $cart_update=array(
                                'order_id'=>$order->order_id,
                                'status'=>'Ordered',
                                'updated_on'=>date("Y-m-d H:i:s")
                            );
                            $carts_update = $this->Carts->patchEntity($cart, $cart_update);
                            $this->Carts->save($carts_update);

                            $payment = $this->Payments->newEntity();
                            $paymentData = array(
                                'shop_id' => $data['userId'],
                                'cart_id' => $data['cart_id'],
                                'reference_token' => $data['reference_token'],
                                'payment_type' => $requestData['paymentType'],
                                'transaction_amount' => $data['amount'],
                                'transaction_status' => 'PENDING',
                            );
                            $payment = $this->Payments->patchEntity($payment, $paymentData);
                            $result['result'] = $order->order_id;
                            if($this->Payments->save($payment, $data)){
                                $result['result'] = $cart_id;
                            } else {
                                $result['status'] = "failure";
                                $result['errorCode'] = 0;
                                $result['message'] = "Record Not Saved";
                            }
                        }
                    } else {
                        $result['status'] = "failure";
                        $result['errorCode'] = 0;
                        $result['message'] = "Record Not Saved";
                    }
                }
            }else{
                $result['status'] = "failure";
                $result['errorCode'] = 0;
                $result['message'] = " Reference Token Already exist";
            }
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($result);
    }

    //cards
    public function cards(){
        $cardId = "card_B92BWmGjzM05yD";
        $card = $this->api->card->fetch($cardId)->toArray(); // Returns a particular card

        if($card){
            $result['status'] = "true";
            $result['errorCode'] = 1;
             $result['message'] = $this::success_cards;
             $result['result'] =  $card;
        }else{
            $result['status'] = "false";
            $result['errorCode'] = 0;
            $result['message'] = $this::failed_cards;
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($result);
    }

    //transfers
    public function transfers(){

        $paymentId = "pay_B92BUfV6ZHETgD";
        $transfers = $this->api->payment->fetch($paymentId)->transfers()->toArray(); // Returns a particular card
        if($transfers){
            $result['status'] = "Success";
            $result['errorCode'] = 1;
             $result['message'] = $this::success_transfers;
             $result['result'] =  $transfers;
        }else{
            $result['status'] = "Failure";
            $result['errorCode'] = 0;
            $result['message'] = $this::failed_transfers;
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($result);
    }

    //user paymentList
    public function paymentList($userId){
        if($this->request->is('get')){
            $find = $this->Payments->find("all", array('order'=>array('Payments.created_on'=>'desc'),'contain' => array('Carts') ))
                ->where(['Carts.shop_id'=> $userId, 'Carts.status IN' => ['Ordered', 'Paid']])->toArray();

            // print_r($find);exit;
            if($find){
                $result['status'] = "success";
                $result['errorCode'] = 1;
                $result['message'] = "User Payment Details Getting Successfully";
                foreach ($find as $key => $value) {
                    $data['orderId'] = isset($value['cart']['cart_id']) ? $value['cart']['cart_id'] : "";
                    $data['totalProduct'] = $value['cart']->total_products;
                    $data['date'] = date("d F, Y", strtotime($value['created_on']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                    $data['time'] = date("h:i a", strtotime($value['created_on']->i18nFormat([\IntlDateFormatter::NONE, \IntlDateFormatter::LONG])));
                    $data['orderStatus'] = 'Completed';
                    $data['amount'] = $value['transaction_amount'] / 100;
                    $data['items'] = $this->getProducts($value['cart']['cart_id']);
                    $completed_orders[] =  $data;   
                }
                $completed_orders = $this->group_by($completed_orders, 'date');
                $result['result']['completedOrders'] = $completed_orders;
            }else{
                $result['status'] = "success";
                $result['errorCode'] = 0;
                $result['message'] ="No Order(s) Found";
            }
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($result);
    }

    public function getProducts($cart_id) {
        $products = array();
        $cart_products = $this->CartProducts->find("all", array('order'=>array('CartProducts.created_on'=>'desc'),'contain' => array('TblProducts') ))
            ->where(['CartProducts.cart_id'=> $cart_id])->toArray();
        if ($cart_products) {
            foreach ($cart_products as $cart_product) {
                $$product = array();
                $product['productName'] = $cart_product['tbl_product']['product_name'];
                $product['unit_price'] = $cart_product['unit_price'];
                $product['unit_total'] = $cart_product['unit_total'];
                $product['quantity'] = $cart_product['qty'];
                $product['product_type'] = $cart_product['product_type'];
                $product['discount_amount'] = ($cart_product['discount_amount']) ? $cart_product['discount_amount'] : 0;
                $product['discount_percentage'] = ($cart_product['discount_percentage']) ? $cart_product['discount_percentage'] : 0;
                $product['delivery_charge'] = ($cart_product['delivery_charge']) ? $cart_product['delivery_charge'] : 0;
                $product['total_amount'] = ($cart_product['total_amount']) ? $cart_product['total_amount'] : 0;
                $products[] = $product;
            }
        }
        return $products;
    }

    public function group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $date = $val[$key];
            if (isset($return[$date])) {
                $return[$date]["products"][] = $val;
            } else {
                $return[$date] = array(
                    "date" => $date,
                    "products" => array($val)
                );
            }
        }
        return array_values($return);
    }
}
