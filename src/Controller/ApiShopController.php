<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
//use Cake\ORM\User;

class ApiShopController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Shops');
        $this->loadModel('Carts');        
        $this->loadModel('Settings');
        $this->loadModel('Category');
    }

    public function beforeFilter(Event $event) {        
       $this->Auth->allow(array('authenticateUser','index','userInfo','userAdd','userEdit','userDelete','userView'));
    }

    public function userView($id) {
        $view = $this->Shops->view($id);
        if($view){
            $output = array(
                'status'=> 'success',
                'errorCode' =>1,
                'message'=>'User Found Successfully',
                'result' => $view,
            );
        } else {
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'User Not Found',
                'result' => array(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function authenticateUser()
    {
        $Shops = '';
        $output = array();
        if($this->request->getData()){
            $password = $this->request->getData('password');
            $Shops=$this->Shops->check($this->request->getData());
            if($Shops){
                $exists = $this->Shops->exists(['mobile' => $this->request->getData('username'),'password' => $Shops['password'] ]);
                $output=$this->Carts->check($Shops['userId']);
                unset($Shops['password']);
                if($exists){
                    $output = array(
                        'status'=> 'success',
                        'errorCode'=>1,
                        'message'=>'successfully logged In',
                        'result' => $Shops,
                    );
                }
                else{
                    $output = array(
                        'status'=> 'success',
                        'errorCode'=>0,
                        'message'=>'Username or Password is invalid',
                        'result' => new \stdClass(),
                    );
                }
            }
            else{
                $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Username or Password is invalid',
                'result' => new \stdClass(),
                 );
            }
        }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Username or Password is invalid',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function index()
    {
       $Shops = $this->Shops->find('all');
         if($Shops){
            $output = array(
                'status'=> 'success',
                'message'=>'successfully logged In',
                'result' => $Shops,
            );
        }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'User not Found',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userInfo($id)
    {
        if(!empty($id)) {
            $quantity=$this->Settings->getSettings();
            $categories=$this->Category->getCatInfo();
            $user = $this->Shops->view($id);
            $output=$this->Carts->check($id);
            if (!empty($output)) {
                $Shops['cartCount']= (string)$output[0]->total_products;
            } else {
                $Shops['cartCount']= '0';
            }
            if (!empty($categories)) {
                $Shops['categories'] = $categories;
            }
            if(!empty($user)) {
                $userInfo['firstName'] = $user['firstName'];
                $userInfo['lastName'] = $user['lastName'];
                $userInfo['userName'] = $user['userName'];
                $userInfo['userRole'] = $user['userRole'];
                $userInfo['email'] = $user['email'];
                $userInfo['shopName'] = $user['shopName'];
                $userInfo['shopNo'] = $user['shopNo'];
                $userInfo['shopLogo'] = $user['shopLogo'];
                $userInfo['address1'] = $user['address1'];
                $userInfo['address2'] = $user['address2'];
                $userInfo['phone'] = $user['phone'];
                $userInfo['mobile'] = $user['mobile'];
                $userInfo['pinCode'] = $user['pinCode'];
                $userInfo['city'] = $user['city'];
                $userInfo['state'] = $user['state'];
                $userInfo['country'] = $user['country'];
                $Shops['userDetails'] = $userInfo;
            }
            if(!empty($quantity)){
               $quantity["deliveryChargePerKg"]= $quantity["delivery charge per kg"];
               unset($quantity["delivery charge per kg"]);
            } else{
               $quantity["deliveryChargePerKg"]='0';
            }
            $Shops['quantity'] = $quantity;
            if($Shops) {
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>1,
                    'message'=>'App info received successfully',
                    'result' => $Shops,
                 );
            }
        } else {
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Please check the data',
                'result' => new \stdClass(),
             );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userAdd()
    {
        $user = $this->Shops->newEntity($this->request->getData());
        if ($this->Shops->save($user)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'message'=>'User Added Successfully',
            'result' => "",
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'User Not Added',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userEdit($id)
    {
        $user =$this->Shops->view($id);
        $Shops=$input=array();
        if ($this->request->is(['post', 'put']) && !empty($user)) {
            $input=$this->request->getData();
            if(!empty($input['userName'])){
               $input['username']=!empty($input['userName'])?$input['userName']:$user['userName'];
            }
            if(!empty($input['pinCode'])){
               $input['pincode']=!empty($input['pinCode'])?$input['pinCode']:$user['pinCode'];
            }
            if(!empty($input['shopNo'])){
               $input['shop_no']=!empty($input['shopNo'])?$input['shopNo']:$user['shopNo']; 
            }
            if(!empty($input['userRole'])){
               $input['user_role']=!empty($input['userRole'])?$input['userRole']:$user['userRole']; 
            }
            if(!empty($input['email'])){
               $input['email_id']=!empty($input['email'])?$input['email']:$user['email']; 
            }
            if(!empty($input['shopName'])){
               $input['shop_name']=!empty($input['shopName'])?$input['shopName']:$user['shopName']; 
            }
            if(!empty($input['firstName'])){
               $input['first_name']=!empty($input['firstName'])?$input['firstName']:$user['firstName']; 
            }
            if(!empty($input['lastName'])){
               $input['last_name']=!empty($input['lastName'])?$input['lastName']:$user['lastName']; 
            }
            if(!empty($input['shopNo'])){
               $input['shop_no']=!empty($input['shopNo'])?$input['shopNo']:$user['shopNo']; 
            }
            if(!empty($input['shopLogo'])){
               $input['shop_logo']=!empty($input['shopLogo'])?$this->imageUpload($input['shopLogo'],str_replace(" ","",$input['firstName'])):$user['shopLogo']; 
            }
            if(!empty($this->request->getData('password'))){
                $input['password'] = $this->request->getData('password');
            }else{
                unset($input['password']);
            }
            $edit_user = $this->Shops->get($id, [
                'contain' => []
            ]);
            $Shops = $this->Shops->patchEntity($edit_user,$input);
            if ($this->Shops->save($Shops)) {
                $basepath = BASE_URL.'/img/logo/';
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>1,
                    'shopLogo'=>$basepath.$Shops->shop_logo,
                    'message'=>'Profile updated successfully',
                );
            }
            else{
                $output = array(
                    'status'=> 'failure',
                    'errorCode'=>0,
                    'message'=>'User details not updated. Please try again.',
                );
            }
        }
        else {
           if($user){
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>1,
                    'message'=>'success',
                    'result' => $user,
                );
            }
            else{
                $output = array(
                    'status'=> 'failure',
                    'errorCode'=>0,
                    'message'=>'User details not updated. Please try again',
                    'result' => new \stdClass(),
                );
            }
        }               
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function imageUpload($base64_string, $name) {
        // open the output file for writing
        $root = WWW_ROOT.'img/logo/';
        $file_name = time().'_'.$name.'.png';
        $ifp = fopen( $root.$file_name, 'w+' ); 
        // split the string on commas
        fwrite( $ifp, base64_decode( $base64_string ) );
        // clean up the file resource
        fclose( $ifp );
        return $file_name;
    }
    
    public function userDelete($id)
    {
        $user_delete = $this->Shops->get($id);
        $message = 'Deleted';
        if (!$this->Shops->delete($user_delete)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'message'=>'success',
            'result' => new \stdClass(),
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'message'=>'Reason of error',
                'result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }
    
}