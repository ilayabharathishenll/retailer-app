<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RetailerController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['name'])) {
                $conditions['first_name like'] = trim("%".$searchParams['name']."%", " ");
            }
            if(!empty($searchParams['email_id'])) {
                $conditions['email_id like'] = trim("%".$searchParams['email_id']."%", " ");
            }
            if(!empty($searchParams['mobile'])) {
                $conditions['mobile like'] = trim("%".$searchParams['mobile']."%", " ");
            }
            if(!empty($searchParams['shop_name'])) {
                $conditions['shop_name like'] = trim("%".$searchParams['shop_name']."%", " ");
            }

            $this->paginate = ['conditions' => $conditions];
        }

        $retailer = $this->paginate($this->Retailer);

        $this->set(compact('retailer', 'searchParams'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $retailer = $this->Retailer->newEntity();
        if ($this->request->is('post')) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();

                if(!empty($data['shop_logo']['name'])) {
                    $file = $data['shop_logo']; //put the data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/logo/';
                    if(in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['shop_logo'] = $name;
                    }
                }
            }

            $retailer = $this->Retailer->patchEntity($retailer, $data);
            if ($this->Retailer->save($retailer)) {
                $this->Flash->success(__('The retailer has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The retailer could not be saved. Please, try again.'));
        }
        $this->set(compact('retailer'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $retailer = $this->Retailer->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // print_r($this->request->getData()['name');exit;
            $data = $this->request->getData();
            if(!empty($data['shop_logo']['name'])) {
                $file = $data['shop_logo']; //put the data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                $name = time().'-'.$file['name'];
                $upload_path = WWW_ROOT.'img/logo/';
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $upload_path.$name);
                    $data['shop_logo'] = $name;
                }
            }
            $retailer = $this->Retailer->patchEntity($retailer, $data);
            if ($this->Retailer->save($retailer)) {
                $this->Flash->success(__('The retailer has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save retailer. Please, try again.'));
        }
        $this->set(compact('retailer'));
    }
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $retailer = $this->Retailer->get($id);
        // print_r($retailer);exit;
        if ($this->Retailer->delete($retailer)) {
            $this->Flash->success(__('The retailer has been deleted.'));
        } else {
            $this->Flash->error(__('Unable to save retailer. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login() {
        $this->autoRender = false;
        $this->set('state', "1");
        if($this->request->is('post')) {
            //
            $user = $this->Auth->identify();
            // print_r($user);exit;
            if($user) {
                $this->Auth->setUser($user);
                $session = $this->request->session();
                $session->write('users',$user);
                return $this->redirect(['controller' => 'dashboard']);
            }
            $this->set('state', "0");
        }
        $this->layout = 'empty';
        $this->render('login');
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }

    public function uniqueRetailerEmail()
    {
        $data = $this->request->getData();
        if (!empty($data["id"])) {
            $cond = array('email_id' => $data["email_id"], 'shop_id !=' => $data["id"]);
        } else {
            $cond = array('email_id' => $data["email_id"]);
        }

        if ($this->Retailer->exists($cond)) {
            die("false");
        } else {
            die("true");
        }
    }

    public function uniqueRetailerMobile()
    {
        $data = $this->request->getData();
        if (!empty($data["id"])) {
            $cond = array('mobile' => $data["mobile"], 'shop_id !=' => $data["id"]);
        } else {
            $cond = array('mobile' => $data["mobile"]);
        }

        if ($this->Retailer->exists($cond)) {
            die("false");
        } else {
            die("true");
        }
    }
}
