<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
//use Cake\ORM\User;

class ApiUsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Users');        
    }

    public function beforeFilter(Event $event) {        
       $this->Auth->allow(array('authenticateUser','index','userView','userAdd','userEdit','userDelete'));
    }
    
    public function authenticateUser()
    {
        $users='';
        if($this->request->getData()){
            $password = $this->request->getData('password');
            $users=$this->Users->check($this->request->getData());
            if($users){
                $exists = $this->Users->exists(['email_id' => $this->request->getData('username'),'password' => $users['password'] ]);
                unset($users['password']);
                if($exists){
                    $output = array(
                    'status'=> 'success',
                    'errorCode'=>1,
                    'Message'=>'successfully logged In',
                    'Result' => $users,
                     );
                }
            }
            else{
                $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'Invalid status or reason of error',
                'Result' => new \stdClass(),
                 );
            }
        }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'Invalid status or reason of error',
                'Result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function index()
    {
       $users = $this->Users->find('all');
         if($users){
            $output = array(
            'status'=> 'success',
            'Message'=>'successfully logged In',
            'Result' => $users,
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'Invalid status or reason of error',
                'Result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userView($id)
    {
        $view = $this->Users->find('all')
                ->where(['user_id'=>$id]);
        if($view){
            $output = array(
            'status'=> 'success',
            'errorCode' =>1,
            'Message'=>'successfully get',
            'Result' => $view,
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'reason of error',
                'Result' => new \stdClass(),
                 );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userAdd()
    {
        $user = $this->Users->newEntity($this->request->getData());
        if ($this->Users->save($user)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'Message'=>'success',
            'Result' => "",
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'Reason of error',
                'Result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userEdit($id)
    {
        $user =$this->Users->find()
                ->where(['user_id'=>$id]);
        $users=$input=array();
        if ($this->request->is(['post', 'put'])) {
            $input=$this->request->getData();
            if(!empty($input['userName'])){
               $input['name']=!empty($input['userName'])?$input['userName']:$user['userName'];
            }
            if(!empty($input['pinCode'])){
               $input['pin_code']=!empty($input['pinCode'])?$input['pinCode']:$user['pinCode'];
            }
            if(!empty($input['shopNo'])){
               $input['shop_no']=!empty($input['shopNo'])?$input['shopNo']:$user['shopNo']; 
            }               
            if($this->request->getData('password')){
                $password = $this->request->getData('password');
                $hasher = new DefaultPasswordHasher();
                $input['password']=$hasher->hash($password);
            }
            $edit_user = $this->Users->get($id, [
                'contain' => []
            ]);
            $users = $this->Users->patchEntity($edit_user,$input);
           
            if ($this->Users->save($users)) {
                $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'Message'=>'success',
                'Result' => "",
                 );
            }
            else{
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>0,
                    'Message'=>'Invalid status or reason of error',
                    'Result' => new \stdClass(),
                );
            }
        }
        else {
           if($user){
                $output = array(
                'status'=> 'success',
                'errorCode'=>1,
                'Message'=>'success',
                'Result' => $user,
                 );
            }
            else{
                $output = array(
                    'status'=> 'success',
                    'errorCode'=>0,
                    'Message'=>'Invalid status or reason of error',
                    'Result' => new \stdClass(),
                );
            }
        }               
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function userDelete($id)
    {
        $user_delete = $this->Users->get($id);
        $message = 'Deleted';
        if (!$this->Users->delete($user_delete)) {
            $output = array(
            'status'=> 'success',
            'errorCode'=>1,
            'Message'=>'success',
            'Result' => "",
             );
            }
        else{
            $output = array(
                'status'=> 'success',
                'errorCode'=>0,
                'Message'=>'Reason of error',
                'Result' => new \stdClass(),
            );
        }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        $this->autoRender = false;
        echo json_encode($output);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $hashing=new DefaultPasswordHasher();
            $auths=new FormAuthenticate();
            $auths->config(['fields' => [
                'username' => 'email_id',
                'password' => 'password'
            ]]);
            $result=$hashing->check($this->request->getData('password'),'$2y$10$qSKnogSvkDS87XHAMg3MCe.FbReM4Upd1xk9Teovbdoz82JrKzAlK');
            $user = $auths->getUsers($this->request->getData());
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
    }
}