<?php
namespace App\Controller;

Use App\Controller\AppController;

class SubCategoryController extends AppController {
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Category');
        $this->loadModel('SubCategory');
    }
	public function index() {
		$searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['name'])) {
                $conditions['sub_category_name like'] = trim("%".$searchParams['name']."%", " ");
            }
             if(!empty($searchParams['category_name'])) {
                $conditions['category_name like'] = trim("%".$searchParams['category_name']."%", " ");
            }
            $this->paginate = ['conditions' => $conditions];
        }

		 //$subcategory = $this->paginate($this->SubCategory);
        $subcategory = $this->SubCategory->find('all', ['contain' => 'tbl_categories'])->where($conditions)->toArray();
       // print_r($subcategory);
		$this->set(compact('subcategory', 'searchParams'));
	}
    public function add() {
       $category = $this->Category->getCat();
       $subcategory = $this->SubCategory->newEntity();
        if($this->request->is('post')) {
            $data = $this->request->data();
            if(!empty($data["image"]["name"])) {
                $file = $data["image"];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                $extArr = array('jpg', 'jpeg', 'gif', 'png');
                $name = time().'-'.$file['name'];
                $upload_path = WWW_ROOT.'img/subcategory/';
                if(in_array($ext, $extArr)) {
                    move_uploaded_file($file['tmp_name'], $upload_path.$name);
                    $data['image'] = $name;
                }
            }
            $subcategories = $this->SubCategory->find()->select()->where(['sub_category_name'=>$data['sub_category_name']])->toArray();
            if (!empty($subcategories)) {
                $this->Flash->error(__("Sub Category name already used."));
            } else {
                $subcategory = $this->SubCategory->patchEntity($subcategory, $data);
                if($this->SubCategory->save($subcategory)) {
                    $this->Flash->success(__("Sub category has been saved"));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__("Category Name is required."));
            }
        }
        $this->set(compact('subcategory','category'));
    }
    public function edit($id = null) {
        $category = $this->Category->getCat();
        $subcategory = $this->SubCategory->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->getData())) {
                $data = $this->request->getData();
                if(!empty($data["image"]["name"])) {
                    $file = $data["image"];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                    $extArr = array('jpg', 'jpeg', 'gif', 'png');
                    $name = time().'-'.$file['name'];
                    $upload_path = WWW_ROOT.'img/subcategory/';
                    if(in_array($ext, $extArr)) {
                        move_uploaded_file($file['tmp_name'], $upload_path.$name);
                        $data['image'] = $name;
                    }
                }
            }
            $subcategory = $this->SubCategory->patchEntity($subcategory, $data);
            if ($this->SubCategory->save($subcategory)) {
                $this->Flash->success(__('The sub category has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Category Name is required.'));
        }
        $this->set(compact('subcategory','category'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subcategory = $this->SubCategory->get($id);
        if ($this->SubCategory->delete($subcategory)) {
            $this->Flash->success(__('The sub category has been deleted.'));
        } else {
            $this->Flash->error(__('The sub category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function getEnableSubCategory()
    {
        if ($this->request->is(['post'])) {
            $category = $this->request->getData();
            $catId = $category["category_id"];
            $subcategory = $this->SubCategory->find()->select()->where(['SubCategory.category_id'=>$catId])->toArray();
            $ArrySubCate=array();
            $i=0;
            if (count($subcategory)>0) {
                foreach ($subcategory as $subcategoryDetails){
                    $ArrySubCate[$i]["sub_cate_id"]=$subcategoryDetails->sub_category_id;
                    $ArrySubCate[$i]["sub_cate_name"]=$subcategoryDetails->sub_category_name;
                    $i++;
                }
                $result="success##^^##".json_encode($ArrySubCate);
            } else{
                $result="failure"; 
            }
        }
        die($result);
    }
}
?>