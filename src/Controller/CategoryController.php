<?php
namespace App\Controller;

Use App\Controller\AppController;

class CategoryController extends AppController {

	public function index() {
		$searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['name'])) {
                $conditions['category_name like'] = trim("%".$searchParams['name']."%", " ");
            }

            $this->paginate = ['conditions' => $conditions];
            //print_r($this->paginate);exit;
        }
		$category = $this->paginate($this->Category);
		$this->set(compact('category', 'searchParams'));
	}

	public function add() {
		$category = $this->Category->newEntity();
		if($this->request->is('post')) {
			$data = $this->request->data();
			if(!empty($data["image"]["name"])) {
				$file = $data["image"];
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$extArr = array('jpg', 'jpeg', 'gif', 'png');
				$name = time().'-'.$file['name'];
				$upload_path = WWW_ROOT.'img/category/';
				if(in_array($ext, $extArr)) {
                    move_uploaded_file($file['tmp_name'], $upload_path.$name);
                    $data['image'] = $name;
                }
			}
			$categories = $this->Category->find()->select()->where(['category_name'=>$data['category_name']])->toArray();
			if (!empty($categories)) {
				$this->Flash->error(__("Category name already used."));
			} else {
            	$data["enable_date"] = isset($data['enable_date']) ? 1 : 0;
				$category = $this->Category->patchEntity($category, $data);
				if($this->Category->save($category)) {
					$this->Flash->success(__("Category saved"));
					return $this->redirect(['action' => 'index']);
				}
				$this->Flash->error(__("Unable to save category. Please, try again."));
			}
		}
		$this->set(compact("category"));
	}

	public function edit($id = null) {
		$category = $this->Category->get($id);
		if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($this->request->getData())) {
            	$data = $this->request->getData();
            	if(!empty($data["image"]["name"])) {
					$file = $data["image"];
					$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
					$extArr = array('jpg', 'jpeg', 'gif', 'png');
					$name = time().'-'.$file['name'];
					$upload_path = WWW_ROOT.'img/category/';
					if(in_array($ext, $extArr)) {
	                    move_uploaded_file($file['tmp_name'], $upload_path.$name);
	                    $data['image'] = $name;
	                }
				}
        	}
        	$data["enable_date"] = isset($data['enable_date']) ? 1 : 0;
        	$category = $this->Category->patchEntity($category, $data);
            if ($this->Category->save($category)) {
                $this->Flash->success(__('Category details are updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to save category. Please, try again.'));
        }
        $this->set(compact('category'));
	}

	public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Category->get($id);
        if ($this->Category->delete($category)) {
            $this->Flash->success(__('Product deleted successfully.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getEnableDate()
    {
        if ($this->request->is(['post'])) {
            $category = $this->request->getData();
            $catId = $category["category_id"];
            $categoryDate = $this->Category->find('all')->select('enable_date')->where(['category_id'=>$catId])->toArray();
            if ($categoryDate[0]["enable_date"] == 1) {
                die("true");
            } else {
                die("false");
            }
        }
    }
}
?>