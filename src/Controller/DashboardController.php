<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Shops');
        $this->loadModel('Carts');
        $this->loadModel('Payments');
        $this->loadModel('CartProducts');
    }

    public function index()
    {
        $users= $this->Shops->find()->toArray();
        $active_users= $this->Users->find()->toArray();
        $carts= $this->Carts->find()->where(['status IN'=> ['Ordered', 'Paid']])->toArray();
        $total_order_price = 0;
        if ($carts) {
            foreach ($carts as $cart) {
                $order_price = ($cart['order_price']) ? $cart['order_price'] : 0;
                $total_order_price += $order_price;
            }
        }
        $activeUsersCount=sizeof($active_users);
        $usersCount=sizeof($users);
        $carts=sizeof($carts);
        $RetailerDetails =  $this->Shops->find()->order('shop_id DESC')->limit(1)->toArray();
        $RetailerTablesDetails =  $this->Shops->find()->order('shop_id DESC')->limit(5)->toArray();
        $conditions['transaction_status'] = 'authorized';
        $order_details = $this->Payments->find("all", ['contain' => array('Shops', 'CartProducts' => ['TblProducts'])])
            ->where($conditions)->order("payment_id DESC")->limit(5)->toArray();

        if ($order_details) {
            foreach ($order_details as $order) {
                $data = $order;
                if ($order['transaction_date'])
                    $data['transaction_date'] = date("d/m/Y", strtotime($order['transaction_date']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                $data["items"] = $this->getProducts($order["cart_id"]);
                $orders[] = $data;
            }
        }

        $orderdetails = $this->Payments->find("all", ['contain' => array('Shops', 'CartProducts' => ['TblProducts'])])
            ->where($conditions)->order("payment_id DESC")->limit(1)->toArray();

        $condition['payment_type'] = 'cod';
        $codorderdetails = $this->Payments->find("all", ['contain' => array('Shops')])->where($condition)->order("payment_id DESC")->limit(1)->toArray();
        $codorder_details = $this->Payments->find("all", ['contain' => array('Shops')])->where($condition)->order("payment_id DESC")->limit(5)->toArray();
        if ($codorder_details) {
            foreach ($codorder_details as $ordercash) {
                $datas = $ordercash;
                if ($ordercash['transaction_date'])
                    $datas['transaction_date'] = date("d/m/Y", strtotime($ordercash['transaction_date']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                $datas["items"] = $this->getProducts($ordercash["cart_id"]);
                $codorders[] = $datas;
            }
        }
        //print_r($orders);
        //$this->set('dashboard');
        $this->set(compact('dashboard', 'usersCount','RetailerDetails','RetailerTablesDetails',"orders","orderdetails", "carts", "total_order_price", 'activeUsersCount',"codorders","codorderdetails"));
    }

    public function getProducts($cart_id) {
        $products = array();
        $cart_products = $this->CartProducts->find("all", array('order'=>array('CartProducts.created_on'=>'desc'),'contain' => array('TblProducts') ))
            ->where(['CartProducts.cart_id'=> $cart_id])->toArray();
        if ($cart_products) {
            foreach ($cart_products as $cart_product) {
                $$product = array();
                $product['productName'] = $cart_product['tbl_product']['product_name'];
                $product['unit_price'] = $cart_product['unit_price'];
                $product['unit_total'] = $cart_product['unit_total'];
                $product['quantity'] = $cart_product['qty'];
                $product['product_type'] = $cart_product['product_type'];
                $product['discount_amount'] = ($cart_product['discount_amount']) ? $cart_product['discount_amount'] : 0;
                $product['discount_percentage'] = ($cart_product['discount_percentage']) ? $cart_product['discount_percentage'] : 0;
                $product['delivery_charge'] = ($cart_product['delivery_charge']) ? $cart_product['delivery_charge'] : 0;
                $product['total_amount'] = ($cart_product['total_amount']) ? $cart_product['total_amount'] : 0;
                $product['package_size'] = ($cart_product['package_size']) ? $cart_product['package_size'] : "";
                $products[] = $product;
            }
        }
        return $products;
    }
}
