<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CodController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Payments');
        $this->loadModel('Carts');
        $this->loadModel('CartProducts');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $searchParams = $this->request->data();
        $conditions = array();

        if(!empty($searchParams)) {
            if(!empty($searchParams['reference_number'])) {
                $conditions['Payments.reference_number'] = trim($searchParams['reference_number'], "");
            }
            if(!empty($searchParams['shop_name'])) {
                $conditions['Shops.shop_name like'] = trim("%" . $searchParams['shop_name'] . "%", "");
            }
            if(!empty($searchParams['area'])) {
                $conditions['Shops.area'] = trim($searchParams['area'], " ");
            }
        }
        $conditions['payment_type'] = 'cod';
        $order_details = $this->Payments->find("all", ['contain' => array('Shops')])->where($conditions)->order("payment_id DESC")->toArray();
        if ($order_details) {
            foreach ($order_details as $order) {
                $data = $order;
                if ($order['transaction_date'])
                    $data['transaction_date'] = date("d/m/Y", strtotime($order['transaction_date']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                $data["items"] = $this->getProducts($order["cart_id"]);
                $orders[] = $data;
            }
        }
        $this->set(compact('orders', 'searchParams'));
    }
    public function getProducts($cart_id) {
        $products = array();
        $cart_products = $this->CartProducts->find("all", array('order'=>array('CartProducts.created_on'=>'desc'),'contain' => array('TblProducts') ))
            ->where(['CartProducts.cart_id'=> $cart_id])->toArray();
            // print_r($cart_products);exit;
        if ($cart_products) {
            foreach ($cart_products as $cart_product) {
                $$product = array();
                $product['productName'] = $cart_product['tbl_product']['product_name'];
                $product['unit_price'] = $cart_product['unit_price'];
                $product['unit_total'] = $cart_product['unit_total'];
                $product['quantity'] = $cart_product['qty'];
                $product['product_type'] = $cart_product['product_type'];
                $product['discount_amount'] = ($cart_product['discount_amount']) ? $cart_product['discount_amount'] : 0;
                $product['discount_percentage'] = ($cart_product['discount_percentage']) ? $cart_product['discount_percentage'] : 0;
                $product['delivery_charge'] = ($cart_product['delivery_charge']) ? $cart_product['delivery_charge'] : 0;
                $product['total_amount'] = ($cart_product['total_amount']) ? $cart_product['total_amount'] : 0;
                $product['package_size'] = ($cart_product['package_size']) ? $cart_product['package_size'] : "";
                $products[] = $product;
            }
        }
        return $products;
    }
    public function edit($id = null)
    {   
        $conditions = array();
        $paymentId = $this->request->getCookie('payment_id');
        $paymentId = explode(",", $paymentId);

        $conditions['payment_id IN'] = $paymentId;
        $order_details = $this->Payments->find("all", ['contain' => array('Shops')])->where($conditions)->toArray();
        if ($order_details) {
                foreach ($order_details as $order) {
                    $data = $order;
                    if ($order['transaction_date'])
                        $data['transaction_date'] = date("d/m/Y", strtotime($order['transaction_date']->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE])));
                    $orders[] = $data;
                    $cartArrId[]=$data['cart_id'];
                }
                //print_r($orders); exit;
            }
        if ($this->request->is(['post', 'put'])) {
             $data = $this->request->getData()['payment'];
             $data["transaction_date"] = date("Y-m-d h:i", strtotime($data["transaction_date"]));
            if (isset($data['transaction_status'])){
                $payment = $this->Payments->updateAll(['transaction_date' => $data['transaction_date'], 'transaction_status' => $data['transaction_status']], ['payment_id IN' => $paymentId]);
                if(strtolower($data['transaction_status'])=="delivered") {
                    $cartId = implode(",", $cartArrId);
                    $cartdetails = $this->Carts->updateAll(['payment_date' =>$data['transaction_date'], 'status' =>"Paid"], ['cart_id IN' => $cartId]);
                }
                if($payment) {
                    $this->Flash->success(__('Order has been updated.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('Unable to save order. Please, try again.'));
        }
        $this->set(compact('orders', 'cookies'));
    }
}